/// Implements methods from SmartPreprocessor defined in preprocess.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "preprocess.h"
#include "suggester.h"

#include <algorithm>
#include <cassert>
#include <functional>
#include <deque>

namespace esicup {

SolutionInputData SmartPreprocessor::prepare(const TestData& data) {
    SolutionInputData result;
    result.context = data.context;

    // 1: create trivial partition: all equal items go in one group
    GroupOrder trivialOrder;
    size_t nItems = data.context->getItemTypesCount();
    for (size_t itemType = 0; itemType < nItems; ++itemType) {
        trivialOrder.groups.emplace_back(itemType, data.context->getItemCount(itemType));
    }
    result.orders.push_back(trivialOrder);

    // 2: apply sorting for given variants

    SorterSuggester sorterSuggester;
    sorterSuggester.suggest(trivialOrder, result.orders, data.context);

    // 3: create possible partions with cutting
    CutterSuggester cutterSuggester;
    auto cuttedCopy = result.orders;
    for (auto& order : cuttedCopy) {
        cutterSuggester.suggest(order, result.orders, data.context);
    }
    return result;
}

} // namespace esicup
