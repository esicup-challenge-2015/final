/// Implements structures, methods and API defined in suggester.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024


#include "suggester.h"

namespace esicup {

void CutterSuggester::suggest(
        const GroupOrder& origin,
        std::vector<GroupOrder>& storage,
        ConstContextPtr context) {
    assert(context->getItemTypesCount());
    const size_t COUNT_THRESHOLD = 50;
    storage.push_back(regroupByCount(origin, COUNT_THRESHOLD));
    std::deque<Group> cuttedInHalfOrder;
    for (const auto& group : origin.groups) {
        if (group.count > 2) {
            Group firstHalf(group.itemType, group.count / 2);
            Group secondHalf(group.itemType, group.count - firstHalf.count);

            cuttedInHalfOrder.push_front(firstHalf);
            cuttedInHalfOrder.push_back(secondHalf);
        } else {
            cuttedInHalfOrder.push_back(group);
        }
    }
    storage.push_back(GroupOrder(
        std::vector<Group>(std::begin(cuttedInHalfOrder), std::end(cuttedInHalfOrder)),
        "cutted_in_half"));
}

SorterSuggester::SorterSuggester() {
    // by width
    comparators.push_back(
        [](const GroupProxy& lhs, const GroupProxy& rhs) -> bool  {
                return lhs.itemInfo.width < rhs.itemInfo.width;
        });
    // by height
    comparators.push_back(
        [](const GroupProxy& lhs, const GroupProxy& rhs) -> bool  {
                return lhs.itemInfo.height < rhs.itemInfo.height;
        });
    // by length
    comparators.push_back(
        [](const GroupProxy& lhs, const GroupProxy& rhs) -> bool  {
                return lhs.itemInfo.length < rhs.itemInfo.length;
        });
    // by weight
    comparators.push_back(
        [](const GroupProxy& lhs, const GroupProxy& rhs) -> bool  {
            return lhs.itemInfo.weight < rhs.itemInfo.weight;
        });
    // by total weight
    comparators.push_back(
        [](const GroupProxy& lhs, const GroupProxy& rhs) -> bool  {
            return lhs.itemInfo.weight * lhs.count < rhs.itemInfo.weight * rhs.count;
        });
    // by area
    comparators.push_back(
        [](const GroupProxy& lhs, const GroupProxy& rhs) -> bool  {
            return lhs.itemInfo.length * lhs.itemInfo.width <
                rhs.itemInfo.length * rhs.itemInfo.width;
        });
    // by total area
    comparators.push_back(
        [](const GroupProxy& lhs, const GroupProxy& rhs) -> bool  {
            return lhs.count * lhs.itemInfo.length * lhs.itemInfo.width <
                rhs.count * rhs.itemInfo.length * rhs.itemInfo.width;
        });

    // by volume
    comparators.push_back(
        [](const GroupProxy& lhs, const GroupProxy& rhs) -> bool  {
            return lhs.itemInfo.height * lhs.itemInfo.length * lhs.itemInfo.width <
                rhs.itemInfo.height * rhs.itemInfo.length * rhs.itemInfo.width;
        });
    // by total volume
    comparators.push_back(
        [](const GroupProxy& lhs, const GroupProxy& rhs) -> bool  {
        return lhs.count * lhs.itemInfo.height * lhs.itemInfo.length * lhs.itemInfo.width <
            rhs.count * rhs.itemInfo.height * rhs.itemInfo.length * rhs.itemInfo.width;
        });
}

void SorterSuggester::suggest(
        const GroupOrder& origin,
        std::vector<GroupOrder>& storage,
        ConstContextPtr context) {

    std::vector<GroupProxy> groupProxies(origin.groups.size());
    for (size_t idx = 0; idx < origin.groups.size(); ++idx) {
        groupProxies[idx].id = idx;
        groupProxies[idx].count = origin.groups[idx].count;
        groupProxies[idx].itemInfo = context->getItemInfo(origin.groups[idx].itemType);
    }

    for (const auto& function : comparators) {
        std::sort(std::begin(groupProxies), std::end(groupProxies), function);
        GroupOrder result;
        result.groups.reserve(origin.groups.size());
        result.history = origin.history;
        for (const auto& proxy : groupProxies) {
            result.groups.push_back(origin.groups[proxy.id]);
        }
        storage.push_back(result);
        GroupOrder resultCopy = result;
        std::reverse(resultCopy.groups.begin(), resultCopy.groups.end());
        storage.push_back(resultCopy);
    }

    GroupOrder clonedOrigin(origin);

    unsigned randomState = 12;
    std::srand(randomState);

    const size_t RAND_CNT = 2;
    for (size_t randIdx = 0; randIdx < RAND_CNT; ++randIdx) {
        std::random_shuffle(
            std::begin(clonedOrigin.groups), std::end(clonedOrigin.groups));
        storage.push_back(clonedOrigin);
    }
}


} // namespace esicup
