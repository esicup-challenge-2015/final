/**
 * @file preprocess.h
 * @author Aliaksandr Nekrashevich
 * @brief Defines and implements preprocessors - entities to convert input
 * data to a format suitable for further processing, and also enhancing by
 * suggesting different group orderings and sequences of group sizes.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "../interfaces.h"

namespace esicup {

/**
 * @brief IPreprocessor describes interface for Preprocessor.
 * Preprocessor is an entity to convert data from 
 * input format to a format suitable for Processor. 
 */
class IPreprocessor {
public:
    /**
     * @brief Converts input data to solution input format
     * and prepares some inner data structures.
     * 
     * @param data The input data to start with.
     * @return Data in the format acceptable by Processors.
     */
    virtual SolutionInputData prepare(const TestData& data) = 0;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~IPreprocessor() {}
};

/**
 * @brief Trivial Preprocessor. Creates solution input data
 * (format suitable for Processor) by simply copying information
 * and group order from the original input data.
 */
class TrivialPreprocessor final: public IPreprocessor {
public:
    /**
     * @brief Prepares solution input data by simply copying information
     * and group order from original input data.
     * 
     * @param data The instance-specific test input data to start with.
     * @return The solution input data (data that can get processed by Processor).
     */
    SolutionInputData prepare(const TestData& data) override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~TrivialPreprocessor() {}
};

/**
 * @brief Smart Preprocessor. Creates solution input data by
 * enhancing with multiple group order sortings and group
 * rearrangements.
 */
class SmartPreprocessor final: public IPreprocessor {
public:
    /**
     * @brief Prepares solution input data by enhancing with
     * multiple group order sortings and group size rearrangements.
     * 
     * @param data The instance-specific test input data to start with.
     * @return The solution input data, ready for processing inside Processor.
     */
    SolutionInputData prepare(const TestData& data) override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~SmartPreprocessor() {}
};

} // namespace esicup

