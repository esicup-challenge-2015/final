/// Implements class methods for TrivialPreprocessor defined in preprocess.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "preprocess.h"

namespace esicup {

SolutionInputData TrivialPreprocessor::prepare(
        const TestData& data) {
    SolutionInputData result;
    result.context = data.context;
    result.orders.resize(1);

    size_t nItems = data.context->getItemTypesCount();
    for (size_t itemType = 0; itemType < nItems; ++itemType) {
        result.orders[0].groups.push_back(
            Group(itemType, data.context->getItemCount(itemType)));
    }

    return result;
}

} // namespace esicup
