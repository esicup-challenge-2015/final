/**
 * @file suggester.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines interface to a group suggester 
 * (entity to create different group orders), and two 
 * implementations: CutterSuggester and SorterSuggester.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */

#pragma once

#include "../interfaces.h"

#include <algorithm>
#include <cassert>
#include <functional>
#include <deque>

namespace esicup {

/**
 * @brief IGroupSuggester is an entity interface
 * to create different group sortings, that
 * are afterwards supplied to the storage.
 */
class IGroupSuggester {
public:
    /**
     * @brief Suggests other group sequences 
     * and adds the suggested sequence to storage.
     *
     * @param origin Original group sequence.
     * @param storage The storage for modified group sequences.
     * @param context The context with instance-specific input information.
     */
    virtual void suggest(
        const GroupOrder& origin,
        std::vector<GroupOrder>& storage,
        ConstContextPtr context) = 0;
    /**
     * @brief Virtual destructor for correct C++ polymorphism.
     */
    virtual ~IGroupSuggester() {};
};

class CutterSuggester final : IGroupSuggester {
public:
    /**
     * @brief Suggests group sequences by cutting 
     * the originally supplied groups into parts.
     * 
     * @param origin The original group order to start from.
     * @param storage The storage to store suggested group orders.
     * @param context The context describing instance-specific input.
     */
    virtual void suggest(const GroupOrder& origin,
            std::vector<GroupOrder>& storage,
            ConstContextPtr context) final override;
    /**
     * @brief Virtual destructor for correct C++ polymorphism.
     */
    virtual ~CutterSuggester() {};

private:
    /**
     * @brief Regroups initial group order by ensuring that each group has 
     * size smaller or equal to allowed count.
     * 
     * @param origin The initial group order.
     * @param allowedCount The maximal allowed group count.
     * @return The group order, obtained from original by splitting groups into
     *      parts with smaller sizes.
     */
    GroupOrder regroupByCount(const GroupOrder& origin, size_t allowedCount) const {
        GroupOrder result;
        result.history = "cutted_by_count";
        auto& sourceGroups = origin.groups;
        for (const auto& group : sourceGroups) {
            size_t leftItems = group.count;
            while (leftItems > 0) {
                size_t canPut = std::min(leftItems, allowedCount);
                result.groups.emplace_back(group.itemType, canPut);
                leftItems -= canPut;
            }
        }
        return result;
    }
};

class SorterSuggester final: IGroupSuggester {
public:
    /**
     * @brief Constructor. Introduces various 
     * comparators and stores them inside
     * the corresponding data structure.
     */
    SorterSuggester ();
    /**
     * @brief Sorts initial group order according to multiple sorting
     * functions available as comparators, and stores the corresponding
     * results to the supplied storage.
     * 
     * @param origin  The initial group order.
     * @param storage The place for storing suggestions.
     * @param context The context describing instance-specific input.
     */
    virtual void suggest(const GroupOrder& origin,
            std::vector<GroupOrder>& storage,
            ConstContextPtr context) final override;
    /**
     * @brief Virtual destructor for correct C++ polymorphism.
     */
    virtual ~SorterSuggester() {};

private:
    /**
     * @brief Group proxy is a structure that allows easier sorting.
     */
    struct GroupProxy {
        /// @brief Index of the group in original group order.
        size_t id; 
        /// @brief Amount of items in group. 
        size_t count;
        /// @brief Item description.
        ItemInfo itemInfo;
    };
    /// @brief Stores comparators that are applied for sortings.
    std::vector<std::function<bool(const GroupProxy&, const GroupProxy&)>> comparators;
};


} // namespace esicup
