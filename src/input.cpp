/// Implements API and methods defined in input.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "input.h"
#include "interfaces.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <cassert>

namespace esicup {

TestData Input::readInput(const TestMetadata& testMetadata) {
    TestData data;
    auto context = ContextPtr(new Context);
    context->setAcceleration(testMetadata.accelerate);

    std::string itemsFileName = testMetadata.pathToTest + itemsFileSuffix;
    std::string binsFileName = testMetadata.pathToTest + binsFileSuffix;
    std::string parametersFileName = testMetadata.pathToTest + parametersFileSuffix;

    readItems(context, itemsFileName);
    readBins(context, binsFileName);
    readParameters(context, parametersFileName);

    data.context = context;
    return data;
}

void Input::readItems(ContextPtr context, const std::string& filePath) {
    std::ifstream inputStream(filePath);
    if (!inputStream) {
        throw std::runtime_error("Cannot open items file.");
    }

    std::string line;
    getline(inputStream, line);

    while(getline(inputStream, line)) {
        std::istringstream stringStream(line);

        ItemInfo item;
        std::string value;

        getline(stringStream, value, ';');
        size_t itemId = std::stoi(value);

        getline(stringStream, value, ';');
        item.length = std::stoi(value);

        getline(stringStream, value, ';');
        item.width = std::stoi(value);

        getline(stringStream, value, ';');
        item.height = std::stoi(value);

        getline(stringStream, value, ';');
        item.weight = std::stod(value);

        getline(stringStream, value, ';');
        item.material = extractMaterialFromString(value);

        getline(stringStream, value, ';');
        item.position = extractPositionFromString(value);

        getline(stringStream, value, ';');
        item.productName = value;

        context->addItem(item, itemId);
    }
}

void Input::readBins(ContextPtr context, const std::string& filePath) {
    std::ifstream inputStream(filePath);
    if (!inputStream) {
        throw std::runtime_error("Cannot open bins file");
    }

    std::string line;
    getline(inputStream, line);

    while(getline(inputStream, line)) {
        std::istringstream stringStream(line);

        BinInfo bin;
        std::string value;

        getline(stringStream, value, ';');
        bin.binName = value;

        getline(stringStream, value, ';');
        bin.length = std::stoi(value);

        getline(stringStream, value, ';');
        bin.width = std::stoi(value);

        getline(stringStream, value, ';');
        bin.height = std::stoi(value);

        getline(stringStream, value, ';');
        bin.allowedWeight = std::stod(value);

        context->addBin(bin);
    }
}

void Input::readParameters(ContextPtr context, const std::string& filePath) {
    std::ifstream inputStream(filePath);
    if (!inputStream) {
        throw std::runtime_error("Cannot open parameters file");
    }

    Constraints constraints;
    std::string line;

    while (inputStream >> line) {
        std::istringstream stringStream(line);
        std::string requirement;
        std::string value;
        getline(stringStream, requirement, '=');
        getline(stringStream, value);

        if (requirement == "tolerance_item_product_demands") {
            constraints.tolerance_item_product_demands = std::stod(value);
        } else if (requirement == "maximum_density_allowed_kg") {
            constraints.maximum_density_allowed_kg = std::stod(value);
            const double CONVERT_TO_KG = 1000000.;
            constraints.maximum_density_allowed_kg /= CONVERT_TO_KG;
        } else if (requirement == "max_number_of_rows_in_a_layer") {
            constraints.max_number_of_rows_in_a_layer = std::stoi(value);
        } else if (requirement == "max_number_of_items_in_a_row") {
            constraints.max_number_of_items_in_a_row = std::stoi(value);
        } else if (requirement == "layer_size_deviation_tolerance") {
            constraints.layer_size_deviation_tolerance = std::stod(value);
        } else if (requirement == "row_size_deviation_tolerance") {
            constraints.row_size_deviation_tolerance = std::stod(value);
        } else if (requirement == "item_size_deviation_tolerance") {
            constraints.item_size_deviation_tolerance = std::stod(value);
        } else if (requirement == "maximum_weigth_above_item_kg") {
            constraints.maximum_weigth_above_item_kg = std::stod(value);
        }
    }
    context->setConstraints(constraints);
}

Material Input::extractMaterialFromString (const std::string& value) {
    if (value == "METAL") {
        return Material::METAL;
    } else if (value == "CARDBOARD") {
        return Material::CARDBOARD;
    } else if (value == "WOOD") {
        return Material::WOOD;
    } else if (value == "PLASTIC") {
        return Material::PLASTIC;
    }
    throw std::logic_error("Unexpected material!");
}

Position Input::extractPositionFromString(const std::string& value) {
    if (value == "WIDTHWISE") {
        return Position::WIDTHWISE;
    } else if (value == "LENGTHWISE") {
        return Position::LENGTHWISE;
    }
    return Position::BOTH;
}

const std::string Input::binsFileSuffix = "/input_bin.csv";
const std::string Input::itemsFileSuffix = "/input_items.csv";
const std::string Input::parametersFileSuffix = "/parameters.txt";

} // namespace esicup

