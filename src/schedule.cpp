/// Implements API defined in schedule.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024


#include "schedule.h"

#include <iostream>
#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <algorithm>
#include <ctime>

#ifdef PROFILE_ON
#include <google/profiler.h>
#endif

namespace esicup {

/// @brief Thread pool for instances to solve.
std::queue<std::string> instancesToSolve;
/// @brief Whether to turn accelerated scenario.
bool acceleratedScenario;
/// @brief Lock for the queue.
std::mutex queueLock;
/// @brief Lock for CLI output.
std::mutex outputLock;

void messageTestStarted(const std::string& pathToTest) {
    std::lock_guard<std::mutex> locker(outputLock);
    std::cout << "Started test: " << pathToTest << std::endl;
}

void messageMetadataCreated(const std::string& pathToTest) {
    std::lock_guard<std::mutex> locker(outputLock);
    std::cout << "Metadata created for test: " << pathToTest << std::endl;
}

void messageInputRead(const std::string& pathToTest) {
    std::lock_guard<std::mutex> locker(outputLock);
    std::cout << "Input read for test: " << pathToTest << std::endl;
}

void messagePreprocessingCompleted(const std::string& pathToTest) {
    std::lock_guard<std::mutex> locker(outputLock);
    std::cout << "Completed preprocessing for test: " << pathToTest << std::endl;
}

void messageProcessingCompleted(const std::string& pathToTest) {
    std::lock_guard<std::mutex> locker(outputLock);
    std::cout << "Completed processing for test: " << pathToTest << std::endl;
}

void messagePostprocessingCompleted(const std::string& pathToTest) {
    std::lock_guard<std::mutex> locker(outputLock);
    std::cout << "Completed postprocessing for test: " << pathToTest << std::endl;
}

void messageOutputWritten(const std::string& pathToTest) {
    std::lock_guard<std::mutex> locker(outputLock);
    std::cout << "Output written for test: " << pathToTest << std::endl;
}

void messageCPUTime(const std::string& pathToTest, double elapsed) {
    std::lock_guard<std::mutex> locker(outputLock);
    std::cout.precision(15); std::cout << std::fixed;
    std::cout << "CPU_TIME: " << pathToTest << " = " << elapsed << std::endl;
}


void solveInstance(const std::string& pathToTest) {
    // 1. Record start time
    struct timespec start, finish;
    double elapsed;
    clock_gettime(CLOCK_MONOTONIC, &start);
    messageTestStarted(pathToTest);

    // 2. Create test metadata
    TestMetadata testMetadata;
    testMetadata.accelerate = acceleratedScenario;
    testMetadata.pathToTest = pathToTest;
    messageMetadataCreated(pathToTest);

    // 3. Read input
    Input input;
    TestData testData = input.readInput(testMetadata);
    messageInputRead(pathToTest);

    // 4. Do preprocessing
    SmartPreprocessor preprocessor;
    SolutionInputData solutionInputData = preprocessor.prepare(testData);
    messagePreprocessingCompleted(pathToTest);

    // 5. Do processing
    CompositeProcessor processor(solutionInputData.context);
    #ifdef PROFILE_ON
    ProfilerStart("profiler.result");
    #endif
    SolutionOutputData solutionOutputData = processor.solve(solutionInputData);
    #ifdef PROFILE_ON
    ProfilerStop();
    #endif
    messageProcessingCompleted(pathToTest);

    // 6. Do postprocessing
    SolutionInstance answer;
    if (acceleratedScenario) {
        NullBinChooser postprocessor;
        answer = postprocessor.update(solutionOutputData, testData.context);
    } else {
        CompositePostprocessor postprocessor;
        answer = postprocessor.update(solutionOutputData, testData.context);
    }
    messagePostprocessingCompleted(pathToTest);

    // 7. Output answer
    Output output;
    TestOutputData outputData;
    outputData.solution = answer;
    outputData.testMetadata = testMetadata;
    outputData.context = testData.context;
    output.write(outputData);
    messageOutputWritten(pathToTest);

    // 8. Report CPU time
    clock_gettime(CLOCK_MONOTONIC, &finish);
    elapsed = (finish.tv_sec - start.tv_sec);
    const double CONVERT_NSEC_TO_SEC = 1000000000.0;
    elapsed += (finish.tv_nsec - start.tv_nsec) / CONVERT_NSEC_TO_SEC;
    messageCPUTime(pathToTest, elapsed);
}

std::string getTask() {
    std::string instanceLocation = "";
    std::lock_guard<std::mutex> locker(queueLock);
    if (!instancesToSolve.empty()) {
        instanceLocation = instancesToSolve.front();
        instancesToSolve.pop();
    }
    return instanceLocation;
}

void employee() {
    while (true) {
        std::string instanceLocation = getTask();
        if (instanceLocation.empty()) {
            return;
        }
        solveInstance(instanceLocation);
    }
}

void createTaskQueue(const InstancesData& data) {
    for (auto path : data.innerDirectories) {
        esicup::instancesToSolve.push(data.pathToSet + "/" + path);
    }
    acceleratedScenario = data.accelerate;
}

void parallelLaunch(const InstancesData& data) {
    createTaskQueue(data);
    const size_t THREAD_COUNT = 8;
    std::vector<std::thread> department;
    for (size_t idx = 0; idx < THREAD_COUNT; ++idx) {
        department.emplace_back(esicup::employee);
    }
    for (auto& employee : department) {
        employee.join();
    }
}


} // namespace esicup
