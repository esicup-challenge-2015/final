/**
 * @file schedule.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines API required for the parallel launch.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 * 
 */
#pragma once

#include "pipe.h"

#include <string>

namespace esicup {

/**
 * @brief Thread-safe message that test optimization has started.
 * 
 * @param pathToTest The path to test folder.
 */
void messageTestStarted(const std::string& pathToTest);
/**
 * @brief Thread-safe message that test metadata is created.
 * 
 * @param pathToTest The path to test folder.
 */
void messageMetadataCreated(const std::string& pathToTest);
/**
 * @brief Thread-safe message that input data is read.
 * 
 * @param pathToTest The path to test folder.
 */
void messageInputRead(const std::string& pathToTest);
/**
 * @brief Thread-safe message that preprocessing is complete.
 * 
 * @param pathToTest The path to test folder.
 */
void messagePreprocessingCompleted(const std::string& pathToTest);
/**
 * @brief Thread-safe message that processing is complete.
 * 
 * @param pathToTest The path to test folder.
 */
void messageProcessingCompleted(const std::string& pathToTest);
/**
 * @brief Thread-safe message that postprocessing is complete.
 * 
 * @param pathToTest The path to test folder.
 */
void messagePostprocessingCompleted(const std::string& pathToTest);
/**
 * @brief Thread-safe message that output is written.
 * 
 * @param pathToTest The path to test folder.
 */
void messageOutputWritten(const std::string& pathToTest);
/**
 * @brief Thread-safe message about CPU time for solving particular test.
 * 
 * @param pathToTest The path to test folder.
 */
void messageCPUTime(const std::string& pathToTest, double elapsed);

/**
 * @brief Parallel launch for the particular instance folder.
 * 
 * @param data The data for parallel launch.
 */
void parallelLaunch(const InstancesData& data);
/**
 * @brief Request to solve a particular instance.
 * 
 * @param pathToTest Path to folder with instance test input.
 */
void solveInstance(const std::string& pathToTest);
/**
 * @brief Get task from thread pool.
 * 
 * @return Path to test input folder.
 */
std::string getTask();
/**
 * @brief Entity function for each particular thread.
 * Solves instances while those are available.
 */
void employee();
/**
 * @brief Create a queue for arranging tasks into threads.
 * 
 * @param data The set of instances to solve with thread pool.
 */
void createTaskQueue(const InstancesData& data);


} // namespace esicup