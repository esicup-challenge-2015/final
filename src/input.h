/**
 * @file input.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines interface and implementation for Input class - entity
 * designed to read instance-specific input information.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "interfaces.h"

namespace esicup {

/**
 * @brief Interface to Input - entity designed to read 
 * instance-specific input information.
 */
class IInput {
public:
    /**
     * @brief Reads instance-specific input information.
     * 
     * @param input The metadata describing the path to the instance folder.
     * 
     * @return The read input instance. 
     */
    virtual TestData readInput(const TestMetadata& input) = 0;
    /**
     * @brief Virtual destructor for correct C++ polymorphism.
     */
    virtual ~IInput() {};
};

/**
 * @brief Input is an entity designed to read 
 * instance-specific input information.
 */
class Input : public IInput {
public:
    /**
     * @brief Reads instance-specific input information.
     * 
     * @param input The metadata describing the path to the instance folder.
     * 
     * @return The read input instance.
     */
    virtual TestData readInput(const TestMetadata& input) override;
    /**
     * @brief Virtual destructor for correct C++ polymorphism.
     */
    virtual ~Input() {};

public:
    /// @brief Describes suffix for building paths to bin files.
    static const std::string binsFileSuffix;
    /// @brief Describes suffix for building paths to items files.
    static const std::string itemsFileSuffix;
    /// @brief Describes suffix for builsing paths to parameters files.
    static const std::string parametersFileSuffix;

private:
    /**
     * @brief Reads items from item-specific file.
     * 
     * @param context The pointer to instance-specific context tracking input information.
     * @param filePath Path to the item-specific file.
     */
    void readItems(ContextPtr context, const std::string& filePath);
    /**
     * @brief Reads bins from bin-specific file.
     * 
     * @param context The pointer to instance-specific context tracking input information.
     * @param filePath Path to bin-specific file.
     */
    void readBins(ContextPtr context, const std::string& filePath);
    /**
     * @brief Reads parameters from the corresponding file.
     * 
     * @param context The pointer to instance-specific context tracking input information.
     * @param filePath Path to the files with parameters.
     */
    void readParameters(ContextPtr context, const std::string& filePath);
    /**
     * @brief Converts string material description to corresponding class enumeration value.
     * 
     * @param material The materials description in string format.
     * @return Material enumeration class value.
     */
    Material extractMaterialFromString (const std::string& material);
    /**
     * @brief Converts string position description to corresponding class enumeration value.
     * 
     * @param position The position description in string format.
     * @return Position enumeration class value.
     */
    Position extractPositionFromString(const std::string& position);
};
} // namespace esicup

