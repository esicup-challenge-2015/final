/// Implements structures and API defined in metric.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "metric.h"
#include "filler2d/filler2d.h"

#include <algorithm>
#include <iostream>
#include <memory>
#include <limits>


namespace esicup {

const long double INF = std::numeric_limits<double>::max();

IMetric::IMetric(const ConstContextPtr& aContext)
    : context(aContext) {
}


UsedBinVolumeMetric::UsedBinVolumeMetric(
        const ConstContextPtr& context,
        bool applyLowerBound)
    : IMetric(context)
    , useLowerBound(applyLowerBound) {
}

long double UsedBinVolumeMetric::calculate(const PartialData& solution) const {
    SolutionStats stats = getStatistics(solution, context);
    long double sumVolume = useLowerBound ? lowerBoundEstimation(stats) : 0.;
    sumVolume += stats.maxPossibleVolume;
    return stats.lastBinIsEmpty ? INF : sumVolume;
}

long double UsedBinVolumeMetric::lowerBoundEstimation(
        const SolutionStats& stats) const {
    double maxAllowedWeight = context->getMaxAllowedBinWeight();
    unsigned long long minBinVolume = context->getMinBinVolume();
    long double weightEstimation = (stats.residualWeight / maxAllowedWeight) * minBinVolume;
    return std::max(stats.residualVolume, weightEstimation);
}


DensityVolumeMetric::DensityVolumeMetric(
        const ConstContextPtr& context,
        bool useLowerBound,
        bool useAllBins)
    : IMetric(context)
    , useLowerBound(useLowerBound)
    , useAllBins(useAllBins) {
}

long double DensityVolumeMetric::calculate(const PartialData& solution) const {
    SolutionStats stats = getStatistics(solution, context);
    if (stats.lastBinIsEmpty) {
        return INF;
    }
    if (!useAllBins) {
        return -stats.lastBinUsedVolume / stats.lastBinMaxVolume;
    }
    if (!useLowerBound) {
        return -stats.usedVolume / stats.maxPossibleVolume;
    } else {
        return -(stats.usedVolume + stats.residualVolume) /
            (stats.maxPossibleVolume + stats.residualVolume);
    }
}


DensityWeightMetric::DensityWeightMetric(
        const ConstContextPtr& context,
        bool useLowerBound,
        bool useAllBins)
    : IMetric(context)
    , useLowerBound(useLowerBound)
    , useAllBins(useAllBins) {
}

long double DensityWeightMetric::calculate(
        const PartialData& solution) const {
    SolutionStats stats = getStatistics(solution, context);
    if (stats.lastBinIsEmpty) {
        return INF;
    }
    if (!useAllBins) {
        return -stats.lastBinUsedWeight / stats.lastBinMaxWeight;
    }
    if (!useLowerBound) {
        return -stats.usedWeight / stats.maxPossibleWeight;
    } else {
        return -(stats.usedWeight + stats.residualWeight) /
            (stats.maxPossibleWeight + stats.residualWeight);
    }
}


DensityAreaMetric::DensityAreaMetric(
        const ConstContextPtr& context,
        bool useAllBins)
    : IMetric(context)
    , useAllBins(useAllBins) {
}

long double DensityAreaMetric::calculate(
        const PartialData& solution) const {
    SolutionStats stats = getStatistics(solution, context);
    if (stats.lastBinIsEmpty) {
        return INF;
    }
    if (!useAllBins) {
        return -stats.lastBinUsedArea / stats.lastBinMaxArea;
    }

    return -stats.usedArea / stats.maxPossibleArea;
}


long double getAverageResult(
        const PartialData& solution,
        bool useLowerBound,
        long double accumulatedSum,
        ConstContextPtr context) {
    SolutionStats stats = getStatistics(solution, context);
    if (stats.lastBinIsEmpty) {
        return INF;
    }
    if (!useLowerBound) {
        return -accumulatedSum / solution.partialSolution.size();
    } else {
        return -(accumulatedSum + solution.currentGroups.groups.size()) /
            (solution.currentGroups.groups.size() + solution.partialSolution.size());
    }
}


AverageDensityVolumeMetric::AverageDensityVolumeMetric(
        const ConstContextPtr& context,
        bool useLowerBound)
    : IMetric(context)
    , useLowerBound(useLowerBound)  {
}

long double AverageDensityVolumeMetric::calculate(const PartialData& solution) const {
    long double accumulatedSum = 0.0L;
    for (const auto& target : solution.partialSolution) {
        accumulatedSum += static_cast<long double>(target->getBin().getUsedVolume()) /
            static_cast<long double>(target->getBin().getVolume());
    }
    return getAverageResult(solution, useLowerBound, accumulatedSum, context);
}


AverageDensityWeightMetric::AverageDensityWeightMetric(
        const ConstContextPtr& context,
        bool useLowerBound)
    : IMetric(context)
    , useLowerBound(useLowerBound)  {
}

long double AverageDensityWeightMetric::calculate(const PartialData& solution) const {
    long double accumulatedSum = 0.0L;
    for (const auto& target : solution.partialSolution) {
        accumulatedSum += static_cast<long double>(target->getBin().getWeight()) /
            static_cast<long double>(target->getBin().getMaximalAllowedWeight());
    }
    return getAverageResult(solution, useLowerBound, accumulatedSum, context);
}


AverageDensityAreaMetric::AverageDensityAreaMetric(
        const ConstContextPtr& context,
        bool useLowerBound)
    : IMetric(context)
    , useLowerBound(useLowerBound)  {
}

long double AverageDensityAreaMetric::calculate(const PartialData& solution) const {
    long double accumulatedSum = 0.0L;
    for (const auto& target : solution.partialSolution) {
        accumulatedSum += static_cast<long double>(target->getBin().getUsedArea()) /
            target->getBin().getBoxArea();
    }
    return getAverageResult(solution, useLowerBound, accumulatedSum, context);
}


MetricFactory::MetricFactory(const ConstContextPtr& instanceContext)
    : context(instanceContext) {
}

IMetricPtr MetricFactory::createMetric(const MetricType& metricType) const {
    switch (metricType) {
        case MetricType::USED_BIN_VOLUME: {
            return std::make_shared<UsedBinVolumeMetric>(context, false);
        } 
        case MetricType::USED_BIN_VOLUME_BOUND: {
            return std::make_shared<UsedBinVolumeMetric>(context, true);
        }
        case MetricType::DENSITY_VOLUME: {
            return std::make_shared<DensityVolumeMetric>(context, false, true);
        }
        case MetricType::DENSITY_VOLUME_LAST: {
            return std::make_shared<DensityVolumeMetric>(context, false, false);
        }
        case MetricType::DENSITY_VOLUME_BOUND: {
            return std::make_shared<DensityVolumeMetric>(context, true, true);
        }
        case MetricType::DENSITY_WEIGHT: {
            return std::make_shared<DensityWeightMetric>(context, false, true);
        }
        case MetricType::DENSITY_WEIGHT_LAST: {
            return std::make_shared<DensityWeightMetric>(context, false, false);
        }
        case MetricType::DENSITY_WEIGHT_BOUND: {
            return std::make_shared<DensityWeightMetric>(context, true, true);
        }
        case MetricType::DENSITY_AREA: {
            return std::make_shared<DensityAreaMetric>(context, true);
        }
        case MetricType::DENSITY_AREA_LAST: {
            return std::make_shared<DensityAreaMetric>(context, false);
        }
        case MetricType::AVERAGE_DENSITY_VOLUME: {
            return std::make_shared<AverageDensityVolumeMetric>(context, false);
        }
        case MetricType::AVERAGE_DENSITY_VOLUME_BOUND: {
            return std::make_shared<AverageDensityVolumeMetric>(context, true);
        }
        case MetricType::AVERAGE_DENSITY_WEIGHT: {
            return std::make_shared<AverageDensityWeightMetric>(context, false);
        }
        case MetricType::AVERAGE_DENSITY_WEIGHT_BOUND: {
            return std::make_shared<AverageDensityWeightMetric>(context, true);
        }
        case MetricType::AVERAGE_DENSITY_AREA: {
            return std::make_shared<AverageDensityWeightMetric>(context, false);
        }
        case MetricType::AVERAGE_DENSITY_AREA_BOUND: {
            return std::make_shared<AverageDensityWeightMetric>(context, true);
        }
        case MetricType::NO_METRIC: {
            return nullptr;
        }
        default: {
            throw std::logic_error("Unexpected metric!");
        }
    }
}

} // namespace esicup
