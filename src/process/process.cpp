/// Implements classes and API defined in process.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "process.h"
#include "../postprocess/postprocess.h"
#include "filler2d/filler2d.h"
#include "filler2d/factory.h"

#include <algorithm>
#include <stdexcept>
#include <vector>
#include <utility>
#include <memory>
#include <queue>
#include <iostream>
#include <thread>
#include <mutex>


namespace esicup {

std::vector<long double> makeQuorumThresholds(
        const VectorMetric& metricOptions, 
        const GeneticData& solutions, 
        size_t quorumCount) {
    std::vector<long double> result;
    for (auto& metric : metricOptions) {
        std::priority_queue<long double> scores;
        for (const auto& solution : solutions.currentSolutions) {
            scores.push(metric->calculate(solution));
            if (scores.size() > quorumCount) {
                scores.pop();
            }
        }
        while (scores.size() > quorumCount) {
            scores.pop();
        }
        if (scores.size()) {
            result.push_back(scores.top());
        } else {
            result.push_back(0);
        }
    }
    return result;
}

GeneticSearch::GeneticSearch(
        std::shared_ptr<IIterationStrategy> iterationStrategy,
        std::vector<std::shared_ptr<IMetric>> metricsVector,
        size_t childCount)
    : selectCount(childCount)
    , strategy(iterationStrategy)
    , metrics(metricsVector) {
}

SolutionOutputData GeneticSearch::solve(const SolutionInputData& data) {
    makeGeneticData(data);
    do {
        iterate();
        select();
    } while (!finish());
    return makeOutput();
}

void GeneticSearch::makeGeneticData(const SolutionInputData& data) {
    solutions.currentSolutions.clear();
    finalizedSolutions.currentSolutions.clear();
    context = data.context;
    for (auto groupOrder : data.orders) {
        PartialData partialSolution;
        partialSolution.currentGroups = groupOrder;
        solutions.currentSolutions.push_back(partialSolution);
    }
}

void GeneticSearch::iterate() {
    solutions = strategy->iterate(solutions);
}


void GeneticSearch::select() {
    if (solutions.currentSolutions.size() <= selectCount) {
        GeneticData clearSolutions;
        for (auto& solution : solutions.currentSolutions) {
            if (!solution.currentGroups.groups.size()) {
                finalizedSolutions.currentSolutions.push_back(solution);
            } else {
                clearSolutions.currentSolutions.push_back(solution);
            }
        }
        solutions = clearSolutions;
        return;
    }

    size_t metricCount = metrics.size();

    std::vector<long double> borders = makeQuorumThresholds(
        metrics, solutions, selectCount);
    std::vector<size_t> storedSolutionsByMetric(metricCount);
    GeneticData selectedSolutions;

    for (const auto& solution : solutions.currentSolutions) {
        if (!solution.currentGroups.groups.size()) {
            finalizedSolutions.currentSolutions.push_back(solution);
            continue;
        }
        bool notUpdated = true;
        const long double EPS = 1e-9, INF = 1e90;
        for (size_t idx = 0; idx < metricCount && notUpdated; ++idx) {
            long double value = metrics[idx]->calculate(solution);
            if (value < INF && (value + EPS  < borders[idx] 
                    || (fabsl(value - borders[idx]) < EPS 
                    && storedSolutionsByMetric[idx] < selectCount))) {
                selectedSolutions.currentSolutions.push_back(solution);
                ++storedSolutionsByMetric[idx];
                notUpdated = false;
            }
        }
    }
    solutions = selectedSolutions;
}

bool GeneticSearch::finish() {
    const size_t MAX_COUNT = 3000;
    return (solutions.currentSolutions.size() == 0) ||
        (finalizedSolutions.currentSolutions.size() > MAX_COUNT);
}

SolutionOutputData GeneticSearch::makeOutput() const {
    SolutionOutputData answer;
    for (const auto& solution : finalizedSolutions.currentSolutions) {
        SolutionInstance result;
        result.bins = solution.getBins();
        answer.solutions.push_back(result);
    }
    return answer;
}

CompositeProcessor::CompositeProcessor(ConstContextPtr context, bool reduce)
        : reduce(reduce) {
    FillerFactory factory(context);
    size_t binCount = context->getBinTypesCount();
    std::vector<size_t> binTypes;
    for (size_t type = 0; type < binCount; ++type) {
        binTypes.push_back(type);
    }

    std::vector<std::pair<FillerType, LayerStrategyType>> filler2DSpecifications = {
         {FillerType::STRAIGHT_FILLER_NFYX, LayerStrategyType::MINIMAL_LOST_AREA},
         {FillerType::STRAIGHT_FILLER_NFXY, LayerStrategyType::MINIMAL_LOST_AREA},
         {FillerType::GUILOTINE_BLSF_MINAS, LayerStrategyType::MINIMAL_LOST_AREA},
         {FillerType::GUILOTINE_BSSF_MINAS, LayerStrategyType::MINIMAL_LOST_AREA},
         {FillerType::ROUND_FILLER_CENTER, LayerStrategyType::MINIMAL_LOST_AREA},
         {FillerType::ROUND_FILLER_CORNER, LayerStrategyType::MINIMAL_LOST_AREA},
    };

    if (!context->isAccelerated()) {
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::SHELF_FILLER_BHF_L, LayerStrategyType::MINIMAL_LOST_AREA));
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::SHELF_FILLER_BHF_W, LayerStrategyType::MINIMAL_LOST_AREA));
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::SHELF_FILLER_BWF_L, LayerStrategyType::MINIMAL_LOST_AREA));
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::SHELF_FILLER_BWF_W, LayerStrategyType::MINIMAL_LOST_AREA));
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::SHELF_FILLER_FF_L, LayerStrategyType::MINIMAL_LOST_AREA));
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::SHELF_FILLER_FF_W, LayerStrategyType::MINIMAL_LOST_AREA));
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::GUILOTINE_BAF_MINAS, LayerStrategyType::MINIMAL_LOST_AREA));
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::GUILOTINE_BLSF_SAS, LayerStrategyType::MINIMAL_LOST_AREA));
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::GUILOTINE_BLSF_LLAS, LayerStrategyType::MINIMAL_LOST_AREA));
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::GUILOTINE_BLSF_SLAS, LayerStrategyType::MINIMAL_LOST_AREA));
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::GUILOTINE_BSSF_SAS, LayerStrategyType::MINIMAL_LOST_AREA));
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::GUILOTINE_BSSF_LLAS, LayerStrategyType::MINIMAL_LOST_AREA));
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::GUILOTINE_BSSF_SLAS, LayerStrategyType::MINIMAL_LOST_AREA));
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::ROUND_FILLER_CORNER_BKW, LayerStrategyType::MINIMAL_LOST_AREA));
        filler2DSpecifications.push_back(std::make_pair<FillerType, LayerStrategyType>(
            FillerType::ROUND_FILLER_CENTER_BKW, LayerStrategyType::MINIMAL_LOST_AREA));
    }

    MetricFactory metricFactory(context);
    VectorMetric selectedMetrics;

    auto startType = MetricType::FIRST_REGULAR;
    auto finishType = MetricType::FINAL_REGULAR;

    if (context->isAccelerated()) {
        startType = MetricType::FIRST_ACCELERATED;
        finishType = MetricType::FINAL_ACCELERATED;
    }

    // Launch with single metric.
    for (MetricType metricType = startType; metricType != finishType;
            metricType = static_cast<MetricType>(1 + static_cast<int>(metricType))) {
        std::shared_ptr<IMetric> metric = metricFactory.createMetric(metricType);
        selectedMetrics.emplace_back(metric);

        if (!context->isAccelerated()) {
            std::shared_ptr<IIterationStrategy> singleStrategy(
                new LastBinClosingStrategy(
                    binTypes, filler2DSpecifications, factory, GroupManager::SINGLE, context));
            processors.push_back(std::unique_ptr<IProcessor>(
                new GeneticSearch(singleStrategy, VectorMetric{metric}, 1)));
        }

        std::shared_ptr<IIterationStrategy> maxAreaStrategy(
            new LastBinClosingStrategy(
                binTypes, filler2DSpecifications, factory, GroupManager::MAXAREA, context));
        processors.push_back(std::unique_ptr<IProcessor>(
                new GeneticSearch(maxAreaStrategy, VectorMetric{metric}, 1)));
        
        if (!context->isAccelerated()) {
            std::shared_ptr<IIterationStrategy> maxWidthdStrategy(
                new LastBinClosingStrategy(
                    binTypes, filler2DSpecifications, factory, GroupManager::MAXWIDTH, context));
            processors.push_back(std::unique_ptr<IProcessor>(
                new GeneticSearch(maxWidthdStrategy, VectorMetric{metric}, 1)));
        }
    }

    // Launch with all metrics.
    std::shared_ptr<IIterationStrategy> singleStrategy(
        new LastBinClosingStrategy(
            binTypes, filler2DSpecifications, factory, GroupManager::SINGLE, context));
    processors.push_back(
        std::unique_ptr<IProcessor>(new GeneticSearch(singleStrategy, selectedMetrics, 1)));

    if (!context->isAccelerated()) {
        std::shared_ptr<IIterationStrategy> maxAreaStrategy(
            new LastBinClosingStrategy(
                binTypes, filler2DSpecifications, factory, GroupManager::MAXAREA, context));
        processors.push_back(std::unique_ptr<IProcessor>(
            new GeneticSearch(maxAreaStrategy, selectedMetrics, 1)));
    }
    
    std::shared_ptr<IIterationStrategy> maxWidthStrategy(
        new LastBinClosingStrategy(
            binTypes, filler2DSpecifications, factory, GroupManager::MAXWIDTH, context));
    processors.push_back(std::unique_ptr<IProcessor>(
        new GeneticSearch(maxWidthStrategy, selectedMetrics, 1)));
}

SolutionOutputData CompositeProcessor::solve(const SolutionInputData& sourceData)  {
    SolutionOutputData result;
    for (size_t orderId = 0; orderId <= sourceData.orders.size(); ++orderId) {
        SolutionInputData data;
        if (orderId < sourceData.orders.size()) {
            const auto& oneInput = sourceData.orders[orderId];
            data.orders.push_back(oneInput);
        } else {
            data = sourceData;
        }
        for (const auto& worker : processors) {
            auto workerResult = worker->solve(data);
            result.solutions.insert(
                    result.solutions.end(),
                    workerResult.solutions.begin(),
                    workerResult.solutions.end());
            const size_t BUFFER_SIZE = 300;
            if (result.solutions.size() > BUFFER_SIZE) {
                if (reduce) {
                    CompositePostprocessor postprocessor;
                    auto selectedInstance = postprocessor.update(result, sourceData.context);
                    result.solutions.clear();
                    result.solutions.push_back(selectedInstance);
                } else {
                    SwapStackPostprocessor postprocessor;
                    auto selectedInstance = postprocessor.update(result, sourceData.context);
                    result.solutions.clear();
                    result.solutions.push_back(selectedInstance);
                }
            }
        }
    }
    return result;
}

} // namespace esicup

