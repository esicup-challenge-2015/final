/**
 * @file straight.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines straightforward fillers: NFXY and NFYX.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */

#pragma once

#include "filler2d.h"
#include "options.h"

namespace esicup {

/**
 * @brief Straightforward NFYX filler adds 
 * stacks along Y-axis (by width) gradually 
 * moving along X-axis (by length).
 */
class StraightforwardFillerNFYX final : public IFiller2D {
public:
    /**
     * @brief Constructor for Straightforward NFYX filler.
     * 
     * @param context  The context with instance input information.
     * @param binType  The index of the bin type.
     * @param type     The type of the filler for self-identification.
     * @param strategy The type of the layer strategy.
     */
    StraightforwardFillerNFYX(ConstContextPtr context,
            size_t binType,
            const FillerType& type,
            LayerStrategyPtr strategy);
    /**
     * @brief Tries to find a position for loading a stack.
     * Returns whether the search is successful.
     * 
     * @param stack The stack candidate for loading.
     * 
     * @return True if the position was found, False otherwise.
     */
    bool ifCanPutStack(const Stack& stack) const override;
    /**
     * @brief Puts stack inside the bin. Assumes that
     * ifCanPutStack returns True.
     * 
     * @param stack The stack to put inside the bin.
     */
    void putStack(const Stack& stack) override;
    /**
     * @brief Clones the filler with inner state.
     * 
     * @return The smart pointer to the cloned filler.
     */
    IFiller2DPtr clone() const override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~StraightforwardFillerNFYX() override;

private:
    /**
     * @brief Finds position for bringing stack inside the bin.
     * 
     * @param stack The stack to bring inside the bin.
     * @param putWidthShift  The place to write the width shift of the position origin.
     * @param putLengthShift The place to write the length shift of the position origin.
     * @return True if position is found, False otherwise.
     */
    bool findPutPosition(
            const Stack& stack,
            size_t* putWidthShift,
            size_t* putLengthShift) const;
    /**
     * @brief Recalculates the shift positions for the next stack, if the
     * stack provided in arguments is loaded.
     * 
     * @param stack The stack recently loaded.
     * @param putWidthShift Width shift of stack position origin.
     * @param putLengthShift Length shift of stack position origin.
     */
    void recalculateNextShifts(
        const Stack& stack, size_t putWidthShift, size_t putLengthShift);

private:
    /// @brief Shift to the next position for stack loading by width.
    size_t nextWidthShift;
    /// @brief Shift to the next position for stack loading by length.
    size_t nextLengthShift;
    /// @brief Shift to the next shelf row (by length).
    size_t nextShelfShift; 
};


/**
 * @brief Straightforward NFYX filler adds stacks along X-axis (by length)
 * gradually moving along Y-axis (by width).
 */
class StraightforwardFillerNFXY final : public IFiller2D {
public:
    /**
     * @brief Constructor for Straightforward NFYX filler.
     * 
     * @param context  The context with instance input information.
     * @param binType  The index of the bin type.
     * @param type     The type of the filler for self-identification.
     * @param strategy The type of the layer strategy.
     */
    StraightforwardFillerNFXY(ConstContextPtr context,
            size_t binType,
            const FillerType& type,
            LayerStrategyPtr strategy);
    /**
     * @brief Tries to find a position for loading a stack.
     * Returns whether the search is successful.
     * 
     * @param stack The stack candidate for loading.
     * 
     * @return True if the position was found, False otherwise.
     */
    bool ifCanPutStack(const Stack& stack) const override;
    /**
     * @brief Puts stack inside the bin. Assumes that
     * ifCanPutStack returns True.
     * 
     * @param stack The stack to put inside the bin.
     */
    void putStack(const Stack& stack) override;
    /**
     * @brief Clones the filler with inner state.
     * 
     * @return The smart pointer to the cloned filler.
     */
    IFiller2DPtr clone() const override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~StraightforwardFillerNFXY() override;

private:
    /**
     * @brief Finds position for bringing stack inside the bin.
     * 
     * @param stack The stack to bring inside the bin.
     * @param putWidthShift  The place to write the width shift of the position origin.
     * @param putLengthShift The place to write the length shift of the position origin.
     * 
     * @return True if position is found, False otherwise.
     */
    bool findPutPosition(const Stack& stack,
            size_t* putWidthShift,
            size_t* putLengthShift) const;
    /**
     * @brief Recalculates the shift positions for the next stack, if the
     * stack provided in arguments is loaded.
     * 
     * @param stack The stack recently loaded.
     * @param putWidthShift Width shift of stack position origin.
     * @param putLengthShift Length shift of stack position origin.
     */
    void recalculateNextShifts(
        const Stack& stack, size_t putWidthShift, size_t putLengthShift);

private:
    /// @brief Shift to the next stack loading position by width.
    size_t nextWidthShift; 
    /// @brief Shift to the next stack loading position by length.
    size_t nextLengthShift; 
    /// @brief Shift to the next shelf column (by width).
    size_t nextShelfShift;
};


} // namespace esicup