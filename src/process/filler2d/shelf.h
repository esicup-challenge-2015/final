/**
 * @file shelf.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines and Implements ShelfFiller - filler that loads stacks shelf-by-shelf.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */

#pragma once
#include "filler2d.h"

namespace esicup {

/**
 * @brief Describes the mechanism to select shelf for loading a stack.
 */
enum class ShelfSelectionRule {
    FF,  ///< First shelf that fits. 
    BWF, ///< With minimal length for lengthwise, with minimal width for widthwise.
    BHF, ///< With minimal width for lengthwise, with minimal length for widthwise
};

/**
 * @brief Shelf fillers packs stacks inside shelfs.
 */
class ShelfFiller final : public IFiller2D {
public:
    /**
     * @brief Constructs Shelf filler. The shelf filler packs stacks inside shelfs.
     * 
     * @param context The context describing instance summary.
     * @param binType The type of the bin to pack inside.
     * @param fillerType Type of the filler to store for self-recognition.
     * @param strategy The strategy for building laers.
     * @param rule The strategy to select currently updated shelf.
     * @param position The position of the shelfs (widthwise or lengthwise).
     */
    ShelfFiller(ConstContextPtr context, size_t binType,
        const FillerType& fillerType, LayerStrategyPtr strategy,
        const ShelfSelectionRule& rule, const Position& position);
    /**
     * @brief Verifies if the filler can put stack inside the bin.
     * 
     * @param stack The stack to load inside the bin.
     * 
     * @return True if the option is found, and False otherwise. 
     */
    bool ifCanPutStack(const Stack& stack) const override;
    /**
     * @brief Loads stack inside the bin. Assumes that 
     * ifCanPutStack returns True.
     * 
     * @param stack The stack to load inside.
     */
    void putStack(const Stack& stack) override;
    /**
     * @brief Clones filler with inner data structures.
     * 
     * @return Pointer to the cloned filler.
     */
    IFiller2DPtr clone() const override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~ShelfFiller() override;

private:
    /**
     * @brief Describes a shelf for packing.
     */
    struct ShelfCoordinates {
        /// @brief Shelf width.
        size_t width; 
        /// @brief Shelf length.
        size_t length;
        /// @brief Shelf shift.
        size_t shift;
    };

private:
    /// @brief Iterator fo vector with Shelf Coordinates.
    typedef std::vector<ShelfCoordinates>::iterator Iterator;

private:
    /**
     * @brief General method for deciding on the shelf index. Selects 
     * the right method according to selectionRule class member.
     * 
     * @param stack The stack that needs packing.
     * @return The index of the selected shelf.
     */
    size_t decideShelfMethod(const Stack& stack);
    /**
     * @brief Decides on the shelf index according to FF policy.
     * 
     * @param stack The stack that needs packing.
     * @return The index of the selected shelf.
     */
    size_t decideShelfWithFF(const Stack& stack);
    /**
     * @brief Decides on the shelf index according to BWF policy.
     * 
     * @param stack The stack that needs packing.
     * @return The index of the selected shelf.
     */
    size_t decideShelfWithBWF(const Stack& stack);
    /**
     * @brief Decides on the shelf index according to BHF policy.
     * 
     * @param stack The stack that needs packing.
     * @return The index of the selected shelf.
     */
    size_t decideShelfWithBHF(const Stack& stack);
    /**
     * @brief Adds new shelf.
     */
    void addNewShelf();
    /**
     * @brief Verifies if the algorithm has an option to load the stack 
     * onto a shelf.
     * 
     * @param stack The stack that needs loading.
     * @param index The index of the shelf.
     * @param isLast Whether the allocation is to the last shelf.
     * 
     * @return True if there is such an option found, and False otherwise.
     */
    bool ifCanPutStackIntoShelf(const Stack& stack, size_t index, bool isLast) const;
    /**
     * @brief Verifies if the algorithm has an option to load the stack lengthwise.
     * 
     * @param stack The stack to load.
     * 
     * @return True if an option is found, and False otherise.
     */
    bool ifCanPutStackLengthwise(const Stack& stack) const;
    /**
     * @brief Verifies if the algorithm has an option to load the stack widthwise.
     * 
     * @param stack The stack to load.
     * 
     * @return True if an option is found, and False otherwise.
     */
    bool ifCanPutStackWidthwise(const Stack& stack) const;

private:
    /// @brief The vector with shelf coordinates.
    std::vector<ShelfCoordinates> shelfs;
    /// @brief Position of the shelfs.
    const Position shelfPosition;
    /// @brief Selection method for the shelf.
    const ShelfSelectionRule selectionRule;
};

} // namespace esicup