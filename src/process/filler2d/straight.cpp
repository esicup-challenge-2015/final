/// Implements API defined in straight.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "straight.h"

#include <memory>
#include <cassert>
#include <algorithm>

namespace esicup {


StraightforwardFillerNFYX::StraightforwardFillerNFYX(
        ConstContextPtr context,
        size_t binType,
        const FillerType& type,
        LayerStrategyPtr strategy)
        : IFiller2D(context, binType, type, strategy)
        , nextWidthShift(0)
        , nextLengthShift(0)
        , nextShelfShift(0) {
}

bool StraightforwardFillerNFYX::ifCanPutStack(const Stack& stack) const {
    size_t putWidthShift = 0, putLengthShift = 0;
    return findPutPosition(stack, &putWidthShift, &putLengthShift);
}

void StraightforwardFillerNFYX::putStack(const Stack& stack) {
    size_t putWidthShift = 0, putLengthShift = 0;
    assert(findPutPosition(stack, &putWidthShift, &putLengthShift));
    bin->carry(stack, putWidthShift, putLengthShift);
    recalculateNextShifts(stack, putWidthShift, putLengthShift);
}

IFiller2DPtr StraightforwardFillerNFYX::clone() const {
    auto fillerPointer = new StraightforwardFillerNFYX(
        context, binType, type, strategy);
    fillerPointer->nextWidthShift = nextWidthShift;
    fillerPointer->nextLengthShift = nextLengthShift;
    fillerPointer->nextShelfShift = nextShelfShift;
    fillerPointer->bin = std::shared_ptr<Bin>(new Bin(*bin));
    return IFiller2DPtr(fillerPointer);
}

StraightforwardFillerNFYX::~StraightforwardFillerNFYX() {}

bool StraightforwardFillerNFYX::findPutPosition(
        const Stack& stack,
        size_t* putWidthShift,
        size_t* putLengthShift) const {
    if (bin->ifCanPut(stack, nextWidthShift, nextLengthShift)) {
        *putWidthShift = nextWidthShift;
        *putLengthShift = nextLengthShift;
        return true;
    }
    if (bin->ifCanPut(stack, 0, nextLengthShift + nextShelfShift)) {
        *putWidthShift = 0;
        *putLengthShift = nextLengthShift + nextShelfShift;
        return true;
    }
    return false;
}

void StraightforwardFillerNFYX::recalculateNextShifts(
        const Stack& stack, size_t putWidthShift, size_t putLengthShift) {
    if (putWidthShift == 0) {
        nextShelfShift = 0;
    }
    nextWidthShift = putWidthShift + stack.getWidth();
    nextLengthShift = putLengthShift;
    nextShelfShift = std::max(nextShelfShift, stack.getLength());
}


StraightforwardFillerNFXY::StraightforwardFillerNFXY(
        ConstContextPtr context,
        size_t binType,
        const FillerType& type,
        LayerStrategyPtr strategy)
        : IFiller2D(context, binType, type, strategy)
        , nextWidthShift(0)
        , nextLengthShift(0)
        , nextShelfShift(0) {
}

bool StraightforwardFillerNFXY::ifCanPutStack(const Stack& stack) const {
    size_t putWidthShift, putLengthShift;
    auto similarStack = Stack::buildSimilar(stack).front();
    return findPutPosition(similarStack, &putWidthShift, &putLengthShift);
}

void StraightforwardFillerNFXY::putStack(const Stack& stack) {
    auto similarStack = Stack::buildSimilar(stack).front();
    size_t putWidthShift = 0, putLengthShift = 0;
    assert(findPutPosition(similarStack, &putWidthShift, &putLengthShift));
    bin->carry(similarStack, putWidthShift, putLengthShift);
    recalculateNextShifts(similarStack, putWidthShift, putLengthShift);
}

IFiller2DPtr StraightforwardFillerNFXY::clone() const {
    auto fillerPointer = new StraightforwardFillerNFXY(context, binType, type, strategy);
    fillerPointer->nextWidthShift = nextWidthShift;
    fillerPointer->nextLengthShift = nextLengthShift;
    fillerPointer->nextShelfShift = nextShelfShift;
    fillerPointer->bin = std::shared_ptr<Bin>(new Bin(*bin));
    return IFiller2DPtr(fillerPointer);
}

StraightforwardFillerNFXY::~StraightforwardFillerNFXY() {}

bool StraightforwardFillerNFXY::findPutPosition(
        const Stack& stack,
        size_t* putWidthShift,
        size_t* putLengthShift) const {
    if (bin->ifCanPut(stack, nextWidthShift, nextLengthShift)) {
        *putWidthShift = nextWidthShift;
        *putLengthShift = nextLengthShift;
        return true;
    }
    if (bin->ifCanPut(stack, nextWidthShift + nextShelfShift, 0)) {
        *putWidthShift = nextWidthShift + nextShelfShift;
        *putLengthShift = 0;
        return true;
    }
    return false;
}

void StraightforwardFillerNFXY::recalculateNextShifts(
        const Stack& stack, size_t putWidthShift, size_t putLengthShift) {
    if (putLengthShift == 0) {
        nextShelfShift = 0;
    }
    nextWidthShift = putWidthShift;
    nextLengthShift = putLengthShift + stack.getLength();
    nextShelfShift = std::max(nextShelfShift, stack.getWidth());
}

} // namespace esicup