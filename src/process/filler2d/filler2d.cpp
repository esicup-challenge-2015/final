/// Implements API defined in filler2d.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "filler2d.h"

#include <memory>
#include <algorithm>
#include <vector>
#include <string>
#include <cassert>
#include <iostream>


namespace esicup {

IFiller2D::IFiller2D(
        ConstContextPtr instanceContext,
        size_t idxBinType,
        const FillerType& fillerType,
        LayerStrategyPtr layerStrategy)
        : type(fillerType)
        , binType(idxBinType)
        , strategy(layerStrategy)
        , context(instanceContext) {
    bin = std::shared_ptr<Bin>(new Bin(idxBinType, instanceContext));
}

bool IFiller2D::ifCanPutLayer(Layer* layer, size_t* stackToPut, bool* willFinalize) const {
    return strategy->ifCanPut(bin, layer, willFinalize, stackToPut);
}

void IFiller2D::putLayer(const Layer& layer, size_t stackToPut) {
    strategy->carry(layer, stackToPut, bin);
}

FillerType IFiller2D::getFiller2DType() const {
    return type;
}

LayerStrategyType IFiller2D::getLayerStrategyType() const {
    return strategy->getType();
}

const Bin& IFiller2D::getBin() const {
    return *bin;
}

IFiller2D::~IFiller2D() {}

} // namespace esicup

