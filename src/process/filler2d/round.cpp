/// Implements API defined in round.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include  "round.h"

#include <algorithm>
#include <cassert>
#include <memory>
#include <iostream>


namespace esicup {

Point2D::Point2D(
    size_t aLengthShift, 
    size_t aWidthShift)
    : lengthShift(aLengthShift) 
    , widthShift(aWidthShift) {
}

size_t Point2D::squaredDistance(const Point2D& other) const {
    size_t lengthDiff = (lengthShift >= other.lengthShift)
        ? lengthShift - other.lengthShift
        : other.lengthShift - lengthShift;

    size_t widthDiff = (widthShift >= other.widthShift)
        ? widthShift - other.widthShift
        : other.widthShift - widthShift;

    return lengthDiff * lengthDiff + widthDiff * widthDiff;
}

void RoundFiller::sortPointVector(std::vector<Point2D>* toSort) const {
    std::sort(std::begin(*toSort), std::end(*toSort), 
        [&](const Point2D& lhs, const Point2D& rhs) -> bool {
            return lhs.squaredDistance(center) < rhs.squaredDistance(center);
    });
}

RoundFiller::RoundFiller(
        const ConstContextPtr& context,
        size_t binType,
        const FillerType& type,
        const LayerStrategyPtr& strategy,
        bool centralize,
        bool leftToRight)
        : IFiller2D(context, binType, type, strategy)
        , centralize(centralize)
        , leftToRight(leftToRight) {
    const auto& binInfo = context->getBinInfo(binType);
    if (centralize) {
        center = Point2D(binInfo.length / 2, binInfo.width / 2);
        points.push_back(center);
    } else {
        center = Point2D(0, 0);
    }
    points.push_back(Point2D(0, 0));
    points.push_back(Point2D(0, binInfo.width));

    points.push_back(Point2D(center.lengthShift, 0));
    points.push_back(Point2D(0, center.widthShift));

    points.emplace_back(binInfo.width, 0);

    points.push_back(Point2D(binInfo.length, binInfo.width));
    points.push_back(Point2D(binInfo.length, 0));
    sortPointVector(&points);
}

void RoundFiller::putStack(const Stack& aStack) {
    Point2D point;
    Stack stack(context);
    assert(findPutPlace(aStack, &point, &stack));
    bin->carry(stack, point.widthShift, point.lengthShift);

    std::vector<Point2D> morePoints;
    morePoints.emplace_back(point.lengthShift, point.widthShift);
    morePoints.emplace_back(point.lengthShift, point.widthShift + stack.getWidth());
    morePoints.emplace_back(point.lengthShift + stack.getLength(), point.widthShift);
    morePoints.emplace_back(
        point.lengthShift + stack.getLength(), point.widthShift + stack.getWidth());
    sortPointVector(&morePoints);

    std::vector<Point2D> finalPoints(points.size() + morePoints.size());;
    std::merge(
        points.begin(), points.end(), morePoints.begin(), morePoints.end(), finalPoints.begin(),
            [&](const Point2D& lhs, const Point2D& rhs) -> bool {
                return lhs.squaredDistance(center) < rhs.squaredDistance(center);
            });
    assert(finalPoints.size() == points.size() + morePoints.size());
    points.swap(finalPoints);
    points.resize(std::unique(std::begin(points), std::end(points)) - std::begin(points));
}

bool RoundFiller::ifCanPutStack(const Stack& stack) const {
    Point2D point;
    Stack stackForm(context);
    return findPutPlace(stack, &point, &stackForm);
}

bool RoundFiller::findPutPlace(
        const Stack& aStack,
        Point2D* coordinate,
        Stack* modifiedStack) const {
    std::vector<Stack> modifiers = Stack::buildSimilar(aStack);
    if (!leftToRight) {
        std::reverse(modifiers.begin(), modifiers.end());
    }
    for (auto& place : points) {
        for (auto& stack : modifiers) {
            *modifiedStack = stack;
            *coordinate = place;
            if (bin->ifCanPut(
                    stack, coordinate->widthShift, coordinate->lengthShift))  {
                return true;
            }
            *coordinate = place;
            if (place.widthShift >= stack.getWidth()
                    && place.lengthShift >= stack.getLength()) {
                *coordinate = Point2D(
                    place.lengthShift - stack.getLength(), place.widthShift - stack.getWidth());

                if (bin->ifCanPut(stack, coordinate->widthShift, coordinate->lengthShift)) {
                    return true;
                }
            }
        }
    }
    return false;
}

IFiller2DPtr RoundFiller::clone() const {
    auto fillerPointer = new RoundFiller(
        context, binType, type, strategy, centralize, leftToRight);
    fillerPointer->points = points;
    fillerPointer->bin = std::make_shared<Bin>(*bin);
    return IFiller2DPtr(fillerPointer);
}

} // namespace esicup
