/// Implements API and structures defined in guilotine.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "guilotine.h"

#include <memory>
#include <cassert>
#include <limits>

namespace esicup {

GuilotineFiller::GuilotineFiller(
        ConstContextPtr context,
        size_t binType,
        const FillerType& type,
        LayerStrategyPtr strategy,
        const GuilotineSelectionRule& selection,
        const GuilotineSplitRule& split)
        : IFiller2D(context, binType, type, strategy),
        selectionRule(selection),
        splitRule(split) {
    RectanglePlacement init;
    init.lengthShift = init.widthShift = 0;
    init.rectangle.length = bin->getLength();
    init.rectangle.width = bin->getWidth();
    freeSpaces.push_back(init);
    shiftIndex = 0;
}

GuilotineFiller::~GuilotineFiller() {}

IFiller2DPtr GuilotineFiller::clone() const {
    auto fillerPointer = new GuilotineFiller(
        context, binType, type, strategy, selectionRule, splitRule);
    fillerPointer->bin = std::shared_ptr<Bin>(new Bin(*bin));
    fillerPointer->freeSpaces = freeSpaces;
    fillerPointer->shiftIndex = shiftIndex;
    return IFiller2DPtr(fillerPointer);
}

bool GuilotineFiller::ifCanPutStack(const Stack& stackCandidate) const {
    auto configuredStacks = Stack::buildSimilar(stackCandidate);
    for (const auto& stack : configuredStacks) {
        if (canPutDefiniteStack(stack)) {
            return true;
        }
    }
    return false;
}

bool GuilotineFiller::canPutDefiniteStack(const Stack& stack) const {
    for (auto space = freeSpaces.begin() + shiftIndex; space != freeSpaces.end(); ++space) {
        if (space->rectangle.length >= stack.getLength()
                && space->rectangle.width >= stack.getWidth()) {
            if (bin->ifCanPut(stack, space->widthShift, space->lengthShift)) {
                return true;
            }
        }
    }
    return false;
}

void GuilotineFiller::putStack(const Stack& stack) {
    auto configuredStacks = Stack::buildSimilar(stack);
    auto selectedStack = configuredStacks.front();
    auto selectedPlace = freeSpaces.end();
    double selectedMetric = std::numeric_limits<double>::max();

    for (const auto& stackOption : configuredStacks) {
        if (canPutDefiniteStack(stackOption)) {
            IndependentRectangle inputRectangle(
                {stackOption.getLength(), stackOption.getWidth()});
            PlacementIterator place = select(inputRectangle);

            double metric = -1.0;
            if (selectionRule == GuilotineSelectionRule::BAF) {
                metric = 1.0 * place->rectangle.length * place->rectangle.width;
            } else if (selectionRule == GuilotineSelectionRule::BSSF) {
                metric = 1.0 * std::min(
                    place->rectangle.length - inputRectangle.length,
                    place->rectangle.width - inputRectangle.width);
            } else if (selectionRule == GuilotineSelectionRule::BLSF) {
                metric = std::max(
                    place->rectangle.length - inputRectangle.length,
                    place->rectangle.width - inputRectangle.width);
            }

            assert(metric > -0.5);
            if (metric < selectedMetric) {
                selectedPlace = place;
                selectedStack = stackOption;
                selectedMetric = metric;
            }
        }
    }

    assert(selectedPlace != freeSpaces.end());
    bin->carry(selectedStack, selectedPlace->widthShift, selectedPlace->lengthShift);
    split(selectedPlace, 
        IndependentRectangle({selectedStack.getLength(), selectedStack.getWidth()}));
}

GuilotineFiller::PlacementIterator GuilotineFiller::select(
        const IndependentRectangle& inputRectangle) {
    assert(freeSpaces.size() > shiftIndex);
    auto selection = freeSpaces.end();
    for (auto iterator = freeSpaces.begin() + shiftIndex; 
            iterator != freeSpaces.end(); ++iterator) {
        if ((iterator->rectangle.length >= inputRectangle.length)
                && (iterator->rectangle.width >= inputRectangle.width)) {
            if (selection == freeSpaces.end() || universalCompare(
                    *iterator, *selection, inputRectangle)) {
                selection = iterator;
            }
        }
    }
    return selection;
}

void GuilotineFiller::split(
        const PlacementIterator& iterator, 
        const IndependentRectangle& inputRectangle) {
    Direction direction(Direction::VERTICAL);
    if (splitRule == GuilotineSplitRule::SAS) {
        direction = splitSAS(*iterator);
    } else if (splitRule == GuilotineSplitRule::SLAS) {
        direction = splitSLAS(*iterator, inputRectangle);
    } else if (splitRule == GuilotineSplitRule::LLAS) {
        direction = splitLLAS(*iterator, inputRectangle);
    } else if (splitRule == GuilotineSplitRule::MINAS) {
        direction = splitMINAS(*iterator, inputRectangle);
    }

    PairRectanglePlacement availableSpaces = makeSplit(
        *iterator, inputRectangle, direction);
    std::swap(*(freeSpaces.begin() + shiftIndex), *iterator);
    ++shiftIndex;

    if (availableSpaces.first.rectangle.length != 0 
            && availableSpaces.first.rectangle.width != 0) {
        rectanglesMerge(availableSpaces.first);
    }
    if (availableSpaces.second.rectangle.length != 0 
            && availableSpaces.second.rectangle.width != 0) {
        rectanglesMerge(availableSpaces.second);
    }
}

bool GuilotineFiller::canMerge(
        const RectanglePlacement& lhs,
        const RectanglePlacement& rhs) const {
    if ((lhs.rectangle.length == rhs.rectangle.length) && (lhs.lengthShift == rhs.lengthShift)) {
        if (lhs.widthShift + lhs.rectangle.width == lhs.widthShift) {
            return true;
        }
    }
    if ((lhs.rectangle.width == rhs.rectangle.width) && (lhs.widthShift == rhs.widthShift)) {
        if (lhs.lengthShift + lhs.rectangle.length == rhs.lengthShift) {
            return true;
        }
    }
    return false;
}

GuilotineFiller::RectanglePlacement GuilotineFiller::merge(
        const RectanglePlacement& lhs, 
        const RectanglePlacement& rhs) const {

    assert(canMerge(lhs, rhs));
    RectanglePlacement result;
    result.rectangle.length = result.rectangle.width = 0;
    result.lengthShift = lhs.lengthShift;
    result.widthShift = lhs.widthShift;

    if (lhs.rectangle.length == rhs.rectangle.length && lhs.lengthShift == rhs.lengthShift) {
        if (lhs.widthShift + lhs.rectangle.width == rhs.widthShift) {
            result.rectangle.length = lhs.rectangle.length;
            result.rectangle.width = lhs.rectangle.width + rhs.rectangle.width;
            return result;
        }
    }
    if (lhs.rectangle.width == rhs.rectangle.width && lhs.widthShift == rhs.widthShift) {
        if (lhs.lengthShift + lhs.rectangle.length == rhs.lengthShift) {
            result.rectangle.width = lhs.rectangle.width;
            result.rectangle.length = lhs.rectangle.length + rhs.rectangle.length;
            return result;
        }
    }

    return result;
}

void GuilotineFiller::rectanglesMerge(
        const RectanglePlacement& placement) {
    RectanglePlacement candidate(placement);
    for (auto iterator = freeSpaces.begin() + shiftIndex;
            iterator != freeSpaces.end(); ++iterator) {
        if (canMerge(candidate, *iterator)) {
            candidate = merge(candidate, *iterator);
            std::swap(*(freeSpaces.begin() + shiftIndex), *iterator);
            ++shiftIndex;
        } else if (canMerge(*iterator, candidate)) {
            candidate = merge(*iterator, candidate);
            std::swap(*iterator, *(freeSpaces.begin() + shiftIndex));
            ++shiftIndex;
        }
    }
    freeSpaces.push_back(candidate);
}

bool GuilotineFiller::universalCompare(
        const RectanglePlacement& lhs, 
        const RectanglePlacement& rhs,
        const IndependentRectangle& inputRectangle) const {

    if (selectionRule == GuilotineSelectionRule::BAF) {
        return compareBAF(lhs, rhs);
    } else if (selectionRule == GuilotineSelectionRule::BSSF) {
        return compareBSSF(lhs, rhs, inputRectangle);
    } else if (selectionRule == GuilotineSelectionRule::BLSF) {
        return compareBLSF(lhs, rhs, inputRectangle);
    }
    throw std::logic_error("Unexpected value.");
}

bool GuilotineFiller::compareBAF(
        const RectanglePlacement& lhs,
        const RectanglePlacement& rhs) const {
    return lhs.rectangle.length * lhs.rectangle.width 
            < rhs.rectangle.length * rhs.rectangle.width;
}

bool GuilotineFiller::compareBSSF(
        const RectanglePlacement& lhs, 
        const RectanglePlacement& rhs,
        const IndependentRectangle& inputRectangle) const {
    return  std::min(lhs.rectangle.length - inputRectangle.length, 
                    lhs.rectangle.width - inputRectangle.width) <
            std::min(rhs.rectangle.length - inputRectangle.length, 
                    rhs.rectangle.width - inputRectangle.width);
}

bool GuilotineFiller::compareBLSF(
        const RectanglePlacement& lhs, 
        const RectanglePlacement& rhs,
        const IndependentRectangle& inputRectangle) const {
    return  std::max(lhs.rectangle.length - inputRectangle.length, 
                    lhs.rectangle.width - inputRectangle.width) <
            std::max(rhs.rectangle.length - inputRectangle.length, 
                    rhs.rectangle.width - inputRectangle.width);
}

GuilotineFiller::PairRectanglePlacement GuilotineFiller::makeSplit(
        const RectanglePlacement& placement,
        const IndependentRectangle& inputRectangle, 
        const Direction direction) {

    RectanglePlacement lhsPlacement;
    RectanglePlacement rhsPlacement;

    lhsPlacement.lengthShift = placement.lengthShift;
    lhsPlacement.widthShift = placement.widthShift + inputRectangle.width;
    lhsPlacement.rectangle.width = placement.rectangle.width - inputRectangle.width;

    rhsPlacement.lengthShift =  placement.lengthShift + inputRectangle.length;
    rhsPlacement.widthShift = placement.widthShift;
    rhsPlacement.rectangle.length = placement.rectangle.length - inputRectangle.length;

    if (direction == Direction::VERTICAL) {
        lhsPlacement.rectangle.length = inputRectangle.length;
        rhsPlacement.rectangle.width = placement.rectangle.width;
    } else {
        lhsPlacement.rectangle.length = placement.rectangle.length;
        rhsPlacement.rectangle.width = inputRectangle.width;
    }

    return std::make_pair(lhsPlacement, rhsPlacement);
}

GuilotineFiller::Direction GuilotineFiller::splitSAS(
        const RectanglePlacement& placement) {
    if (placement.rectangle.length > placement.rectangle.width) {
        return Direction::VERTICAL;
    } else {
        return Direction::HORIZONTAL;
    }
}

GuilotineFiller::Direction GuilotineFiller::splitSLAS(
        const RectanglePlacement& placement,
        const IndependentRectangle& inputRectangle) const {
    if (placement.rectangle.length - inputRectangle.length 
            < placement.rectangle.width - inputRectangle.width) {
        return Direction::VERTICAL;
    } else {
        return Direction::HORIZONTAL;
    }
}

GuilotineFiller::Direction GuilotineFiller::splitLLAS(
        const RectanglePlacement& placement,
        const IndependentRectangle& inputRectangle) const {
    if (placement.rectangle.length - inputRectangle.length 
            >= placement.rectangle.width - inputRectangle.width) {
        return Direction::VERTICAL;
    } else {
        return Direction::HORIZONTAL;
    }
}

GuilotineFiller::Direction GuilotineFiller::splitMINAS(
        const RectanglePlacement& placement,
        const IndependentRectangle& inputRectangle) const {
    if (inputRectangle.length * (placement.rectangle.width - inputRectangle.width) >=
        inputRectangle.width * (placement.rectangle.length - inputRectangle.length)) {
        return Direction::HORIZONTAL;
    } else {
        return Direction::VERTICAL;
    }
}

} // namespace esicup
