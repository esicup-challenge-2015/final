/// Implements API defined in shelf.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "shelf.h"

#include <algorithm>
#include <cassert>
#include <memory>
#include <vector>
#include <exception>


namespace esicup {

ShelfFiller::ShelfFiller(ConstContextPtr context, size_t binType,
        const FillerType& type, LayerStrategyPtr strategy,
        const ShelfSelectionRule& rule, const Position& position)
            : IFiller2D(context, binType, type, strategy),
            shelfPosition(position),
            selectionRule(rule)  {
    shelfs.push_back(ShelfCoordinates({0, 0, 0}));
}

ShelfFiller::~ShelfFiller() {}

IFiller2DPtr ShelfFiller::clone() const {
    auto fillerPointer = new ShelfFiller(context, binType, type,
        strategy, selectionRule, shelfPosition);
    fillerPointer->shelfs = shelfs;
    fillerPointer->bin = std::shared_ptr<Bin>(new Bin(*bin));
    return IFiller2DPtr(fillerPointer);
}

bool ShelfFiller::ifCanPutStack(const Stack& stack) const {
    if (shelfPosition == Position::LENGTHWISE) {
        return ifCanPutStackLengthwise(stack);
    }  else if (shelfPosition == Position::WIDTHWISE) {
        return ifCanPutStackWidthwise(stack);
    } 
    return false;
}

void ShelfFiller::putStack(const Stack& stack) {
    size_t bestShelf = decideShelfMethod(stack);
    assert(bestShelf < shelfs.size());

    if (shelfPosition == Position::LENGTHWISE) {
        bin->carry(stack, shelfs[bestShelf].shift, shelfs[bestShelf].length);
        shelfs[bestShelf].width = std::max(stack.getWidth(), shelfs[bestShelf].width);
        shelfs[bestShelf].length += stack.getLength();
    } else {
        bin->carry(stack, shelfs[bestShelf].width, shelfs[bestShelf].shift);
        shelfs[bestShelf].width += stack.getWidth();
        shelfs[bestShelf].length = std::max(stack.getLength(), shelfs[bestShelf].length);
    }
}

bool ShelfFiller::ifCanPutStackLengthwise(const Stack& stack) const {
    // if can put in closed shelf
    for (const auto& candidate : shelfs) {
        if ((stack.getWidth() <= candidate.width) &&
            (stack.getLength() <= bin->getLength() - candidate.length) &&
            bin->ifCanPut(stack, candidate.shift, candidate.length)) {
                return true;
            }
    }
    // if can put in last shelf
    if ((stack.getWidth() <= bin->getWidth() - shelfs.back().shift) &&
        (stack.getLength() <= bin->getLength() - shelfs.back().length) &&
        bin->ifCanPut(stack, shelfs.back().shift, shelfs.back().length)) {
            return true;
    }
    // if can put in new shelf
    if ((stack.getWidth() <= bin->getWidth() - (shelfs.back().shift + shelfs.back().width)) &&
        (stack.getLength() <= bin->getLength()) &&
        bin->ifCanPut(stack, shelfs.back().shift + shelfs.back().width, 0)) {
            return true;
    }
    return false;
}

bool ShelfFiller::ifCanPutStackWidthwise(const Stack& stack) const {
    // if can put in closed shelf
    for (const auto& candidate : shelfs) {
        if ((stack.getWidth() <= bin->getWidth() - candidate.width) &&
            (stack.getLength() <= candidate.length) &&
            bin->ifCanPut(stack, candidate.width, candidate.shift)) {
                return true;
            }
    }
    // if can put in last shelf
    if ((stack.getWidth() <= bin->getWidth() - shelfs.back().width) &&
        (stack.getLength() <= bin->getLength() - shelfs.back().shift) &&
        bin->ifCanPut(stack, shelfs.back().width, shelfs.back().shift)) {
            return true;
    }
    // if can put in new shelf
    if ((stack.getWidth() <= bin->getWidth()) &&
        (stack.getLength() <= bin->getLength() - (shelfs.back().shift + shelfs.back().length)) &&
        bin->ifCanPut(stack, 0, shelfs.back().shift + shelfs.back().length)) {
            return true;
    }

    return false;
}

bool ShelfFiller::ifCanPutStackIntoShelf(
        const Stack& stack, size_t index, bool isLast) const {
    if (shelfPosition == Position::LENGTHWISE) {
        // Shift is by width.
        if (stack.getWidth() <= shelfs[index].width &&
                stack.getLength() <= bin->getLength() - shelfs[index].length) {
            return true;
        }
        if (isLast) {
            if (stack.getWidth() <= bin->getWidth() - shelfs[index].shift &&
                    stack.getLength() <= bin->getLength() - shelfs[index].length) {
                return true;
            }
        }
    } else {
        // Shift is by length.
        if (stack.getWidth() <= bin->getWidth() - shelfs[index].width &&
                stack.getLength() <= shelfs[index].length) {
            return true;
        }
        if (isLast) {
            if (stack.getWidth() <= bin->getWidth() - shelfs[index].width &&
                    stack.getLength() <= bin->getLength() - shelfs[index].shift) {
                return true;
            }
        }
    }
    return false;
}

void ShelfFiller::addNewShelf() {
    assert(shelfs.size() > 0);
    size_t lastIndex = shelfs.size() - 1;

    if (shelfPosition == Position::LENGTHWISE) {
        shelfs.push_back(
            ShelfCoordinates{0, 0, shelfs[lastIndex].shift + shelfs[lastIndex].width});
    } else {
        shelfs.push_back(
            ShelfCoordinates{0, 0, shelfs[lastIndex].shift + shelfs[lastIndex].length});
    }
}

size_t ShelfFiller::decideShelfMethod(const Stack& stack) {
    if (selectionRule == ShelfSelectionRule::FF) {
        return decideShelfWithFF(stack);
    } else if (selectionRule == ShelfSelectionRule::BWF) {
        return decideShelfWithBWF(stack);
    } else if (selectionRule == ShelfSelectionRule::BHF) {
        return decideShelfWithBHF(stack);
    }
    throw std::logic_error("Unexpected shelf decision mechanism.");
}

// First Fit Shelf
size_t ShelfFiller::decideShelfWithFF(const Stack& stack) {
    for (size_t idx = 0; idx < shelfs.size(); ++idx) {
        if (ifCanPutStackIntoShelf(stack, idx, false)) {
            return idx;
        }
    }
    assert(shelfs.size() > 0);
    size_t lastIdx = shelfs.size() - 1;
    if (ifCanPutStackIntoShelf(stack, lastIdx, true)) {
        return lastIdx;
    }
    addNewShelf();
    return lastIdx + 1;

}

// With minimal length for lengthwise, with minimal width for widthwise.
size_t ShelfFiller::decideShelfWithBWF(const Stack& stack) {
    size_t selectedIdx = shelfs.size();
    for (size_t idx = 0; idx < shelfs.size(); ++idx) {
        if (ifCanPutStackIntoShelf(stack, idx, false)) {
            if (selectedIdx == shelfs.size()) {
                selectedIdx = idx;
            } else if (shelfPosition == Position::LENGTHWISE) {
                if (shelfs[selectedIdx].length < shelfs[idx].length) {
                    selectedIdx = idx;
                } else if (shelfs[selectedIdx].width < shelfs[idx].width) {
                    selectedIdx = idx;
                }
            }
        }
    }

    if (selectedIdx == shelfs.size()) {
        assert(shelfs.size() > 0);
        size_t lastIdx = shelfs.size() - 1;
        if (ifCanPutStackIntoShelf(stack, lastIdx, true)) {
            return lastIdx;
        }

        addNewShelf();
        return lastIdx + 1;
    }
    
    return selectedIdx;
}

// With minimal width for lengthwise, with minimal length for widthwise
size_t ShelfFiller::decideShelfWithBHF(const Stack& stack) {
    size_t selectedIdx = shelfs.size();
    for (size_t idx = 0; idx < shelfs.size(); ++idx) {
        if (ifCanPutStackIntoShelf(stack, idx, false)) {
            if (selectedIdx == shelfs.size()) {
                selectedIdx = idx;
            } else if (shelfPosition == Position::LENGTHWISE) {
                if (shelfs[idx].width < shelfs[selectedIdx].width) {
                    selectedIdx = idx;
                }
            } else if (shelfs[idx].length < shelfs[selectedIdx].length) {
                selectedIdx = idx;
            }
        }
    }

    if (selectedIdx == shelfs.size()) {
        assert(shelfs.size() > 0);
        size_t lastIdx = shelfs.size() - 1;
        if (ifCanPutStackIntoShelf(stack, lastIdx, true)) {
            return lastIdx;
        }
        addNewShelf();
        return lastIdx + 1;
    } 
    return selectedIdx;
}

} // namespace esicup



