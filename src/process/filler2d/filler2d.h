/**
 * @file filler2d.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Represents the IFiller2D interface - entity holding a bin and 
 * packing layers/stacks inside. 
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "options.h"
#include "../../interfaces.h"
#include "../layer_strategy/layer_strategy.h"
#include "../../common.h"

#include <set>
#include <memory>

namespace esicup {

/**
 * @brief Filler 2D is an entity designed to pack a bin (truck, container), 
 * and IFiller 2D is the corresponding interface. Usually filler works on 
 * the stack-level, while there are some layer-level methods that are 
 * delegated to Layer Strategy. Filler2D manages the bin/container that
 * carries stacks with packages.
 */
class IFiller2D {
public:
    /**
     * @brief Interface constructor.
     * 
     * @param context Instance-related input.
     * @param binType The index of the bin type.
     * @param fillerType The type of the filler, helps with self-identification.
     * @param strategy The strategy managing layer-stack relations.
     */
    IFiller2D(ConstContextPtr context,
            size_t binType,
            const FillerType& fillerType,
            LayerStrategyPtr strategy);
    /**
     * @brief Verifies if a particular stack can be added to the managed bin.
     * 
     * @param stack The stack candidate for loading.
     * @return True if there is some place for the stack, and False otherwise.
     */
    virtual bool ifCanPutStack(const Stack& stack) const = 0;
    /**
     * @brief Loads the stack to the container/bin.
     * 
     * @param stack The stack to load inside.
     */
    virtual void putStack(const Stack& stack) = 0;
    /**
     * @brief Clones the Filler entity with inner state and returns a 
     * shared pointer to the newly introduced object.
     * 
     * @return Shared pointer to the cloned object.
     */
    virtual std::shared_ptr<IFiller2D> clone() const = 0;
    /**
     * @brief Verifies if a layer can be added to some stack inside the bin.
     * Delegates the job to the layer strategy.
     * 
     * @param layer The pointer to candidate layer.
     * @param stackToPut The pointer to the index of the candidate destination stack.
     * @param willFinalize Whether that will finalize the stack.
     * @return True if some opportunity is found, and False otherwise.
     */
    bool ifCanPutLayer(Layer* layer, size_t* stackToPut, bool* willFinalize) const;
    /**
     * @brief Loads layer to a certain stack inside the bin.
     * Delegates the job to the layer strategy.
     * 
     * @param layer The layer to load inside the stack.
     * @param stackToPut The index of stack to load the layer inside.
     */
    void putLayer(const Layer& layer, size_t stackToPut);

    /**
     * @brief Retrieves the type of the filler.
     * 
     * @return The type of the filler.
     */
    FillerType getFiller2DType() const;
    /**
     * @brief Retrieves the type of the layer strategy.
     * 
     * @return The type of the applied layer strategy.
     */
    LayerStrategyType getLayerStrategyType() const;

    /**
     * @brief Retrieves the current state of the container/truck/bin.
     * 
     * @return Constant reference to the current container/truck/bin state.
     */
    const Bin& getBin() const;
    /**
     * @brief Virtual destructor to ensure correct polymorphism.
     */
    virtual ~IFiller2D();

protected:
    /// @brief The type of the filler.
    const FillerType type;
    /// @brief The index of the bin/truck/container type.
    const size_t binType;
    /// @brief Layer strategy, responsible for adding layers to stacks.
    const std::shared_ptr<ILayerStrategy> strategy;
    /// @brief Instance-specific input information.
    const ConstContextPtr context;
    /// @brief The bin where the packages/layers/stacks are packed.
    /// Represents the current state of packing.
    std::shared_ptr<Bin> bin;
};

/// @brief Pointer to a filler.
typedef std::shared_ptr<IFiller2D> IFiller2DPtr;


} // namespace esicup
