/**
 * @file round.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines RoundFiller - entity for packing bins in circular fashion.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once
#include "filler2d.h"

namespace esicup {

/**
 * @brief Two-dimensional point with corresponding API.
 */
struct Point2D {
public:
    /**
     * @brief Constructor for two-dimensional point.
     * 
     * @param lengthShift Shift by length from origin.
     * @param widthShift  Shift by width from origin.
     */
    Point2D(size_t lengthShift = 0, size_t widthShift = 0);
    /**
     * @brief The squared euclidean distance from this point to other point.
     * 
     * @param other The other point to compute distance to.
     * 
     * @return The squared euclidean distance. 
     */
    size_t squaredDistance(const Point2D& other) const;
    /**
     * @brief Operator to compare points.
     * 
     * @param other The other point to compare with.
     * 
     * @return True if points are equal, False otherwise.
     */
    bool operator == (const Point2D& other) const {
        return lengthShift == other.lengthShift 
            && widthShift == other.widthShift;
    }

public:
    /// @brief The shift by length.
    size_t lengthShift; 
    /// @brief The shift by width.
    size_t widthShift;
};

/**
 * @brief Filler that loads stacks in a circular
 * fashion from central point.
 */
class RoundFiller final : public IFiller2D {
public:
    /**
     * @brief Constructor for Round Filler. 
     * 
     * @param context     The context with instance-specific input.
     * @param binType     Index of the bin type.
     * @param type        The type of the filler (self-identification).
     * @param strategy    The type of the layer strategy.
     * @param centralize  Whether the circular loading begins from bin center.
     * @param leftToRight Specifies the order to look through the similar stacks.
     */
    RoundFiller(
        const ConstContextPtr& context,
        size_t binType,
        const FillerType& type,
        const LayerStrategyPtr& strategy,
        bool centralize = true,
        bool leftToRight = true);
    /**
     * @brief Verifies if the filler can put stack inside the bin.
     * 
     * @param stack The stack candidate for loading.
     * 
     * @return True if there is a corresponding option, and False otherwise.
     */
    bool ifCanPutStack(const Stack& stack) const override;
    /**
     * @brief Puts stack inside the bin.
     * Assumes that ifCanPutStack method returns True.
     * 
     * @param stack The stack to load to the bin.
     */
    void putStack(const Stack& stack) override;
    /**
     * @brief Clones the filler with inner data structures.
     * 
     * @return Pointer to a cloned filler.
     */
    IFiller2DPtr clone() const override;
    /**
     * @brief Virtual destructor for correct C++ polymorphism.
     */
    virtual ~RoundFiller() override {}

private:
    /**
     * @brief Tries to load the stack inside the bin. If loaded,
     * writes in stackForm the form of the stack, and in coordinate
     * the origin of the location.
     * 
     * @param stack The stack to place inside the bin.
     * @param coordinate Writes in this variable the coordinate of the stack origin.
     * @param stackForm The form of the stack if location 
     *      is found (e.g. obtained by rotating stack). 
     * 
     * @return True if there is a relevant place found, and False otherwise.
     */
    bool findPutPlace(const Stack& stack, Point2D* coordinate, Stack* stackForm) const;
    /**
     * @brief Sorts vector with 2D points according 
     * to the distance from the loading center.
     * 
     * @param toSort The vector with points to sort.
     */
    void sortPointVector(std::vector<Point2D>* toSort) const;

private:
    /// @brief Whether the loading center should be 
    /// in the center of the bin or in the corner.
    bool centralize;
    /// @brief Specifies the order to iterate through similar stacks.
    bool leftToRight;
    /// @brief The candidate points for the origin of the next added stack.
    std::vector<Point2D> points;
    /// @brief The location of the loading center.
    Point2D center;
};

} // namespace esicup