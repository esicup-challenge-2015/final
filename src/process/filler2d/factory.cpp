/// Implements API defined in factory.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "factory.h"
#include "straight.h"
#include "round.h"
#include "shelf.h"
#include "guilotine.h"
#include "../layer_strategy/factory.h"

#include <memory>

namespace esicup {

FillerFactory::FillerFactory(ConstContextPtr instanceContext) : context(instanceContext) {}

IFiller2DPtr FillerFactory::createFiller2D(
        const FillerType& type,
        const LayerStrategyType& strategyType,
        size_t binType) const {
    std::shared_ptr<ILayerStrategy> strategy =
        LayerStrategyFactory::createLayerStrategy(strategyType);
    if (type == FillerType::STRAIGHT_FILLER_NFYX) {
        return IFiller2DPtr(
            new StraightforwardFillerNFYX(context, binType, type, strategy));
    } else if (type == FillerType::STRAIGHT_FILLER_NFXY) {
        return IFiller2DPtr(
            new StraightforwardFillerNFXY(context, binType, type, strategy));
    } else if (type == FillerType::SHELF_FILLER_FF_L) {
        return IFiller2DPtr(new ShelfFiller(context, binType, type, strategy,
            ShelfSelectionRule::FF, Position::LENGTHWISE));
    } else if (type == FillerType::SHELF_FILLER_FF_W) {
        return IFiller2DPtr(new ShelfFiller(context, binType, type, strategy,
            ShelfSelectionRule::FF, Position::WIDTHWISE));
    } else if (type == FillerType::SHELF_FILLER_BWF_L) {
        return IFiller2DPtr(new ShelfFiller(context, binType, type, strategy,
            ShelfSelectionRule::BWF, Position::LENGTHWISE));
    } else if (type == FillerType::SHELF_FILLER_BWF_W) {
        return IFiller2DPtr(new ShelfFiller(context, binType, type, strategy,
            ShelfSelectionRule::BWF, Position::WIDTHWISE));
    } else if (type == FillerType::GUILOTINE_BAF_SAS) {
        return IFiller2DPtr(new GuilotineFiller(context, binType, type, strategy,
            GuilotineSelectionRule::BAF, GuilotineSplitRule::SAS));
    } else if (type == FillerType::GUILOTINE_BSSF_SAS) {
        return IFiller2DPtr(new GuilotineFiller(context, binType, type, strategy,
            GuilotineSelectionRule::BSSF, GuilotineSplitRule::SAS));
    } else if (type == FillerType::GUILOTINE_BLSF_SAS) {
        return IFiller2DPtr(new GuilotineFiller(context, binType, type, strategy,
            GuilotineSelectionRule::BLSF, GuilotineSplitRule::SAS));
    } else if (type == FillerType::GUILOTINE_BAF_SLAS) {
        return IFiller2DPtr(new GuilotineFiller(context, binType, type, strategy,
            GuilotineSelectionRule::BAF, GuilotineSplitRule::SLAS));
    } else if (type == FillerType::GUILOTINE_BSSF_SLAS) {
        return IFiller2DPtr(new GuilotineFiller(context, binType, type, strategy,
            GuilotineSelectionRule::BSSF, GuilotineSplitRule::SLAS));
    } else if (type == FillerType::GUILOTINE_BLSF_SLAS) {
        return IFiller2DPtr(new GuilotineFiller(context, binType, type, strategy,
            GuilotineSelectionRule::BLSF, GuilotineSplitRule::SLAS));
    } else if (type == FillerType::GUILOTINE_BAF_LLAS) {
        return IFiller2DPtr(new GuilotineFiller(context, binType, type, strategy,
            GuilotineSelectionRule::BAF, GuilotineSplitRule::LLAS));
    } else if (type == FillerType::GUILOTINE_BSSF_LLAS) {
        return IFiller2DPtr(new GuilotineFiller(context, binType, type, strategy,
            GuilotineSelectionRule::BSSF, GuilotineSplitRule::LLAS));
    } else if (type == FillerType::GUILOTINE_BLSF_LLAS) {
        return IFiller2DPtr(new GuilotineFiller(context, binType, type, strategy,
            GuilotineSelectionRule::BLSF, GuilotineSplitRule::LLAS));
    } else if (type == FillerType::GUILOTINE_BAF_MINAS) {
        return IFiller2DPtr(new GuilotineFiller(context, binType, type, strategy,
            GuilotineSelectionRule::BAF, GuilotineSplitRule::MINAS));
    } else if (type == FillerType::GUILOTINE_BSSF_MINAS) {
        return IFiller2DPtr(new GuilotineFiller(context, binType, type, strategy,
            GuilotineSelectionRule::BSSF, GuilotineSplitRule::MINAS));
    } else if (type == FillerType::GUILOTINE_BLSF_MINAS) {
        return IFiller2DPtr(new GuilotineFiller(context, binType, type, strategy,
            GuilotineSelectionRule::BLSF, GuilotineSplitRule::MINAS));
    } else if (type == FillerType::ROUND_FILLER_CORNER) {
        return std::make_shared<RoundFiller>(context, binType, type, strategy, false, true);
    } else if (type == FillerType::ROUND_FILLER_CENTER) {
        return std::make_shared<RoundFiller>(context, binType, type, strategy, true, true);
    } else if (type == FillerType::SHELF_FILLER_BHF_L) {
        return IFiller2DPtr(new ShelfFiller(context, binType, type, strategy,
            ShelfSelectionRule::BHF, Position::LENGTHWISE));
    } else if (type == FillerType::SHELF_FILLER_BHF_W) {
        return IFiller2DPtr(new ShelfFiller(context, binType, type, strategy,
            ShelfSelectionRule::BHF, Position::WIDTHWISE));
    } else if (type == FillerType::ROUND_FILLER_CORNER_BKW) {
        return std::make_shared<RoundFiller>(context, binType, type, strategy, false, false);
    } else if (type == FillerType::ROUND_FILLER_CENTER_BKW) {
        return std::make_shared<RoundFiller>(context, binType, type, strategy, true, false);
    }

    return nullptr;
}

} // namespace esicup