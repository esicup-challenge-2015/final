/**
 * @file guilotine.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines guilotine bin packing algorithm, the corresponding entity
 * is called GuilotineFiller.
 *
 * @copyright (c) Belarusian State University, 2015 
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once
#include "filler2d.h"

namespace esicup {

/**
 * @brief Enumerates selection rules for 
 * the guilotine bin packing algorithm.
 */
enum class GuilotineSelectionRule {
    BAF,    ///< BAF selection method
    BSSF,   ///< BSSF selection method
    BLSF,   ///< BLSF selection method
};

/**
 * @brief Enumerates split rules for the 
 * guilotine bin packing algorithm.
 */
enum class GuilotineSplitRule {
    SAS,    ///< SAS split approach
    SLAS,   ///< SLAS split approach
    LLAS,   ///< LLAS split approach
    MINAS,  ///< MINAS split approach
};


/**
 * @brief Implements guilotine bin packing algorithm.
 */
class GuilotineFiller final : public IFiller2D {
public:
    /**
     * @brief Guilotine filler constructor.
     * 
     * @param context   The instance-specific input summary.
     * @param binType   The index of the bin to pack inside.
     * @param type      The type of the filler for self-identification.
     * @param strategy  Strategy to account for layer-stack interaction.
     * @param selection Selection method.
     * @param split     Split method.
     */
    GuilotineFiller(
        ConstContextPtr context,
        size_t binType,
        const FillerType& type,
        LayerStrategyPtr strategy,
        const GuilotineSelectionRule& selection,
        const GuilotineSplitRule& split);

    /**
     * @brief Verifies if the algorithm has an option 
     * to put new stack to the truck/bin.
     * 
     * @param stack The stack to try loading inside the bin.
     * @return True if there is a corresponding option, and False otherwise.
     */
    bool ifCanPutStack(const Stack& stack) const override;
    /**
     * @brief Loads stack inside the truck/bin.
     * 
     * @param stack The stack to load inside.
     */
    void putStack(const Stack& stack) override;
    /**
     * @brief Clones the filler with inner state.
     * 
     * @return Smart pointer to the created object.
     */
    IFiller2DPtr clone() const override;
    /**
     * @brief Virtual destructor for correct C++ polymorphism.
     */
    virtual ~GuilotineFiller() override;

private:
    /**
     * @brief Structure to represent rectangle as 
     * something having length and width.
     */
    struct IndependentRectangle {
    public:
        /// @brief Rectangle length.
        size_t length;
        /// @brief Rectangle width.
        size_t width;
    };
    /**
     * @brief Structure to represent rectangle together
     * with placement coordinates.
     */
    struct RectanglePlacement {
    public:
        /// @brief Shift from origin along length axis.
        size_t lengthShift;
        /// @brief Shift from origin along width axis.
        size_t widthShift;
        /// @brief The placed rectangle.
        IndependentRectangle rectangle;
    };
    /**
     * @brief Enumerates split directions.
     */
    enum class Direction {
        HORIZONTAL, ///< Horizontal split direction.
        VERTICAL    ///< Vertical split direction.
    };

private:
    /// @brief Iterator over placed rectangles.
    typedef std::vector<RectanglePlacement>::iterator PlacementIterator;
    /// @brief Pair of placed rectangles.
    typedef std::pair<RectanglePlacement, RectanglePlacement> PairRectanglePlacement;

private:
    /**
     * @brief Selects the place for the rectangle.
     * 
     * @param inputRectangle Rectangle looking for a place.
     * 
     * @return Iterator to the location of the place (inside free spaces).
     */
    PlacementIterator select(const IndependentRectangle& inputRectangle);
    /**
     * @brief Splits placement to account for input rectangle.
     * 
     * @param iterator The iterator to the placement.
     * @param inputRectangle The rectangle to account for.
     */
    void split(
        const PlacementIterator& iterator, 
        const IndependentRectangle& inputRectangle);
    /**
     * @brief Verifies if the algorithm can find an option to put stack inside
     * in the form provided, i.e. without similarities and modifications.
     * 
     * @param stack The stack to try loading inside the bin.
     * 
     * @return True if there the option is found, False otherwise.
     */
    bool canPutDefiniteStack(const Stack& stack) const;
    /**
     * @brief Tries to merge the rectangle placement 
     * with some of the available spaces.
     * 
     * @param placement The rectangle placement for merging.
     */
    void rectanglesMerge(const RectanglePlacement& placement);

    /**
     * @brief Verifies if thwo rectangle placements can be merged.
     * 
     * @param lhs The first rectangle placement for merging.
     * @param rhs The second rectangle placement for merging.
     * 
     * @return True if the rectangle placements can get merged, and False otherwise.
     */
    bool canMerge(
        const RectanglePlacement& lhs,
        const RectanglePlacement& rhs) const;
    /**
     * @brief Merges two rectangle placements into one rectangle 
     * placement, when such merge is available.
     * 
     * @param lhs The first rectangle placement for merging.
     * @param rhs The second rectangle placement for merging.
     * 
     * @return The merged rectangle placement.
     */
    RectanglePlacement merge(
        const RectanglePlacement& lhs,
        const RectanglePlacement& rhs) const;
    /**
     * @brief Compares two rectangles according 
     * to specified selection rule. 
     * 
     * @param lhs The first rectangle to compare. 
     * @param rhs The second rectangle to compare.
     * @param inputRectangle The reference rectangle 
     *          (for the cases when relevant).
     * 
     * @return True if lhs < rhs, False otherwise.
     */
    bool universalCompare(
        const RectanglePlacement& lhs,
        const RectanglePlacement& rhs,
        const IndependentRectangle& inputRectangle) const;

    /**
     * @brief Compares two placed rectangles according to BAF selection rule.
     * 
     * @param lhs The first rectangle to compare.
     * @param rhs The second rectangle to compare.
     * 
     * @return True if lhs < rhs, and False otherwise.
     */
    bool compareBAF(const RectanglePlacement& lhs,
        const RectanglePlacement& rhs) const;
    /**
     * @brief Compares two placed rectangles according to BSSF selection rule.
     * 
     * @param lhs The first rectangle to compare.
     * @param rhs The second rectangle to compare.
     * @param inputRectangle The reference rectangle.
     * 
     * @return True if lhs < rhs, and False otherwise.
     */
    bool compareBSSF(const RectanglePlacement& lhs,
        const RectanglePlacement& rhs,
        const IndependentRectangle& inputRectangle) const;
    /**
     * @brief Compares two placed rectangles according to BLSF selection rule.
     * 
     * @param lhs The first rectangle to compare.
     * @param rhs The second rectangle to compare.
     * @param inputRectangle The reference rectangle.
     * 
     * @return True if lhs < rhs, and False otherwise. 
     */
    bool compareBLSF(const RectanglePlacement& lhs,
        const RectanglePlacement& rhs,
        const IndependentRectangle& inputRectangle) const;
    
    /**
     * @brief Performs split of a placed rectangle in specified 
     * direction to allocate space for the input rectangle.
     * 
     * @param placement The placement for splitting.
     * @param inputRectangle The rectangle for allocating 
     *          space (corresponds to some stack loaded).
     * @param direction The split direction.
     * 
     * @return Pair with two rectangle placements, representing the free spaces
     *      after subspace has been allocated for inputRectangle.
     */
    PairRectanglePlacement makeSplit(
        const RectanglePlacement& placement,
        const IndependentRectangle& inputRectangle,
        const Direction direction);
    /**
     * @brief Decides on spit direction according to SAS split rule.
     * 
     * @param placement The rectangle placement to split.
     * 
     * @return The direction of the decided split.
     */
    Direction splitSAS(
        const RectanglePlacement& placement);
    /**
     * @brief Decides on split direction according to SLAS split rule.
     * 
     * @param placement The rectangle placement to split.
     * @param inputRectangle The reference rectangle.
     * 
     * @return The direction of the decided split.
     */
    Direction splitSLAS(
        const RectanglePlacement& placement,
        const IndependentRectangle& inputRectangle) const;
    /**
     * @brief Decides on split direction according to LLAS split rule.
     * 
     * @param placement The rectangle placement to split.
     * @param inputRectangle The reference rectangle.
     * 
     * @return The direction of the decided split.
     */
    Direction splitLLAS(
        const RectanglePlacement& placement,
        const IndependentRectangle& inputRectangle) const;
    /**
     * @brief Decides on split direction according to MINAS split rule.
     * 
     * @param placement The rectangle placement to split.
     * @param inputRectangle The reference rectangle.
     * 
     * @return The direction of the decided split.
     */
    Direction splitMINAS(
        const RectanglePlacement& placement,
        const IndependentRectangle& inputRectangle) const;
    /**
     * @brief Decides on split direction according to LAS split rule.
     * 
     * @param placement The rectangle placement to split.
     * @param inputRectangle The reference rectangle.
     * 
     * @return The direction of the decided split.
     */
    Direction splitLAS(
        const RectanglePlacement& placement,
        const IndependentRectangle& inputRectangle);

private:
    /// @brief The vector with spaces available.
    std::vector<RectanglePlacement> freeSpaces;
    /// @brief The index of shift for proper space management.
    size_t shiftIndex;
    /// @brief The rule to compare placements.
    const GuilotineSelectionRule selectionRule;
    /// @brief The rule to select split direction.
    const GuilotineSplitRule splitRule;
};

} // namespace esicup
