/**
 * @file factory.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines filler factory - abstract factory for filler creation.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once
#include "filler2d.h"
#include "options.h"


namespace esicup {

/**
 * @brief Factory for fillers. Simplifies filler creation,
 * and also provides uniform interface.
 */
class FillerFactory final {
public:
    /**
     * @brief Factory constructor.
     * 
     * @param context Context with instance-specific input information.
     */
    FillerFactory(ConstContextPtr context);
    /**
     * @brief Creates Filler2D according to the requested parameters.
     * 
     * @param type The type of the filler.
     * @param strategy Type The type of the layer strategy.
     * @param binType  The index of the bin type.
     * 
     * @return Smart pointer to the newly created filler.
     */
    IFiller2DPtr createFiller2D(
            const FillerType& type,
            const LayerStrategyType& strategyType,
            size_t binType) const;
private:
    /// @brief Context with instance-specific input information.
    ConstContextPtr context;
};

} // namespace esicup