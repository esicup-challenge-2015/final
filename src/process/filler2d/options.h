/**
 * @file options.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines filler type enumeration.
 * 
 * @copyright (c) Belarusian State Univesrity, 2015
 * @copyright (c) Smith School of Business, 2024
 */

#pragma once

namespace esicup {

/**
 * @brief Enumerates filler types.
 */
enum class FillerType {
    STRAIGHT_FILLER_NFXY,       ///< Straight filler, along x axis then along y axis.
    STRAIGHT_FILLER_NFYX,       ///< Straight filler, along y axis then along x axis.
    SHELF_FILLER_FF_L,          ///< Shelf filler, with FF policy, lengthwise.
    SHELF_FILLER_FF_W,          ///< Shelf filler, with FF policy, widthwise.
    SHELF_FILLER_BWF_L,         ///< Shelf filler, with BWF policy, lengthwise.
    SHELF_FILLER_BWF_W,         ///< Shelf filler, with BWF policy, widthwise.
    SHELF_FILLER_BHF_L,         ///< Shelf filler, with BHF policy, lengthwise.
    SHELF_FILLER_BHF_W,         ///< Shelf filler, with BHF policy, widthwise.

    GUILOTINE_BAF_SAS,          ///< Guilotine filler, with BAF selection and SAS split.
    GUILOTINE_BSSF_SAS,         ///< Guilotine filler, with BSSF selection and SAS split.
    GUILOTINE_BLSF_SAS,         ///< Guilotine filler, with BLSF selection and SAS split.
    GUILOTINE_BAF_SLAS,         ///< Guilotine filler, with BAF selection and SLAS split.
    GUILOTINE_BSSF_SLAS,        ///< Guilotine filler, with BSSF selection and SLAS split.
    GUILOTINE_BLSF_SLAS,        ///< Guilotine filler, with BLSF selection and SLAS split.
    GUILOTINE_BAF_LLAS,         ///< Guilotine filler, with BAF selection and LLAS split.
    GUILOTINE_BSSF_LLAS,        ///< Guilotine filler, with BSSF selection and LLAS split.
    GUILOTINE_BLSF_LLAS,        ///< Guilotine filler, with BLSF selection and LLAS split.
    GUILOTINE_BAF_MINAS,        ///< Guilotine filler, with BAF selection and MINAS split.
    GUILOTINE_BSSF_MINAS,       ///< Guilotine filler, with BSSF selection and MINAS split.
    GUILOTINE_BLSF_MINAS,       ///< Guilotine filler, with BLSF selection and MINAS split.

    ROUND_FILLER_CORNER,        ///< Round filler, starting from corner.
    ROUND_FILLER_CENTER,        ///< Round filler, starting from center.
    ROUND_FILLER_CORNER_BKW,    ///< Round filler, starting from corner, backwards.
    ROUND_FILLER_CENTER_BKW,    ///< Round filler, starting from center, backwards.
};

} // namespace esicup
