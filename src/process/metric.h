/**
 * @file metric.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Implements various metrics and factory for creating them.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "data.h"

#include <memory>

namespace esicup {

/**
 * @brief IMetric is an interface for calculating metrics - some
 * proxies about the solution quality or features.
 */
class IMetric {
public:
    /**
     * @brief Metric constructor.
     * 
     * @param context The instance input information.
     */
    IMetric(const ConstContextPtr& context);
    /**
     * @brief Calculates the metric.
     * 
     * @param solution Partial solution to calculate the metric from.
     * 
     * @return The metric value.
     */
    virtual long double calculate(const PartialData& solution) const = 0;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~IMetric() {}

protected:
    /// @brief The instance input information.
    ConstContextPtr context;
};

/// @brief Smart pointer to the metric.
typedef std::shared_ptr<IMetric> IMetricPtr;

/**
 * @brief Enumerates metric types.
 */
enum class MetricType {
    USED_BIN_VOLUME,                ///< The volume used by bins.

    DENSITY_VOLUME_LAST,            ///< The volume density in the last bin.
    DENSITY_WEIGHT_LAST,            ///< The weight density in the last bin.
    DENSITY_AREA_LAST,              ///< The area density in the last bin.

    DENSITY_VOLUME,                 ///< The volume density across solution. 
    DENSITY_WEIGHT,                 ///< The weight density across solution.
    DENSITY_AREA,                   ///< The area density across solution.

    USED_BIN_VOLUME_BOUND,          ///< The used bin volume with bound for unpacked items.
    DENSITY_VOLUME_BOUND,           ///< The volume density with bound for unpacked items.
    DENSITY_WEIGHT_BOUND,           ///< The weight density with bound for unpacked items.

    AVERAGE_DENSITY_VOLUME,         ///< The average volume density across all bins in solution.
    AVERAGE_DENSITY_WEIGHT,         ///< The average weight density across all bins in solution.
    AVERAGE_DENSITY_AREA,           ///< The average area density across all bins in solution.

    AVERAGE_DENSITY_WEIGHT_BOUND,   ///< The average weight density with bound for unpacked items.
    AVERAGE_DENSITY_VOLUME_BOUND,   ///< The average volume density with bound for unpacked items.
    AVERAGE_DENSITY_AREA_BOUND,     ///< The average area density with bound for unpacked items.
    NO_METRIC,                      ///< Value specifying no metric.

    FIRST_ACCELERATED = DENSITY_VOLUME_LAST,     ///< The first metric applied in accelerated mode.
    FINAL_ACCELERATED =  DENSITY_VOLUME,          ///< The last metric applied in accelerated mode.

    FIRST_REGULAR = USED_BIN_VOLUME,             ///< The first metric applied in regular mode.
    FINAL_REGULAR =  AVERAGE_DENSITY_AREA_BOUND   ///< The last metric applied in regular mode.
};


/**
 * @brief Sum of bin volumes in partial solution. 
 * Lower Bound: apply estimation on the rest of the items.
 */
class UsedBinVolumeMetric final : public IMetric {
public:
    /**
     * @brief Constructor for UsedBinVolume metric.
     * 
     * @param context The context with instance-specific input information.
     * @param useLowerBound Whether to use lower bound estimation on unpacked items.
     */
    UsedBinVolumeMetric(
            const ConstContextPtr& context,
            bool useLowerBound);
    /**
     * @brief Calculates the metric, i.e. sum of bin volumes in partial solution.
     * If lower bound is applied, applies estimation on the rest of the items.
     * 
     * @param solution Partial solution to calculate the metric from.
     * 
     * @return The metric value.
     */
    virtual long double calculate(const PartialData& solution) const override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~UsedBinVolumeMetric() override {}

private:
    /**
     * @brief Estimates the lower bound for the solution.
     * 
     * @param stats The statistics computed on partial solution.
     * @return The value of the lower bound.
     */
    long double lowerBoundEstimation(const SolutionStats& stats) const;

private:
    /// @brief Whether to use lower bound estimation on unpacked items.
    bool useLowerBound;
};

/**
 * @brief Density of used volume in partial solution if useAllBins 
 * is true (else density in the last bin). If useLowerBound is true, 
 * then estimates the unpacked density as 1.
 */
class DensityVolumeMetric final : public IMetric {
public:
    /**
     * @brief Constructor for Density Volume Metric.
     * 
     * @param context The context with instance-specific input information.
     * @param useLowerBound Whether to estimate a bound on the non-packed items.
     * @param useAllBins Whether to use all bins (or only last bin).
     */
    DensityVolumeMetric(
            const ConstContextPtr& context,
            bool useLowerBound,
            bool useAllBins);
    /**
     * @brief Calculates the metric. Density of used volume in partial solution if useAllBins 
     * is true (else density in the last bin). If useLowerBound is true, then estimates the 
     * unpacked density as 1.
     * 
     * @param solution Partial solution to calculate the metric from.
     * 
     * @return The metric value.
     */
    virtual long double calculate(const PartialData& solution) const override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~DensityVolumeMetric() override {}

private:
    /// @brief Whether to use lower bound for non-packed items.
    bool useLowerBound;
    /// @brief Whether to use all bins (only last bin alternatively).
    bool useAllBins;
};

/**
 * @brief Density of used weight in whole solution 
 * if useAllBins is true (else density in the last bin).
 * If useLowerBound is true, then get estimation of 
 * rest density as 1.
 */
class DensityWeightMetric final : public IMetric {
public:
    /**
     * @brief Constructor for Density Weight Metric.
     * 
     * @param context The context with instance-specific input information.
     * @param useLowerBound Whether to use lower bound for unpacked items.
     * @param useAllBins Whether to use all bins (else only last bin).
     */
    DensityWeightMetric(
            const ConstContextPtr& context,
            bool useLowerBound,
            bool useAllBins);
    /**
     * @brief Calculates the metric, i.e. the density of used weight in whole solution 
     * if useAllBins is true (else density in last bin). If useLowerBound is true, then 
     * get estimation of the rest density as 1.
     * 
     * @param solution Partial solution to calculate the metric from.
     * 
     * @return The metric value.
     */
    virtual long double calculate(const PartialData& solution) const override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    ~DensityWeightMetric() override {}

private:
    /// @brief Whether to use lower bound for non-packed items.
    bool useLowerBound;
    /// @brief Whether to use all bins (only last bin alternatively).
    bool useAllBins;
};
 
/**
 * @brief Density of used area in partial solution if
 * useAllBins is true (else area density in the last bin only).
 */
class DensityAreaMetric final : public IMetric {
public:
    /**
     * @brief Constructor for Density Area Metric.
     * 
     * @param context The context with instance-specific input information.
     * @param useAllBins Whether to use all bins (else the last bin only).
     */
    DensityAreaMetric(
            const ConstContextPtr& context,
            bool useAllBins);
    /**
     * @brief Calculates the metric. I.e. the density of used area in 
     * partial solution if useAllBins is true (else area density in 
     * the last bin only).
     * 
     * @param solution Partial solution to calculate the metric from.
     * 
     * @return The metric value.
     */
    virtual long double calculate(const PartialData& solution) const override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~DensityAreaMetric() override {}

private:
    /// @brief Whether to use all bins (only last bin alternatively).
    bool useAllBins;
};

/**
 * @brief Sum of volume densities in all bins, divided by the 
 * number of bins. If useLowerBound is enabled, then the formula is
 * (density_sum + groups.size) / (bins.size + groups.size).
 */
class AverageDensityVolumeMetric final : public IMetric {
public:
    /**
     * @brief Constructor for Average Density Volume metric.
     * 
     * @param context The context with instance-specific input information.
     * @param useLowerBound Whether to use lower bound for unpacked items.
     */
    AverageDensityVolumeMetric(const ConstContextPtr& context, bool useLowerBound);
    /**
     * @brief Calculates the metric. I.e. the sum of volume densities 
     * in all bins, divided by the number of bins. If useLowerBound is 
     * enabled, then the following formula is applied: 
     * (density_sum + groups.size) / (bins.size + groups.size).
     * 
     * @param solution Partial solution to calculate the metric from.
     * 
     * @return The metric value.
     */
    virtual long double calculate(const PartialData& solution) const override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~AverageDensityVolumeMetric() override {}

private:
    /// @brief Whether to use lower bound for unpacked items.
    bool useLowerBound;
};

/**
 * @brief Sum of weight densities in all bins, 
 * divided by the number of bins.
 * If useLowerBound is enabled, then the formula 
 * is (density_sum + groups.size) / (bins.size + groups.size).
 */
class AverageDensityWeightMetric final : public IMetric {
public:
    /**
     * @brief Constructor for Average Density Weight metric.
     * 
     * @param context The context with instance-specific input information.
     * @param useLowerBound Whether to use lower bound for unpacked items.
     */
    AverageDensityWeightMetric(
            const ConstContextPtr& context,
            bool useLowerBound);
    /**
     * @brief Calculates the metric. I.e. the sum of weight densities
     * in all bins, divided by the number of bins. If useLowerBound
     * is enabled, then the formula is 
     * (density_sum + groups.size) / (bins.size + groups.size).
     * 
     * @param solution Partial solution to calculate the metric from.
     * 
     * @return The metric value.
     */
    virtual long double calculate(const PartialData& solution) const override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~AverageDensityWeightMetric() override {}

private:
    /// @brief Whether to use lower bound for non-packed items.
    bool useLowerBound;
};

/**
 * @brief Sum of area densities in all bins, divided by the number of bins.
 * If useLowerBound is enabled, then the formula is
 * (density_sum + groups.size) / (bins.size + groups.size).
 */
class AverageDensityAreaMetric final : public IMetric {
public:
    /**
     * @brief Constructor for Average Density Area Metric.
     * 
     * @param context The context with instance-specific input information.
     * @param useLowerBound Whether to use lower bound for non-packed items.
     */
    AverageDensityAreaMetric(
            const ConstContextPtr& context,
            bool useLowerBound = false);
    /**
     * @brief Calculates the metric. I.e. the sum of area densities in all bins, 
     * divided by the number of bins. If useLowerBound is enabled, then 
     * the formula is (density_sum + groups.size) / (bins.size + groups.size).
     * 
     * @param solution Partial solution to calculate the metric from.
     * 
     * @return The metric value.
     */
    virtual long double calculate(const PartialData& solution) const override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~AverageDensityAreaMetric() override {}

private:
    /// @brief Whether to use lower bound for non-packed items.
    bool useLowerBound;
};

/**
 * @brief Abstract factory for metrics. 
 * Allows to create metrics in uniform fashion.
 */
class MetricFactory final {
public:
    /**
     * @brief Constructor for metric factory.
     * 
     * @param context Context with instance-specific input information.
     */
    MetricFactory(const ConstContextPtr& context);
    /**
     * @brief Factory method to create metrics.
     * 
     * @param metricType The type of the metric to create.
     * 
     * @return Smart pointer to the created metric.
     */
    IMetricPtr createMetric(const MetricType& metricType) const;

private:
    /// @brief The instance input information.
    ConstContextPtr context;
};

/**
 * @brief A common method to create average metrics
 * with densities involved.
 * 
 * @param solution The partial solution to compute metric on.
 * @param useLowerBound Whether to use density 
 *      lower bound for the rest of the items.
 * @param accumulatedSum The accumulated metric sum.
 * @param context The instance-specific input information.
 * 
 * @return The metric value.
 */
long double getAverageResult(
        const PartialData& solution,
        bool useLowerBound,
        long double accumulatedSum,
        ConstContextPtr context);

} // namespace esicup
