/// Implements structures and API defined in data.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "data.h"

namespace esicup {

SolutionStats getStatistics(
        const PartialData& solution,
        const ConstContextPtr& context) {
    SolutionStats stats = SolutionStats(
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0});
    for (const auto& filler : solution.partialSolution) {
        const auto& bin = filler->getBin();
        stats.usedVolume += bin.getUsedVolume();
        stats.usedWeight += bin.getWeight();
        stats.usedArea += bin.getUsedArea();
        stats.maxPossibleVolume += bin.getVolume();
        stats.maxPossibleWeight += bin.getMaximalAllowedWeight();
        stats.maxPossibleArea += bin.getBoxArea();
    }
    for (const auto& group : solution.currentGroups.groups) {
        const auto& itemInfo = context->getItemInfo(group.itemType);
        stats.residualVolume += itemInfo.getVolume() * group.count;
        stats.residualWeight += itemInfo.weight * group.count;
    }
    if (solution.partialSolution.size()) {
        const auto& bin = solution.partialSolution.back()->getBin();
        stats.lastBinIsEmpty = !(bin.getStackCount());

        stats.lastBinUsedVolume = bin.getUsedVolume();
        stats.lastBinUsedArea = bin.getUsedArea();
        stats.lastBinUsedWeight = bin.getWeight();

        stats.lastBinMaxVolume = bin.getVolume();
        stats.lastBinMaxArea = bin.getBoxArea();
        stats.lastBinMaxWeight = bin.getMaximalAllowedWeight();
    } else {
        stats.lastBinIsEmpty = true;
    }
    return stats;
}

PartialData PartialData::clone() const {
    PartialData cloneData;
    cloneData.currentGroups = currentGroups;
    for (const auto& fillerPtr : partialSolution) {
        cloneData.partialSolution.push_back(fillerPtr->clone());
    }
    return cloneData;
}

std::vector<BinWithHistory> PartialData::getBins() const {
    std::vector<BinWithHistory> answer;
    for (const auto& filler : partialSolution) {
        answer.push_back({filler->getBin(), 
            {filler->getFiller2DType(), filler->getLayerStrategyType()}});
    }
    return answer;
}

} // namespace esicup
