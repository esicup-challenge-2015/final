/**
 * @file process.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines and implements Processors - entities to get preprocessed
 * information at input and provide solutions with multiple packings, that
 * should be post-processed afterwards.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "../interfaces.h"
#include "iteration_strategy.h"
#include "metric.h"
#include "data.h"

#include <queue>

namespace esicup {

/// @brief Vector with smart pointers to metrics.
typedef std::vector<std::shared_ptr<IMetric>> VectorMetric;

/**
 * @brief Creates metric threshold for selecting specific number of solutions.
 * 
 * @param metrics The metrics to evaluate.
 * @param solutions Currently available population for selection.
 * @param quorumCount Amount of entries to select according to each metric.
 * 
 * @return The vector with selection thresholds according to each metric.
 */
std::vector<long double> makeQuorumThresholds(
    const VectorMetric& metrics, const GeneticData& solutions, size_t quorumCount);
    
/**
 * @brief Interface to Processor entity - entity 
 * creating packings based on the input data.
 */
class IProcessor {
public:
    /**
     * @brief Creates packing from input data.
     * 
     * @param data Input data for packing.
    */
    virtual SolutionOutputData solve(const SolutionInputData& data) = 0;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
    */
    virtual ~IProcessor() {};
};

/**
 * @brief Implements genetic search, that at each iteration
 * expands population and then selects the most effective options.
 */
class GeneticSearch final: public IProcessor {
public:
    /**
     * @brief Constructor for Genetic Search.
     * 
     * @param strategy Iteration strategy to process evolution.
     * @param metrics  Metrics for selection.
     * @param selectCount Amount of trajectories to select at each selection.
     */
    GeneticSearch(
            std::shared_ptr<IIterationStrategy> strategy,
            std::vector<std::shared_ptr<IMetric>> metrics,
            size_t selectCount);
    /**
     * @brief Creates packing from input data.
     * 
     * @param data Input data for packing.
    */
    SolutionOutputData solve(const SolutionInputData& data) override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~GeneticSearch() {};

private:
    /**
     * @brief Genetic selection. Selects the most effective options.
     */
    void select();
    /**
     * @brief Genetic evolution. Generates new partial packings. 
     * Delegates to iteration strategy.
     */
    void iterate();
    /**
     * @brief Converts input format to genetic iteration format.
     * 
     * @param data The data in the input format for processing stage.
     */
    void makeGeneticData(const SolutionInputData& data);
    /**
     * @brief Checks for terminating condition.
     * 
     * @return True if termination condition has appeared, and False otherwise.
     */
    bool finish();
    /**
     * @brief Creates output data from internal genetic form.
     * 
     * @return Solution format ready for post-processing.
     */
    SolutionOutputData makeOutput() const;

private:
    /// @brief Amount of trajectories for selection after evolution per metric.
    size_t selectCount;

    /// @brief Evolution algorithm.
    std::shared_ptr<IIterationStrategy> strategy;
    /// @brief Vector with selection metrics.
    std::vector<std::shared_ptr<IMetric>> metrics;

    /// @brief Current partial solutions.
    GeneticData solutions;
    /// @brief Finalized packing trajectories.
    GeneticData finalizedSolutions;

    /// @brief Instance-specific input information.
    ConstContextPtr context;
};


/**
 * @brief Performs multiple genetic search launches 
 * and aggregates the obtained results.
 */
class CompositeProcessor final: public IProcessor {
public:
    /**
     * @brief Constructor for Composite Processor. Initializes
     * multiple processors for solving the instance.
     * 
     * @param context Context with instance-specific input information.
     * @param reduce Whether to apply composite postprocessor or 
     * swap stack postprocessor at selection stage.
     */
    CompositeProcessor(ConstContextPtr context, bool reduce = true);
    /**
     * @brief Solves the instance and creates solution output data
     * with packings.To get post-processed.
     * 
     * @param data The input data to start from.
     * 
     * @return Solution output data with multiple packings.
     */
    SolutionOutputData solve(const SolutionInputData& data) override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~CompositeProcessor() {};

private:
    /// @brief List of algorithms to try.
    std::vector<std::unique_ptr<IProcessor>> processors;
    /// @brief If true, composite postprocessor is applied. 
    /// Otherwise, Swap stack postprocessor is applied.
    bool reduce;
};

} // namespace esicup

