/**
 * @file data.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines data structures for the processing module.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "filler2d/filler2d.h"
#include "../interfaces.h"

#include <vector>
#include <memory>
#include <string>

namespace esicup {

/**
 * @brief Represents solution prefix - sequence 
 * with bins that contain a part of the stacks
 * that need packing.
 */
struct PartialData {
public:
    /**
     * @brief Clones partial data with inner data structures.
     * 
     * @return The cloned partial data.
     */
    PartialData clone() const;
    /**
     * @brief Retrieves the packed bin sequence.
     * 
     * @return Vector with packed bins.
     */
    std::vector<BinWithHistory> getBins() const;

public:
    /// @brief Sequence of fillers representing the partial packing.
    std::vector<IFiller2DPtr> partialSolution;
    /// @brief The currently unpacked groups.
    GroupOrder currentGroups;
};

/**
 * @brief Represents statistics corresponding to the solution.
 */
struct SolutionStats {
public:
    /// @brief Non-covered volume of bins.
    long double residualVolume;
    /// @brief Available weight inside bins.
    long double residualWeight;

    /// @brief Effectively used volume.
    long double usedVolume;
    /// @brief Effectively used area.
    long double usedArea;
    /// @brief Effectively used weight.
    long double usedWeight;

    /// @brief The sum of allowed bin weights in the solution.
    long double maxPossibleWeight;
    /// @brief The sum of bin volumes in the solution.
    long double maxPossibleVolume;
    /// @brief The sum of bin areas in the solution.
    long double maxPossibleArea;

    /// @brief The effectively used volume inside the last bin.
    long double lastBinUsedVolume;
    /// @brief The effectively used area inside the last bin.
    long double lastBinUsedArea;
    /// @brief The weight of all items inside the last bin.
    long double lastBinUsedWeight;

    /// @brief Maximal item volume inside the last bin.
    long double lastBinMaxVolume;
    /// @brief Maximal item weight inside the last bin.
    long double lastBinMaxWeight;
    /// @brief Maximal item area inside the last bin.
    long double lastBinMaxArea;

    /// @brief Whether the last bin is empty.
    bool lastBinIsEmpty;
};

/**
 * @brief Computes statistics from partial solution.
 * 
 * @param solution The partial solution to compute statistics from.
 * @param context The information about instance-specific input.
 * 
 * @return The statistics computed on the partial solution. 
 */
SolutionStats getStatistics(
        const PartialData& solution,
        const ConstContextPtr& context);

/**
 * @brief Defines genetic data - a set of partial solutions
 * to start from and improve.
 */
struct GeneticData {
public:
    /// @brief The vector with partial solutions to proceed in genetic fashion.
    std::vector<PartialData> currentSolutions;
};

} // namespace esicup
