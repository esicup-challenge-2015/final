/// Implements structures and API defined in iteration_straegy.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "iteration_strategy.h"

#include <iostream>
#include <fstream>

namespace esicup {

LastBinClosingStrategy::LastBinClosingStrategy(
    const std::vector<size_t>& binTypes,
    const std::vector<std::pair<FillerType, LayerStrategyType>>& filler2DNames,
    const FillerFactory& fillerFactory,
    const GroupManager &aGroupManager,
    const ConstContextPtr& instanceContext)
    : allowedBinTypes(binTypes)
    , allowedFiller2DNames(filler2DNames)
    , groupManager(aGroupManager)
    , factory(fillerFactory)
    , context(instanceContext) {
}

GeneticData LastBinClosingStrategy::iterate(
        const GeneticData& previousData) const {
    GeneticData result;
    for (const auto& parent : previousData.currentSolutions) {
        for (auto binType : allowedBinTypes) {
            for (const auto& filler2DInit : allowedFiller2DNames) {
                auto child = parent.clone();
                child.partialSolution.push_back(factory.createFiller2D(
                    filler2DInit.first, filler2DInit.second, binType));
                for (auto& currentGroup : child.currentGroups.groups) {
                    Group previousGroup = currentGroup;
                    do {
                        previousGroup = currentGroup;
                        if (groupManager == GroupManager::SINGLE) {
                            currentGroup = singleItemApplyGroup(currentGroup,
                                    child.partialSolution, context);
                        }
                        if (groupManager == GroupManager::MAXAREA){
                            currentGroup = applyGroup(currentGroup, child.partialSolution, context,
                                [](const Layer& lhs, const Layer& rhs) -> bool {
                                    if (lhs.getLength() * lhs.getWidth() > rhs.getLength() * rhs.getWidth() ) {
                                        return true;
                                    }
                                    if (lhs.getLength() * lhs.getWidth() == rhs.getWidth() * rhs.getLength() ){
                                        if (lhs.getItemCount() > rhs.getItemCount()) {
                                            return true;
                                        }
                                    }
                                    return false;
                                },
                                [](const Stack& lhs, const Stack& rhs) -> bool {
                                    if (lhs.getLength() * lhs.getWidth() > rhs.getLength() * rhs.getWidth() ) {
                                        return true;
                                    }
                                    if (lhs.getLength() * lhs.getWidth() == rhs.getLength() * rhs.getWidth()) {
                                        if (lhs.getItemCount() > rhs.getItemCount()) {
                                            return true;
                                        }
                                    }
                                    return false;
                                });
                        }
                        if (groupManager == GroupManager::MAXWIDTH) {
                            currentGroup = applyGroup(currentGroup, child.partialSolution, context,
                                [](const Layer& lhs, const Layer& rhs) -> bool {
                                    if (lhs.getWidth() > rhs.getWidth() ) {
                                        return true;
                                    }
                                    if (lhs.getWidth() == rhs.getWidth()){
                                        if (lhs.getItemCount() > rhs.getItemCount()) {
                                            return true;
                                        }
                                    }
                                    return false;
                                },
                                [](const Stack& lhs, const Stack& rhs) -> bool {
                                    if (lhs.getWidth() > rhs.getWidth() ) {
                                        return true;
                                    }
                                    if (lhs.getWidth() == rhs.getWidth()) {
                                        if (lhs.getItemCount() > rhs.getItemCount()) {
                                            return true;
                                        }
                                    }
                                    return false;
                                },
                                true);
                        }
                    } while (currentGroup.count && currentGroup.count < previousGroup.count);
                }
                auto groupsToProcess = child.currentGroups.groups;
                child.currentGroups.groups.clear();
                for (const auto& group : groupsToProcess) {
                    if (group.count) {
                        child.currentGroups.groups.push_back(group);
                    }
                }
                result.currentSolutions.push_back(child);
            }
        }
    }
    return result;
}

Group LastBinClosingStrategy::singleItemLayerApplyGroup(
        Group group,
        const std::vector<IFiller2DPtr>& partialSolution,
        ConstContextPtr context) const{
    if (group.count) {
        Position position = Position::LENGTHWISE;
        if (context->getItemInfo(group.itemType).position != Position::BOTH) {
            position = context->getItemInfo(group.itemType).position;
        }
        if (Layer::ifCanCreate(1, 1, group.itemType, position, context)) {
            Layer layer(1, 1, group.itemType, position, context);
            bool response = false;
            size_t stackToPut;
            if (partialSolution.back()->ifCanPutLayer(&layer, &stackToPut, &response)) {
                partialSolution.back()->putLayer(layer, stackToPut);
                group.count -= 1;
            }
        }
    }
    return group;
}

Group LastBinClosingStrategy::singleItemStackApplyGroup(
        Group group,
        const std::vector<IFiller2DPtr>& partialSolution,
        ConstContextPtr context) const{
    Position position = Position::LENGTHWISE;
    if (context->getItemInfo(group.itemType).position != Position::BOTH) {
        position = context->getItemInfo(group.itemType).position;
    }
    Stack stack(context);
    for (size_t layerId = 0; layerId < group.count; ++layerId) {
        if (!Layer::ifCanCreate(1, 1, group.itemType, position, context)) {
            break;
        }
        Layer layer(1, 1, group.itemType, position, context);

        Stack candidate = stack;
        bool verificator;
        if (!candidate.ifCanPut(layer, &verificator)) {
            break;
        }
        candidate.carry(layer);
        if (partialSolution.back()->ifCanPutStack(candidate)) {
            stack = candidate;
        } else {
            break;
        }
    }
    if (stack.getLayerCount()) {
        partialSolution.back()->putStack(stack);
        group.count -= stack.getLayerCount();
    }
    return group;
}

Group LastBinClosingStrategy::singleItemApplyGroup(
        Group group,
        const std::vector<IFiller2DPtr>& partialSolution,
        ConstContextPtr context) const{
    Group previousGroup = group;
    auto updatedGroup = singleItemLayerApplyGroup(group, partialSolution, context);
    if (updatedGroup.count < previousGroup.count) {
        return updatedGroup;
    }
    return singleItemStackApplyGroup(updatedGroup, partialSolution, context);
}

Group LastBinClosingStrategy::applyLayerGroup(
        Group group,
        const std::vector<IFiller2DPtr>& partialSolution,
        ConstContextPtr context, 
        std::function<bool (const Layer&, const Layer&)> layerComparator, 
        const bool useWidthFactor) const {

    if (!group.count) {
        return group;
    }
    std::vector<Position> positionOptions;

    if (context->getItemInfo(group.itemType).position == Position::BOTH 
            || context->getItemInfo(group.itemType).position == Position::LENGTHWISE) {
        positionOptions.push_back(Position::LENGTHWISE);
    }

    if (context->getItemInfo(group.itemType).position == Position::BOTH 
            || context->getItemInfo(group.itemType).position == Position::WIDTHWISE) {
        positionOptions.push_back(Position::WIDTHWISE);
    }
    const Bin& bin = partialSolution.back()->getBin();
    const BinInfo& binInfo = context->getBinInfo(bin.getBinType());
    size_t binWidth = binInfo.width, binLength = binInfo.length;

    const ItemInfo& itemInfo = context->getItemInfo(group.itemType);
    size_t itemWidth = itemInfo.width;
    size_t itemLength = itemInfo.length;

    Layer selectedLayer(1, 1, group.itemType, positionOptions[0], context, false);

    for (auto position : positionOptions) {
        size_t currentLength = 0, currentWidth = 0;
        if (position == Position::LENGTHWISE) {
            currentLength = itemLength;
            currentWidth = itemWidth;
        } else {
            currentLength = itemWidth;
            currentWidth = itemLength;
        }
        for (size_t idx = 1; idx <= group.count && idx * currentWidth <= binWidth; idx++)  {
            for (size_t jnd = 1; idx * jnd <= group.count && jnd * currentLength <= binLength; jnd++) {
                if (Layer::ifCanCreate(idx, jnd, group.itemType, position, context))  {
                    Layer layer(idx, jnd, group.itemType, position, context, false);
                    if (layerComparator(layer, selectedLayer) == false) {
                        continue;
                    }
                    size_t stackToPut = 0;
                    bool response = false;
                    if (partialSolution.back()->ifCanPutLayer(&layer, &stackToPut, &response) ){
                        selectedLayer = layer;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
        }
    }
    bool response = false;
    size_t stackToPut = 0;
    if (useWidthFactor == true) {
        if (selectedLayer.getWidth() < WIDTH_FACTOR * binWidth){
            return singleItemLayerApplyGroup(group, partialSolution, context);
        }
    }
    while (group.count >= selectedLayer.getItemCount() 
            && partialSolution.back()->ifCanPutLayer(&selectedLayer, &stackToPut, &response)) {
        partialSolution.back()->putLayer(selectedLayer, stackToPut);
        group.count -= selectedLayer.getItemCount();
    }

    return group;
}

Group LastBinClosingStrategy::applyStackGroup(
        Group group,
        const std::vector<IFiller2DPtr>& partialSolution,
        ConstContextPtr context, 
        std::function<bool (const Stack&, const Stack&)> stackComparator, 
        const bool useWidthFactor) const {

    if (!group.count) {
        return group;
    }
    std::vector<Position> positionOptions;
    if (context->getItemInfo(group.itemType).position == Position::BOTH 
            || context->getItemInfo(group.itemType).position == Position::LENGTHWISE) {
        positionOptions.push_back(Position::LENGTHWISE);
    }

    if (context->getItemInfo(group.itemType).position == Position::BOTH 
            || context->getItemInfo(group.itemType).position == Position::WIDTHWISE) {
        positionOptions.push_back(Position::WIDTHWISE);
    }
    const Bin &bin = partialSolution.back()->getBin();
    const BinInfo &binInfo = context->getBinInfo(bin.getBinType());
    size_t binWidth = binInfo.width, binLength = binInfo.length;

    const ItemInfo &itemInfo = context->getItemInfo(group.itemType);
    size_t itemWidth = itemInfo.width;
    size_t itemLength = itemInfo.length;

    Stack selectedStack(context);

    for (auto position : positionOptions) {
        size_t currentLength = 0, currentWidth = 0;
        if (position == Position::LENGTHWISE) {
            currentLength = itemLength;
            currentWidth = itemWidth;
        } else {
            currentLength = itemWidth;
            currentWidth = itemLength;
        }
        for (size_t idx = 1; idx <= group.count && idx * currentWidth <= binWidth; idx++) {
            for (size_t jnd = 1; idx * jnd <= group.count && jnd * currentLength <= binLength; jnd++) {
                if (Layer::ifCanCreate(idx, jnd, group.itemType, position, context)) {
                    Layer layer(idx, jnd, group.itemType, position, context, false);
                    Stack candidate(context);

                    for (size_t height = 1; height * layer.getItemCount() <= group.count; height++){
                        bool response = false;
                        if (candidate.ifCanPut(layer, &response)) {
                            candidate.carry(layer);
                        } else {
                            break;
                        }

                        if (stackComparator(candidate, selectedStack) == false) {
                            continue;
                        }

                        if (partialSolution.back()->ifCanPutStack(candidate) ) {
                            selectedStack = candidate;
                        } else {
                            break;
                        }
                    }
                }
            }
        }
    }

    if (useWidthFactor == true) {
        if (selectedStack.getWidth() < WIDTH_FACTOR * binWidth){
            return singleItemStackApplyGroup(group, partialSolution, context);
        }
    }
    if (selectedStack.getItemCount() == 0) {
        return group;
    }
    while (group.count >= selectedStack.getItemCount() 
            && partialSolution.back()->ifCanPutStack(selectedStack)) {
        partialSolution.back()->putStack(selectedStack);
        group.count -= selectedStack.getItemCount();
    }
    return group;
}
Group LastBinClosingStrategy::applyGroup(Group group,
        const std::vector<IFiller2DPtr>& partialSolution,
        ConstContextPtr context,
        std::function<bool (const Layer&, const Layer&)> layerComparator,
        std::function<bool (const Stack&, const Stack&)> stackComparator, 
        const bool useWidthFactor) const {
    auto updatedGroups = applyLayerGroup(
        group, partialSolution, context, layerComparator, useWidthFactor);
    return applyStackGroup(
        updatedGroups, partialSolution, context, stackComparator, useWidthFactor);
}
} // namespace esicup
