/**
 * @file iteration_strategy.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines iteration strategy - algorithm responsible for genetic
 * evolution by expanding previous set of solutions.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "data.h"
#include "filler2d/factory.h"

#include <vector>
#include <functional>

/// @brief Factor to account for partial width utilization.
const double WIDTH_FACTOR = 0.93;

namespace esicup {

/**
 * @brief Enumeration on the approaches to pack groups inside bin.
 */
enum class GroupManager{
    SINGLE,     ///< Everything from single item.
    MAXWIDTH,   ///< Width-driven heuristic.
    MAXAREA,    ///< Area-driven heuristic.
};

/**
 * @brief Interface for iteration strategy - approach
 * designed to create further solutions extending previously
 * supplied.
 */
class IIterationStrategy {
public:
    /**
     * @brief Iteration of the genetic algorithm. Extends 
     * or modifies previous solutions and creates further
     * solutions.
     * 
     * @param previousData The previous state of genetic evolution.
     * @return Updated state of genetic evolution.
     */
    virtual GeneticData iterate(
            const GeneticData& previousData) const = 0;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~IIterationStrategy() {};
};

/**
 * @brief Genetic evolution by packing non-packed items to different
 * bins and according to different filler types and layer strategies.
 * First creates one-level layers, and then grows. 
 */
class LastBinClosingStrategy final: public IIterationStrategy {
public:
    /**
     * @brief Constructor for last bin closing strategy.
     * 
     * @param binTypes The indices of bin types to try.
     * @param filler2DConfigurations Available configurations for 2D fillers.
     * @param factory Abstract factory for creating fillers.
     * @param groupManager Policy around processing groups.
     * @param context Context with instance-specific input information.
     */
    LastBinClosingStrategy(
            const std::vector<size_t>& binTypes,
            const std::vector<std::pair<FillerType, LayerStrategyType>>& filler2DConfigurations,
            const FillerFactory& factory,
            const GroupManager& groupManager,
            const ConstContextPtr& context);
    /**
     * @brief Iteration of genetic strategy. Packs non-packed items
     * to one more bin according to different policies.
     * 
     * @param previousData Previous state of genetic evolution.
     * @return Updated state of genetic evolution.
     */
    GeneticData iterate(
            const GeneticData& previousData) const override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~LastBinClosingStrategy() {};

private:
    /**
     * @brief Creates layers of a single item and tries to pack to some 
     * existing stack inside the bin.
     * 
     * @param group Group to pack.
     * @param partialSolution The currently processed partial solution.
     * @param context Context with instance-specific input information.
     * 
     * @return The group of residual non-packed items.
     */
    Group singleItemLayerApplyGroup(Group group,
        const std::vector<IFiller2DPtr>& partialSolution,
        ConstContextPtr context) const;
    /**
     * @brief Creates stacks with single items and tries to pack 
     * inside the bin.
     * 
     * @param group Group to pack.
     * @param partialSolution  The currently processed partial solution.
     * @param context Context with instance-specific input information.
     * 
     * @return The group of residual non-packed items. 
     */
    Group singleItemStackApplyGroup(Group group,
        const std::vector<IFiller2DPtr>& partialSolution,
        ConstContextPtr context) const;
    /**
     * @brief First tries to pack an item inside some existing stack
     * through singleItemLayerApplyGroup, then tries to pack into
     * new stacks with singleItemStackApplyGroup.
     * 
     * @param group Group to pack.
     * @param partialSolution The currently processed partial solution.
     * @param context Context with instance-specific input information.
     * 
     * @return The group of residual non-packed items. 
     */
    Group singleItemApplyGroup(Group group,
        const std::vector<IFiller2DPtr>& partialSolution,
        ConstContextPtr context) const;
    /**
     * @brief Creates rectangular layers of items and tries to pack them 
     * into some existing stack inside the bin. The candidate layers are 
     * compared according to layer comparator.
     * 
     * @param group Group to pack.
     * @param partialSolution The currently processed partial solution.
     * @param context Context with instance-specific input information.
     * @param layerComparator Function to compare different layers. 
     * @param useWidthFactor Specific factor for incomplete width utilization.
     * 
     * @return The group of residual non-packed items.
     */
    Group applyLayerGroup(Group group,
        const std::vector<IFiller2DPtr>& partialSolution,
        ConstContextPtr context, 
        std::function<bool (const Layer&, const Layer&)> layerComparator, 
        const bool useWidthFactor = false) const;
    /**
     * @brief Creates box stacks with n_width x n_length x n_height 
     * items and tries to pack inside the bin.
     * 
     * @param group Group to pack.
     * @param partialSolution The currently processed partial solution.
     * @param context Context with instance-specific input information.
     * @param stackComparator Function to compare stacks.
     * @param useWidthFactor Specific factor for incomplete width utilization.
     * 
     * @return The group of residual non-packed items. 
     */
    Group applyStackGroup(Group group,
        const std::vector<IFiller2DPtr>& partialSolution,
        ConstContextPtr context, 
        std::function<bool (const Stack&, const Stack&)> stackComparator, 
        const bool useWidthFactor = false) const;
    /**
     * @brief First tries to pack items inside some existing stack
     * through applyLayerGroup, then tries to pack into
     * new stacks with applyStackGroup.
     * 
     * @param group Group to pack.
     * @param partialSolution The currently processed partial solution.
     * @param context Context with instance-specific input information.
     * @param layerComparator Function to compare different layers. 
     * @param stackComparator Function to compare stacks.
     * @param useWidthFactor Specific factor for incomplete width utilization.
     * 
     * @return The group of residual non-packed items. 
     */
    Group applyGroup(Group group,
        const std::vector<IFiller2DPtr>& partialSolution,
        ConstContextPtr context,
        std::function<bool (const Layer&, const Layer&)> layerComparator,
        std::function<bool (const Stack&, const Stack&)> stackComparator, 
        const bool useWidthFactor = false) const;
        
private:
    /// @brief Vector with allowed bin types.
    std::vector<size_t> allowedBinTypes;
    /// @brief Vector with allowed filler configurations.
    std::vector<std::pair<FillerType, LayerStrategyType>> allowedFiller2DNames;
    /// @brief Type of policy towards group allocation.
    GroupManager groupManager;
    /// @brief Factory for creating fillers.
    FillerFactory factory;
    /// @brief Instance-specific input information.
    ConstContextPtr context;
};

} // namespace esicup
