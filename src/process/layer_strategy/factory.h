/**
 * @file factory.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines factory for layer strategies - entities for adding layers to stacks.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */

#pragma once
#include "options.h"
#include "layer_strategy.h"

namespace esicup {

/**
 * @brief Abstract factory to create layer strategies.
 * Layer strategy is responsible for adding layers to stacks.
 */
class LayerStrategyFactory {
public:
    /**
     * @brief Factory method to create layer strategies.
     * 
     * @param type The type of layer strategy.
     * 
     * @return Pointer to a newly created layer strategy.
     */
    static LayerStrategyPtr createLayerStrategy(
            const LayerStrategyType& type);
};

} // namespace esicup