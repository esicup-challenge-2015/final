/// Implements api defined in factory.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "factory.h"

#include <memory>
#include <exception>

namespace esicup {


LayerStrategyPtr LayerStrategyFactory::createLayerStrategy(
        const LayerStrategyType& layerStrategyType) {
    switch (layerStrategyType) {
        case LayerStrategyType::MINIMAL_LOST_AREA: {
            return std::make_shared<MinimalLostAreaLayerStrategy>(layerStrategyType);
        }
        case LayerStrategyType::FIRST_FIT:  {
            return std::make_shared<FirstFitLayerStrategy>(layerStrategyType);
        }
        default: {
            throw std::logic_error("Unexpected layer strategy!");
        }
    }
    return nullptr;
}

}