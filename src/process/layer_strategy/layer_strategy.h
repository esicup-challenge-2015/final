/**
 * @file layer_strategy.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines interface and implementations for layer strategies - entities
 * to verify if layer can be added to some stack in a particular bin, finding
 * such stack and adding if no problem has been found. 
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "../../common.h"
#include "options.h"

#include <memory>

namespace esicup {

// Layer Strategy Type enum class definition can be found in interfaces.h.

/**
 * @brief Layer strategy is responsible for verifying if layer can be added to stack
 * inside a particular bin, and for adding to that stack if no problem has been found.
 */    
class ILayerStrategy {
public:
    /**
     * @brief Constructor for ILayerStrategy.
     * 
     * @param strategyType The type of layer strategy for self-identification.
     */
    ILayerStrategy(const LayerStrategyType& strategyType);
    /**
     * @brief Verifies if layer strategy can put layer to some stack inside the bin.
     * 
     * @param targetBin The bin to put the layer inside.
     * @param layer The layer for loading to the bin.
     * @param willFinalize Whether that will finalize the stack after loading.
     * @param stackToIdxPut The stack index where the layer would fit.
     * 
     * @return True if the relevant opportunity is found, and False otherwise.
     */
    virtual bool ifCanPut(
            std::shared_ptr<const Bin> targetBin,
            Layer* layer,
            bool* willFinalize,
            size_t* stackIdxToPut) = 0;
    /**
     * @brief Loads the layer to the stack inside the bin.
     * 
     * @param layer The layer to carry.
     * @param stackId The stack index to load.
     * @param targetBin The bin for loading the stack.
     */
    virtual void carry(const Layer& layer,
            size_t stackId,
            std::shared_ptr<Bin> targetBin) = 0;
    /**
     * @brief Returns layer strategy type for self-identification.
     * 
     * @return The type of layer strategy.
     */
    LayerStrategyType getType() const;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~ILayerStrategy();

protected:
    /// @brief The type of the layer strategy.
    LayerStrategyType type;
};

/// @brief Standard smart pointer to a layer strategy.
typedef std::shared_ptr<ILayerStrategy> LayerStrategyPtr;
/// @brief Standard smart pointer to constant layer strategy.
typedef std::shared_ptr<const ILayerStrategy> LayerStrategyConstPtr;

class MinimalLostAreaLayerStrategy final : public ILayerStrategy {
public:
    /**
     * @brief Constructor for Minimal Lost Area layer strategy.
     * 
     * @param strategyType The type of layer strategy for self-identification.
     */
    MinimalLostAreaLayerStrategy(const LayerStrategyType& strategyType);
    /**
     * @brief Verifies if layer strategy can put layer to some stack inside the bin.
     * If there is an opportunity, returns True and writes the feasible layer form,
     * stack index and whether that would finalize the stack into the parameter pointers.
     * The stack is selected such that the minimal area is wasted.
     * 
     * @param targetBin The bin to put the layer inside.
     * @param layer The layer for loading to the bin.
     * @param willFinalize Whether that will finalize the stack after loading.
     * @param stackIdxToPut The stack index where the layer would fit.
     * 
     * @return True if the relevant opportunity is found, and False otherwise.
     */
    virtual bool ifCanPut(
            std::shared_ptr<const Bin> targetBin,
            Layer* layer,
            bool* willFinalize,
            size_t* stackIdxToPut) override;
    /**
     * @brief Loads the layer to the stack inside the bin.
     * 
     * @param layer The layer to carry.
     * @param stackId The stack index to load.
     * @param targetBin The bin for loading the stack.
     */
    void carry(const Layer& layer,
            size_t stackId,
            std::shared_ptr<Bin> targetBin) override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~MinimalLostAreaLayerStrategy();
};

class FirstFitLayerStrategy final : public ILayerStrategy {
public:
    /**
     * @brief Constructor for FirstFitLayerStrategy.
     * 
     * @param strategyType The type of layer strategy for self-identification.
     */
    FirstFitLayerStrategy(const LayerStrategyType& strategyType);
    /**
     * @brief Verifies if layer strategy can put layer to some stack inside the bin.
     * If there is an opportunity, returns True and writes the feasible layer form,
     * stack index and whether that would finalize the stack into the parameter pointers.
     * The first stack that fits is selected.
     * 
     * @param targetBin The bin to put the layer inside.
     * @param layer The layer for loading to the bin.
     * @param willFinalize Whether that will finalize the stack after loading.
     * @param stackIdxToPut The stack index where the layer would fit.
     * 
     * @return True if the relevant opportunity is found, and False otherwise.
     */
    virtual bool ifCanPut(
            std::shared_ptr<const Bin> targetBin,
            Layer* layer,
            bool* willFinalize,
            size_t* stackIdxToPut) override;
    /**
     * @brief Loads the layer to the stack inside the bin.
     * 
     * @param layer The layer to carry.
     * @param stackId The stack index to load.
     * @param targetBin The bin for loading the stack.
     */
    void carry(const Layer& layer,
            size_t stackId,
            std::shared_ptr<Bin> targetBin) override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~FirstFitLayerStrategy() override {};
};

} // namespace esicup
