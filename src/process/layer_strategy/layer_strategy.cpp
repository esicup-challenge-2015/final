/// Implements API and classes defined in layer_strategy.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "layer_strategy.h"

#include <memory>
#include <cassert>
#include <vector>


namespace esicup {

// ILayerStrategy.

ILayerStrategy::ILayerStrategy(const LayerStrategyType& strategyType)
    : type(strategyType) {
}

LayerStrategyType ILayerStrategy::getType() const {
    return type;
}

ILayerStrategy::~ILayerStrategy() {}

// MinimalLostAreaLayerStrategy.

MinimalLostAreaLayerStrategy::MinimalLostAreaLayerStrategy(
        const LayerStrategyType& strategyType) : ILayerStrategy(strategyType) {
}

bool MinimalLostAreaLayerStrategy::ifCanPut(
        std::shared_ptr<const Bin> targetBin,
        Layer* layer,
        bool* willFinalize,
        size_t* stackIdxToPut) {
    *willFinalize = false;
    bool positionFound = false;
    size_t stackCount = targetBin->getStackCount();
    size_t selectedAreaDifference = 0;
    bool differenceSet = false;
    std::vector<Layer> candidates = {*layer, *layer};
    if (candidates.front().ifCanFlip()) {
        candidates.front().flip();
    }
    if (candidates.front().getWidth() > candidates.front().getLength()) {
        std::swap(candidates.front(), candidates.back());
    }
    if (layer->getWeight() + targetBin->getWeight()
            > targetBin->getMaximalAllowedWeight()) {
        // Expected to speed up check performance.
        positionFound = false;
    } else {
        for (size_t stackId = 0; stackId < stackCount; ++stackId)  {
            bool innerFinalize = false;
            for (auto& target : candidates) {
                if (targetBin->ifCanPutLayerToStack(stackId, target, &innerFinalize)) {
                    positionFound = true;
                    auto stackArea = targetBin->getStackById(stackId).getBoxArea();
                    auto layerArea = target.getBoxArea();
                    assert(stackArea >= layerArea);
                    auto areaDifference = stackArea - layerArea;
                    if (differenceSet
                            && selectedAreaDifference == areaDifference
                            && !innerFinalize) {
                        *stackIdxToPut = stackId;
                        *willFinalize = innerFinalize;
                        *layer = target;
                    }
                    if (!differenceSet || areaDifference < selectedAreaDifference) {
                        selectedAreaDifference = areaDifference;
                        *stackIdxToPut = stackId;
                        differenceSet = true;
                        *willFinalize = innerFinalize;
                        *layer = target;
                    }
                }
            }
        }
    }
    return positionFound;
}

void MinimalLostAreaLayerStrategy :: carry(
        const Layer& layer,
        size_t stackToPut,
        std::shared_ptr<Bin> targetBin){
    targetBin->loadLayerToStack(stackToPut, layer);
}

MinimalLostAreaLayerStrategy::~MinimalLostAreaLayerStrategy() {}

// FirstFitLayerStrategy

FirstFitLayerStrategy::FirstFitLayerStrategy(
    const LayerStrategyType& strategyType) 
    : ILayerStrategy(strategyType) {
}

bool FirstFitLayerStrategy::ifCanPut(
        std::shared_ptr<const Bin> targetBin,
        Layer* layer,
        bool* willFinalize,
        size_t* stackIdxToPut) {
    Layer flippedLayer = *layer, normalLayer = *layer;
    if (flippedLayer.ifCanFlip()) {
        flippedLayer.flip();
    }
    std::vector<Layer> candidates = {flippedLayer, normalLayer};
    for (size_t stackId = 0; stackId < targetBin->getStackCount(); ++stackId) {
        for (auto& candidateLayer : candidates) {
            if (targetBin->ifCanPutLayerToStack(stackId, candidateLayer, willFinalize)) {
                *layer = candidateLayer;
                *stackIdxToPut = stackId;
                return true;
            }
        }
    }
    return false;
}

void FirstFitLayerStrategy::carry(
        const Layer& layer,
        size_t stackId,
        std::shared_ptr<Bin> targetBin) {
    targetBin->loadLayerToStack(stackId, layer);
}



} // namespace esicup

