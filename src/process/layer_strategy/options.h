/**
 * @file options.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines enumeration to name layer strategies.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

namespace esicup {

/**
 * @brief Enumeration to define layer strategy types.
 */
enum class LayerStrategyType {
    MINIMAL_LOST_AREA, ///< Finds stack s.t. the inefficiently utilized area is minimized.
    FIRST_FIT          ///< Loads layer on the first stack that fits.
};

}