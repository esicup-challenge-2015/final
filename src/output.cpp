/// Implements structures and API defined in output.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "output.h"

#include <sstream>
#include <algorithm>
#include <cassert>

namespace esicup {

void Output::initialize(const TestOutputData& outputData) {
    context = outputData.context;
    idManager.setInstanceItems(outputData.context);
    writeBinId = writeStackId = writeLayerId = writeRowId = 0;
}

void Output::write(const TestOutputData& outputData) {
    initialize(outputData);

    outBinsStream.open(outputData.testMetadata.pathToTest + "/bins.csv");
    outStacksStream.open(outputData.testMetadata.pathToTest + "/stacks.csv");
    outLayersStream.open(outputData.testMetadata.pathToTest + "/layers.csv");
    outRowsStream.open(outputData.testMetadata.pathToTest + "/rows.csv");
    outItemsStream.open(outputData.testMetadata.pathToTest + "/items.csv");

    outBinsStream   << "id bin;bin type;" << std::endl;
    outStacksStream << "id stack;id bin;xO;yO;zO;xE;yE;zE;" << std::endl;
    outLayersStream << "id layer;id stack;xO;yO;zO;xE;yE;zE;flag top;" << std::endl;
    outRowsStream   << "id row;id layer;xO;yO;zO;xE;yE;zE;" << std::endl;
    outItemsStream  << "id item;id row;xO;yO;zO;xE;yE;zE;" << std::endl;

    outputBins(outputData.solution.getBins());

    outBinsStream.close();
    outStacksStream.close();
    outLayersStream.close();
    outRowsStream.close();
    outItemsStream.close();
}

void Output::outputBins(const std::vector<Bin>& bins) {
    std::vector<Bin> sortedBins(bins);
    for (const auto& bin: sortedBins) {
        std::ostringstream stream;
        stream << writeBinId << ";"
            << context->getBinInfo(bin.getBinType()).binName << ";";

        outBinsStream << stream.str() << std::endl;
        outputStacks(bin.getStackPlacements());
        ++writeBinId;
    }
}

void Output::outputStacks(const std::vector<LoadedStack>& stacks) {
    for (const auto& loadedStack: stacks) {
        std::ostringstream stream;
        Coordinates3D origin = {
            loadedStack.lengthShift, 
            loadedStack.widthShift, 
            0
        };
        Coordinates3D extremity = {
            origin.xCoordinate + loadedStack.stack.getLength(),
            origin.yCoordinate + loadedStack.stack.getWidth(),
            loadedStack.stack.getHeight()
        };

        stream << writeStackId << ";"
            << writeBinId << ";"
            << origin.xCoordinate << ";"
            << origin.yCoordinate << ";"
            << origin.zCoordinate << ";"
            << extremity.xCoordinate << ";"
            << extremity.yCoordinate << ";"
            << extremity.zCoordinate << ";";

        outStacksStream << stream.str() << std::endl;

        outputLayers(loadedStack.stack.getLayerPlacements(), origin);
        ++writeStackId;
    }
}

void Output::outputLayers(const std::vector<LiftedLayer>& layers,
                          const Coordinates3D& stackOrigin) {

    size_t count = 0;
    for (const auto& liftedLayer: layers) {
        std::ostringstream stream;
        //layers are sorted by weight
        ++count;
        bool isTop = (count == layers.size());

        stream << writeLayerId << ";"
            << writeStackId << ";"
            << stackOrigin.xCoordinate << ";"
            << stackOrigin.yCoordinate << ";"
            << liftedLayer.heightShift << ";"
            << liftedLayer.layer.getLength() + stackOrigin.xCoordinate << ";"
            << liftedLayer.layer.getWidth() + stackOrigin.yCoordinate << ";"
            << liftedLayer.layer.getHeight() + liftedLayer.heightShift << ";"
            << isTop << ";";
        outLayersStream << stream.str() << std::endl;

        Coordinates3D coordinatesOfLayer = stackOrigin;;
        coordinatesOfLayer.zCoordinate = liftedLayer.heightShift;

        outputRows(liftedLayer, coordinatesOfLayer);
        ++writeLayerId;
    }
}

void Output::outputRows(const LiftedLayer& liftedLayer,
                        const Coordinates3D& layerOrigin) {

    size_t countRows = 0, countItemsInRow = 0;
    Position rowPosition;
    bool initSuccess = false;

    // Here we assume that there exist at least one way to form pair (countRows, countItemsInRow).
    const Constraints& constraints = context->getConstraints();
    if (constraints.max_number_of_items_in_a_row >= liftedLayer.layer.getLengthInItems() &&
            constraints.max_number_of_rows_in_a_layer >= liftedLayer.layer.getWidthInItems()) {
        countRows = liftedLayer.layer.getWidthInItems();
        countItemsInRow = liftedLayer.layer.getLengthInItems();
        rowPosition = Position::LENGTHWISE;
        initSuccess = true;
    } else if (constraints.max_number_of_items_in_a_row >= liftedLayer.layer.getWidthInItems() &&
            constraints.max_number_of_rows_in_a_layer >= liftedLayer.layer.getLengthInItems()) {
        countRows = liftedLayer.layer.getLengthInItems();
        countItemsInRow = liftedLayer.layer.getWidthInItems();
        rowPosition = Position::WIDTHWISE;
        initSuccess = true;
    }

    assert(initSuccess);

    Position itemPosition = liftedLayer.layer.getItemPosition();

    size_t addXValue = liftedLayer.layer.getLength() / liftedLayer.layer.getLengthInItems();
    size_t addYValue = liftedLayer.layer.getWidth() / liftedLayer.layer.getWidthInItems();

    Coordinates3D rowOrigin = layerOrigin;
    Coordinates3D rowExtremity = layerOrigin;

    rowExtremity.zCoordinate += liftedLayer.layer.getHeight();

    if (rowPosition == Position::WIDTHWISE) {
        rowExtremity.xCoordinate += addXValue;
        rowExtremity.yCoordinate += liftedLayer.layer.getWidth();
    } else {
        rowExtremity.xCoordinate += liftedLayer.layer.getLength();
        rowExtremity.yCoordinate += addYValue;
    }

    for (size_t idx = 0; idx < countRows; ++idx) {
        std::ostringstream stream;
        stream << writeRowId << ";" << writeLayerId << ";"
            << rowOrigin.xCoordinate << ";" << rowOrigin.yCoordinate << ";" << rowOrigin.zCoordinate << ";"
            << rowExtremity.xCoordinate << ";" << rowExtremity.yCoordinate  << ";" << rowExtremity.zCoordinate << ";";
        outRowsStream << stream.str() << std::endl;

        outputItems(
            liftedLayer.layer.getItemType(), countItemsInRow, 
            rowOrigin, rowPosition, itemPosition);
        ++writeRowId;

        if (rowPosition == Position::WIDTHWISE) {
            rowOrigin.xCoordinate += addXValue;
            rowExtremity.xCoordinate += addXValue;
        } else {
            rowOrigin.yCoordinate += addYValue;
            rowExtremity.yCoordinate += addYValue;
        }
    }
}

void Output::outputItems(size_t itemType,
                         size_t countItems,
                         const Coordinates3D& rowOrigin,
                         Position rowPosition,
                         Position itemPosition) {
    size_t addXValue = 0;
    size_t addYValue = 0;

    if (itemPosition == Position::LENGTHWISE) {
        addXValue = context->getItemInfo(itemType).length;
        addYValue = context->getItemInfo(itemType).width;
    } else {
        addXValue = context->getItemInfo(itemType).width;
        addYValue = context->getItemInfo(itemType).length;
    }

    size_t xOrigin = rowOrigin.xCoordinate;
    size_t yOrigin = rowOrigin.yCoordinate;
    size_t zOrigin = rowOrigin.zCoordinate;
    size_t zExtremity = zOrigin + context->getItemInfo(itemType).height;
    size_t xExtremity = xOrigin + addXValue;
    size_t yExtremity = yOrigin + addYValue;

    for (size_t idx = 0; idx < countItems; ++idx) {
        std::ostringstream stream;

        stream << idManager.getAvailableId(itemType) << ";"
            << writeRowId << ";"
            << xOrigin << ";" << yOrigin << ";" << zOrigin << ";"
            << xExtremity << ";" << yExtremity << ";" << zExtremity << ";";
        outItemsStream << stream.str() << std::endl;

        if (rowPosition == Position::WIDTHWISE) {
            yOrigin += addYValue;
            yExtremity += addYValue;
        } else {
            xOrigin += addXValue;
            xExtremity += addXValue;
        }
    }
}

} // namespace esicup

