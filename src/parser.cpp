/// Implements API defined in parser.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "parser.h"

#include <map>
#include <fstream>
#include <iostream>
#include <cassert>


namespace esicup {

void printUsage(int argc, char** argv) {
    assert(argc > 0);
    std::cout << "Usage: " << argv[0] 
        << "-p set_of_instances_directory " 
        << "-a accelerated scenario " <<  std::endl;
    std::cout << "Example: " << argv[0]
        << "-p data/instancesA " 
        << "-a YES" << std::endl;
    std::cout << "Example: " << argv[0]
        << "-p data/instancesA " 
        << "-a NO" << std::endl; 
}


InstancesData Parser::parse(int argc, char** argv) {
    InstancesData data;
    data.initOk = true;

    const int ARGUMENT_COUNT = 5;
    if (argc != ARGUMENT_COUNT) {
        data.initOk = false;
    }
    else {
        std::map<std::string, std::string> cliParameters;
        for (size_t idx = 1; idx < ARGUMENT_COUNT; idx += 2) {
            cliParameters[argv[idx]] = argv[idx + 1];
        }

        typedef std::map<std::string, std::string>::iterator ParseIterator;

        ParseIterator location = cliParameters.find("-p");
        if (location != cliParameters.end()) {
            data.pathToSet = location->second;
        } else {
            data.initOk = false;
        }

        ParseIterator acceleration = cliParameters.find("-a");
        if (location != cliParameters.end()) {
            std::string argument = acceleration->second;
            if (argument == "YES") {
                data.accelerate = true;
            } else if (argument == "NO") {
                data.accelerate = false;
            } else {
                data.initOk = false;
            }
        } else {
            data.initOk = false;
        }

        if (data.initOk)
            data.innerDirectories = extractDirectoriesNames(data.pathToSet + instancesFileSuffix);
    }

    return data;
}

std::vector<std::string> Parser::extractDirectoriesNames(const std::string& path) {
    std::ifstream stream(path);
    if (!stream) {
        throw std::runtime_error("Cannot open list_instances file");
    }

    std::vector<std::string> directories;
    std::string line;
    while (stream >> line) {
        directories.push_back(line);
    }
    return directories;
}

const std::string Parser::instancesFileSuffix = "/list_instances.csv";

} //namespace esicup
