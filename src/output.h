/**
 * @file output.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines interface and implementation for the output class.
 * Output class is designed to export output data into the checker-compatible format.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "interfaces.h"
#include <fstream>

namespace esicup {

/**
 * @brief Output interface. Designed to export 
 *     output data into checker-compatible format.
 */
class IOutput {
public:
    /**
     * @brief Writes output data into checker-compatible format.
     * 
     * @param data The output data to export.
     */
    virtual void write(const TestOutputData& data) = 0;
    /**
     * @brief Virtual destructor for correct C++ polymorphism.
     */
    virtual ~IOutput() {};
};

/**
 * @brief Implements export of the output data to the
 * esicup checker compatible format.
 */
class Output: public IOutput {
public:
    /**
     * @brief Writes output data into checker-compatible format.
     * 
     * @param data The output data to export.
     */
    void write(const TestOutputData& data) override;
    /**
     * @brief Virtual destructor for correct C++ polymorphism.
     */
    virtual ~Output() {};
private:
    /**
     * @brief Defines 3-dimensional Cartesian coordinates.
     */
    struct Coordinates3D {
        /// @brief Coordinate along x-axis.
        size_t xCoordinate;
        /// @brief Coordinate along y-axis.
        size_t yCoordinate;
        /// @brief Coordinate along z-axis.
        size_t zCoordinate;
    };

private:
    /**
     * @brief Initializes inner structures according to the supplied output data.
     * 
     * @param outputData The data to export.
     */
    void initialize(const TestOutputData& outputData);
    /**
     * @brief Outputs bins-related information (through outBinsStream).
     * 
     * @param bins The bins to export.
     */
    void outputBins(const std::vector<Bin>& bins);
    /**
     * @brief Outputs stack-related information (through outStacksStream).
     * 
     * @param stacks The stacks to export.
     */
    void outputStacks(const std::vector<LoadedStack>& stacks);
    /**
     * @brief Outputs layer-related information (through outLayersStream).
     * 
     * @param layers The layers to export.
     * @param stackOrigin The stack origin.
     */
    void outputLayers(const std::vector<LiftedLayer>& layers, const Coordinates3D& stackOrigin);
    /**
     * @brief Outputs row-related information (through outRowsStream).
     * 
     * @param layer The layer to export.
     * @param layerOrigin The layer origin.
     */
    void outputRows(const LiftedLayer& layer, const Coordinates3D& layerOrigin);
    /**
     * @brief Outputs item-related information (through outItemsStream).
     * 
     * @param itemType The index of the item type.
     * @param countItems The number of the items.
     * @param rowOrigin The origin of the row.
     * @param rowPosition The position of the row.
     * @param itemPosition The position of the items.
     */
    void outputItems(
        size_t itemType,
        size_t countItems,
        const Coordinates3D& rowOrigin,
        Position rowPosition,
        Position itemPosition);

private:
    /// @brief Stream for exporting bins.
    std::ofstream outBinsStream;
    /// @brief Stream for exporting stacks.
    std::ofstream outStacksStream;
    /// @brief Stream for exporting layers.
    std::ofstream outLayersStream;
    /// @brief Stream for exporting rows.
    std::ofstream outRowsStream;
    /// @brief Stream for exporting items.
    std::ofstream outItemsStream;

    /// @brief Index of the bin to write.
    size_t writeBinId;
    /// @brief Index of the stack to write.
    size_t writeStackId;
    /// @brief Index of the layer to write.
    size_t writeLayerId;
    /// @brief Index of the row to write.
    size_t writeRowId;

    /// @brief Index manager. Retrieves item ids one-by-one 
    /// in the input-specific format.
    ItemIdManager idManager;
    /// @brief Context with input-specific information.
    ConstContextPtr context;
};

} // namespace esicup

