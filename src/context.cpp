/// Implements structures and API defined in context.h
/// 
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "context.h"

#include <cassert>
#include <iostream>
#include <algorithm>
#include <cmath>


namespace esicup {

bool ItemInfo::operator==(const ItemInfo& other) const {
    const double EPS = 1e-7;
    return material == other.material
        && width == other.width
        && length == other.length
        && height == other.height
        && position == other.position
        && (fabs(weight - other.weight) < EPS);
}

unsigned long long ItemInfo::getVolume() const {
    return static_cast<unsigned long long>(width) 
        * static_cast<unsigned long long>(length) 
        * static_cast<unsigned long long>(height);
}

unsigned long long BinInfo::getVolume() const {
    return static_cast<unsigned long long>(width) 
        * static_cast<unsigned long long>(length) 
        * static_cast<unsigned long long>(height);
}

const BinInfo& Context::getBinInfo(size_t binType) const {
    assert(binType < instanceBins.size());
    return instanceBins[binType];
}

const ItemInfo& Context::getItemInfo(size_t itemType) const {
    assert(itemType < instanceItems.size());
    return instanceItems[itemType].info;
}

const Constraints& Context::getConstraints() const {
    return constraints;
}

size_t Context::getBinTypesCount() const {
    return instanceBins.size();
}

size_t Context::getItemTypesCount() const {
    return instanceItems.size();
}

size_t Context::getItemCount(size_t itemType) const {
    assert(itemType < instanceItems.size());
    return instanceItems[itemType].inputIDs.size();
}

void Context::addItem(ItemInfo info, size_t itemId) {
    if (itemInfoToType.find(info) == itemInfoToType.end()) {
        itemInfoToType[info] = instanceItems.size();
        ItemInfoWrapper productInfo;
        productInfo.info = info;
        instanceItems.push_back(productInfo);
    }
    int type = itemInfoToType[info];
    instanceItems[type].inputIDs.push_back(itemId);
    ++productCount[info.productName];
}

void Context::addBin(BinInfo info) {
    if (!instanceBins.size()) {
        stats.maxAllowedBinWeight = info.allowedWeight;
        stats.minBinVolume = info.getVolume();
    } 
    stats.maxAllowedBinWeight = std::max(stats.maxAllowedBinWeight, info.allowedWeight);
    stats.minBinVolume = std::min(info.getVolume(), stats.minBinVolume);
    instanceBins.push_back(info);
}

unsigned long long Context::getMinBinVolume() const {
    return stats.minBinVolume;
}

double Context::getMaxAllowedBinWeight() const {
    return stats.maxAllowedBinWeight;
}

void Context::setConstraints(Constraints data) {
    constraints = data;
}

const std::vector<ItemInfoWrapper>& Context::getAllItems() const {
    return instanceItems;
}

size_t Context::getProductCount(const std::string& productName) const {
    size_t answer = 0;
    if (productCount.find(productName) != productCount.end()) {
        answer = productCount.at(productName);
    }
    return answer;
}

ItemIdManager::ItemIdManager(ConstContextPtr context)
        : instanceItems(context->getAllItems())  {
}

void ItemIdManager::setInstanceItems(ConstContextPtr context) {
    instanceItems = context->getAllItems();
}

size_t ItemIdManager::getAvailableId(size_t itemType) {
    assert(itemType < instanceItems.size());
    auto& productInfo = instanceItems[itemType];
    assert(productInfo.inputIDs.size() > 0);
    size_t answer = productInfo.inputIDs.back();
    productInfo.inputIDs.pop_back();
    return answer;
}

} // namespace esicup
