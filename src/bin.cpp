/// Implements API defined in common.h related to Bins.
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024
#include "common.h"

#include <unordered_map>
#include <iostream>
#include <cassert>
#include <algorithm>


namespace esicup {

bool ifStackPlacementsOverlap(
        const Stack &candidate,
        size_t widthShift,
        size_t lengthShift,
        const LoadedStack &fixed) {
    size_t maxSmallerLength = std::max(lengthShift, fixed.lengthShift);
    size_t minBiggerLength = std::min(lengthShift + candidate.getLength(), 
                                      fixed.lengthShift + fixed.stack.getLength());

    size_t maxSmallerWidth = std::max(widthShift, fixed.widthShift);
    size_t minBiggerWidth = std::min(widthShift + candidate.getWidth(), 
                                     fixed.widthShift + fixed.stack.getWidth());

    return (maxSmallerLength < minBiggerLength && maxSmallerWidth < minBiggerWidth);
}


Bin::Bin(size_t binType, ConstContextPtr context)
        : IBox(context)
        , binType(binType) {
    stats = {0.,0, 0, 0, 0};
    binInfo = context->getBinInfo(binType);
}

size_t Bin::getWidth() const {
    return binInfo.width;
}

size_t Bin::getLength() const {
    return binInfo.length;
}

size_t Bin::getHeight() const {
    return binInfo.height;
}

double Bin::getWeight() const {
    return stats.weightInside;
}

double Bin::getMaximalAllowedWeight() const {
    return binInfo.allowedWeight;
}

size_t Bin::getBinType() const {
    return binType;
}

bool Bin::ifCanPut(
        const Stack& stack, size_t xWidthShift, size_t yLengthShift, bool overlapCheck) const {
    // Check putting inside bin.
    if (xWidthShift > getWidth() || yLengthShift > getLength()) {
        return false;
    }
    // C4
    if (xWidthShift + stack.getWidth() > getWidth()
            || yLengthShift + stack.getLength() > getLength()) {
        return false;
    }
    // C9
    if (stack.getHeight() > getHeight()) {
        return false;
    }
    // C2
    if (getWeight() + stack.getWeight() > getMaximalAllowedWeight()) {
        return false;
    }
    // C3
    if (overlapCheck) {
        for (const auto& fixed : loadedStacks) {
            if (ifStackPlacementsOverlap(stack, xWidthShift, yLengthShift, fixed)) {
                return false;
            }
        }
    }
    return true;
}

void Bin::addStackUpdate(const Stack& stack) {
    stats.weightInside  += stack.getWeight();
    stats.stackVolume += stack.getVolume();
    stats.coveredArea += stack.getBoxArea();
    stats.itemCount += stack.getItemCount();
    stats.itemVolume += stack.getUsefulVolume();
}

void Bin::carry(const Stack& stack, size_t xPosition, size_t yPosition) {
    addStackUpdate(stack);
    loadedStacks.push_back(LoadedStack({stack, xPosition, yPosition}));
}

size_t Bin::getStackCount() const {
    return loadedStacks.size();
}

const std::vector<LoadedStack>& Bin::getStackPlacements() const {
    return loadedStacks;
}

const Stack& Bin::getStackById(size_t stackId) const {
    return loadedStacks[stackId].stack;
}

void Bin::removeStackUpdate(const Stack& stack) {
    stats.weightInside -= stack.getWeight();
    stats.stackVolume -= stack.getVolume();
    stats.coveredArea -= stack.getBoxArea();
    stats.itemCount -= stack.getItemCount();
    stats.itemVolume -= stack.getUsefulVolume();
}

void Bin::removeStack(size_t stackId) {
    auto& stack = loadedStacks[stackId].stack;
    removeStackUpdate(stack);
    loadedStacks.erase(loadedStacks.begin() + stackId);
}

bool Bin::ifCanPutLayerToStack(
        size_t stackId,
        const Layer& layer,
        bool* stackFinalized) const {
    // C8
    if (loadedStacks[stackId].stack.getHeight() + layer.getHeight() > getHeight()) {
        return false;
    }
    // C2
    if (getWeight() + layer.getWeight() > getMaximalAllowedWeight()) {
        return false;
    }
    return loadedStacks[stackId].stack.ifCanPut(layer, stackFinalized);
}

const Layer& Bin::getLayerFromStack(size_t stackId, size_t layerId) const {
    return loadedStacks[stackId].stack.getLayerById(layerId);
}


void Bin::loadLayerToStack(size_t stackId, const Layer& layer) {
    auto& stack = loadedStacks[stackId].stack;
    removeStackUpdate(stack);
    stack.carry(layer);
    addStackUpdate(stack);
}

void Bin::removeLayerFromStack(size_t stackId, size_t layerId) {
    auto& stack = loadedStacks[stackId].stack;
    removeStackUpdate(stack);
    loadedStacks[stackId].stack.removeLayer(layerId);
    addStackUpdate(stack);
}


size_t Bin::getUsedArea() const {
    return stats.coveredArea;
}

size_t Bin::getStackVolume() const {
    return stats.stackVolume;
}

size_t Bin::getUsedVolume() const {
    return stats.itemVolume;
}

void Bin::reorderAllLayers() {
    for (auto& placement : loadedStacks) {
        placement.stack.reorderLayers();
    }
}

size_t Bin::getItemCount() const {
    return stats.itemCount;
}

std::unordered_map<std::string, size_t> Bin::getProductCount() const {
    std::unordered_map<std::string, size_t> productToCount;
    for (const auto& stackPlacement : loadedStacks) {
        auto innerMap = stackPlacement.stack.getProductCount();
        for (auto& valuePair : innerMap) {
            productToCount[valuePair.first] += valuePair.second;
        }
    }
    return productToCount;
}

bool Bin::ifCanBeZero() const {
    std::unordered_map<std::string, size_t> productToCount = getProductCount();
    for (const auto& valuePair : productToCount) {
        const auto& productId = valuePair.first;
        size_t productCountInBin = valuePair.second;

        size_t totalProductCount = context->getProductCount(productId);
        const double EPS = 1e-7;
        size_t maxAllowed = EPS +
            context->getConstraints().tolerance_item_product_demands * totalProductCount;

        if (productCountInBin > maxAllowed) {
            return false;
        }
    }
    return true;
}

std::tuple<size_t, size_t> Bin::getCoordinatesById(size_t stackId) const {
    return std::tie(loadedStacks[stackId].widthShift, loadedStacks[stackId].lengthShift);
}


} // namespace esicup
