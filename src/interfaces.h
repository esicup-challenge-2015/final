/**
 * @file interfaces.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines commonly used data structures.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 * 
 */
#pragma once
#include <vector>
#include <string>
#include <memory>
#include <algorithm>

#include "common.h"
#include "process/filler2d/options.h"
#include "process/layer_strategy/options.h"

namespace esicup {

/**
 * @brief Summarizes the set of directories with different 
 * instances, path to root directory and CLI arguments.
 */
struct InstancesData {
public:
    /// @brief Vector with inner directory names.
    std::vector<std::string> innerDirectories;
    /// @brief Path to the root directory.
    std::string pathToSet;
    /// @brief Whether CLI arguments were read successfully.
    bool initOk;
    /// @brief Whether acceleration was requested.
    bool accelerate;
};

/**
 * @brief Metadata required for reader to read test instance.
 */
struct TestMetadata {
public:
    /// @brief Absolute path to the test instance.
    std::string pathToTest;
    /// @brief Whether acceleration is requested.
    bool accelerate;
};

/**
 * @brief Test input data. 
 */
struct TestData {
public:
    /// @brief Context with instance-specific input information.
    ConstContextPtr context;
};

/**
 * @brief Group contains several similar items.
 */
struct Group {
public:
    /**
     * @brief Group constructor.
     * 
     * @param itemType Index of the item type.
     * @param count Number of items in the group.
     */
    Group(size_t itemType, size_t count)
        : itemType(itemType)
        , count(count)  {}

public:
    /// @brief The index of the item type.
    size_t itemType;
    /// @brief Amount of items in the group.
    size_t count;
};

/**
 * @brief Group order is a vector of groups 
 * with some description.
 */
struct GroupOrder {
public:
    /**
     * @brief Default constructor.
     */
    GroupOrder() {}
    /**
     * @brief Constructs Group order.
     * 
     * @param groupsVector The groups of items, usually waiting for packing.
     * @param description Description of the group order.
     */
    GroupOrder(const std::vector<Group>& groupsVector, const std::string& description)
        : groups(groupsVector)
        , history(description) {}
    
public:
    /// @brief The groups of similar items, usually waiting for packing.
    std::vector<Group> groups;
    /// @brief Description of the group order.
    std::string history; 
};

/**
 * @brief Data in the input format for processing stage.
 */
struct SolutionInputData {
public:
    /// @brief Group orders on the items to pack.
    std::vector<GroupOrder> orders;
    /// @brief Context with instance-specific input information.
    ConstContextPtr context;
};

/**
 * @brief Contains bin with information on filler type and 
 * layer strategy type applied to fill the bin.
 */
struct BinWithHistory {
public:
    /// @brief Bin, filled with packages.
    Bin bin;
    /// @brief Configuration of the filler that created the bin.
    std::pair<FillerType, LayerStrategyType> history;
};

/**
 * @brief Solution Instance represents a sequence of packed
 * bins and API on top of the sequence.
 */
struct SolutionInstance {
public:
    /**
     * @brief Retrieves the bins sequence from the solution.
     * 
     * @return Sequence with filled bins.
     */
    std::vector<Bin> getBins() const {
        std::vector<Bin> result;
        for (auto& binWithStory : bins) {
            result.push_back(binWithStory.bin);
        }
        return result;
    }
    /**
     * @brief Computes the sum of bin volumes.
     * 
     * @return The sum of bin volumes.
     */
    size_t getSumBinVolume() const {
        size_t answer = 0;
        for (auto& binWithStory : bins) {
            answer += binWithStory.bin.getVolume();
        }
        return answer;
    }
    /**
     * @brief Sorts bins by stack volume inside the bins.
     */
    void sortBinsByStackVolume() {
        std::sort(bins.begin(), bins.end(),
            [](const BinWithHistory& lhs, const BinWithHistory& rhs) -> bool {
                return lhs.bin.getStackVolume() < rhs.bin.getStackVolume();
            });
    }
    /**
     * @brief Verifies if the bins are sorted by stack volume inside the bin.
     * 
     * @return True if sorted, False otherwise.
     */
    bool isSorted() const {
        return std::is_sorted(bins.begin(), bins.end(),
                [](const BinWithHistory& lhs, const BinWithHistory& rhs) -> bool {
                    return lhs.bin.getStackVolume() < rhs.bin.getStackVolume();
                });
    }

public:
    /// @brief The sequence with packed bins.
    std::vector<BinWithHistory> bins;
};

/**
 * @brief Solution output from the processing stage.
 * Essentially, a vector with solutions that need
 * postprocessing (designed to select the most effective).
 */
struct SolutionOutputData {
public:
    /// @brief Vector with packings to postprocess/select from.
    std::vector<SolutionInstance> solutions;
};

/**
 * @brief Solution result in the format for export to files.
 */
struct TestOutputData {
public:
    /// @brief Structure with path to the instance.
    TestMetadata testMetadata;
    /// @brief The solution packing to export.
    SolutionInstance solution;
    /// @brief Context with instance-specific input information.
    ConstContextPtr context;
};

} // namespace esicup

