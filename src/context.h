/**
 * @file context.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines structures and API to store and process instance input information.
 * Context stores the instance description, while ItemIdManager allows to gradually
 * retrieve item ids.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024 
 */
#pragma once

#include <vector>
#include <unordered_map>
#include <cmath>
#include <memory>
#include <string>

namespace esicup {

/**
 * @brief Enumerates through possible 
 * materials the package carries.
 */
enum class Material {
    METAL,      ///< Metal items.
    CARDBOARD,  ///< Cardboard items.
    PLASTIC,    ///< Plastic items.
    WOOD        ///< Wooden items.
};

/**
 * @brief Enumerates through positions. Position may
 * describe the range of positions originally allowed
 * (widthwise, lengthwise, both), or the position in
 * the solution allocation (only widthwise or lengthwise).
 */
enum class Position {
    WIDTHWISE,  ///< Widthwise placement.
    LENGTHWISE, ///< Lengthwise placement.
    BOTH        ///< Either widthwise or lengthwise placement.
};

/**
 * @brief Describes item (another term package). The
 * items/packages are supposed to get packed/loaded 
 * into bins/trucks.
 */
struct ItemInfo {
public:  
    /**
     * @brief Built-in method to compute item/package volume.
     * 
     * @return The volume of the item/package.
     */
    unsigned long long getVolume() const;
    /**
     * @brief Built-in method to compare item descriptions.
     * 
     * @param other The item description to compare with.
     * 
     * @return True if descriptions are the same, and False otherwise.
     */
    bool operator == (const ItemInfo& other) const;

public:
    /// @brief The material the package carries.
    Material material;
    /// @brief The allowed package position.
    Position position;
    /// @brief The width of the item/package.
    size_t width;
    /// @brief The length of the item/package.
    size_t length;
    /// @brief The height of the item/package.
    size_t height;
    /// @brief The weight of the item/package.
    double weight;
    /// @brief The name/identifier of the product in the item/package.
    std::string productName;
};


/**
 * @brief Hash function for using ItemInfo in unordered maps or sets.
 */
class ItemInfoHash {
public:
    /**
     * @brief The hash function for ItemInfo (description for item/package). 
     * Provides built-in hash value of the product name/type.
     * 
     * @param info The object to compute hash from.
     * @return The hash value.
     */
    std::size_t operator()(const ItemInfo& info) const {
        return std::hash<std::string>()(info.productName);
    }
};

/**
 * @brief Describes bin (another term - truck).
 * Bins correspond to the types of trucks that may be
 * applied to carry the packages.
 */
struct BinInfo {
public:
    /**
     * @brief Built-in method to compute the volume 
     * of the bin/truck/container.
     * 
     * @return The volume of the bin/truck/container.
     */
    unsigned long long getVolume() const;

public:
    /// @brief The width of the bin/container/truck.
    size_t width;
    /// @brief The length of the bin/container/truck.
    size_t length;
    /// @brief The height of the bin/container/truck.
    size_t height;
    /// @brief The weight the bin/container/truck can carry.
    double allowedWeight;
    /// @brief The name of the bin/container/truck.
    std::string binName;

};

/**
 * @brief Entity representing a group of similar items/packages.
 * Describes the nature of the item/package, and the ids of the
 * corresponding individual items/packages.
 */
struct ItemInfoWrapper {
    /// @brief Describes the parameters of the items/packages.
    ItemInfo info;
    /// @brief Vector with individual identifiers for the items/packages.
    std::vector<int> inputIDs;
};

/**
 * @brief Stores the instance-specific parameters and requirements.
 */
struct Constraints {
    /// @brief The tolerance to item-product demands.
    double tolerance_item_product_demands;
    /// @brief The maximal allowed density in kilogramms.
    double maximum_density_allowed_kg;
    /// @brief The maximal allowed number of rows in a layer (for non-metal items).
    size_t max_number_of_rows_in_a_layer;
    /// @brief The maximal allowed number of items in a row.
    size_t max_number_of_items_in_a_row;
    /// @brief The allowed tolerance for deviation in layer size.
    double layer_size_deviation_tolerance;
    /// @brief The allowed tolerance for deviation in row size.
    double row_size_deviation_tolerance;
    /// @brief The allowed tolerance in deviation for item size.
    double item_size_deviation_tolerance;
    /// @brief The maximal allowed weight above item in kg (for non-metal items).
    double maximum_weigth_above_item_kg;
};

/**
 * @brief Context entity defined in what follows is a description of an instance. Stores 
 * instance-specific requirements, available types of trucks/bins and the set of items
 * to pack/load inside. Also provides API that can report summarizing information. 
 */
class Context {
public:
    /**
     * @brief Retrieves bin/truck description by the bin/truck type index.
     * 
     * @param binTypeIndex The index of the bin type.
     * @return Constant reference to the corresponding bin/truck description.
     */
    const BinInfo& getBinInfo(size_t binTypeIndex) const;
    /**
     * @brief Retrieves the description of the item/package by the item/package type index.
     * 
     * @param itemTypeIndex The index of the item/package type.
     * @return Constant reference to the corresponding item/truck description.
     */
    const ItemInfo& getItemInfo(size_t itemTypeIndex) const;
    /**
     * @brief Provides instance-specific parameters, constraints and requirements.
     * 
     * @return Constant reference to the corresponding data structure.
     */
    const Constraints& getConstraints() const;

    /**
     * @brief Provides the amount of bin/truck types.
     * 
     * @return The number of bin/truck types.
     */
    size_t getBinTypesCount() const;
    /**
     * @brief Provides the amount of item/package types.
     * 
     * @return The number of item/package types.
     */
    size_t getItemTypesCount() const;

    /**
     * @brief Counts the total amount of items/packages of 
     * particular type for packing/loading.
     * 
     * @param itemType The type of the item/packages.
     * @return The number of item/packages of particular type.
     */
    size_t getItemCount(size_t itemType) const;
    /**
     * @brief Counts the number of items/packages with particular
     * product type.
     * 
     * @param productName The name of the product type. 
     * @return The corresponding number of items/packages.
     */
    size_t getProductCount(const std::string& productName) const;

    /**
     * @brief Adds item/package to the instance description.
     * 
     * @param info The information about the item/package.
     * @param itemId The individual index of the item/package.
     */
    void addItem(ItemInfo info, size_t itemId);
    /**
     * @brief Adds bin/truck type to the instance description.
     * 
     * @param info The information about the bin/truck.
     */
    void addBin(BinInfo info);
    /**
     * @brief Adds instance requirements/constraints to instance description.
     * 
     * @param requirements The instance-specific requirements/constraints.
     */
    void setConstraints(Constraints requirements);
    /**
     * @brief Retrieves the vector with all registered items. Each element
     * stores all items of a particular type, to get the corresponding ids,
     * one is supposed to look at entry.inputIDs.
     * 
     * @return The vector with registered items.
     */
    const std::vector<ItemInfoWrapper>& getAllItems() const;

    /**
     * @brief Retrieves the minimal bin volume among available bin types.
     * 
     * @return The described minimal bin volume.
     */
    unsigned long long getMinBinVolume() const;
    /**
     * @brief Retrieves the maximal allowed weight of the items inside a bin.
     * 
     * @return The maximal allowed bin weight among all possible bin types.
     */
    double getMaxAllowedBinWeight() const;
    /**
     * @brief Whether the accelerated launch is performed.
     * 
     * @param acceleration The status of accelerated launch.
     */
    void setAcceleration(bool acceleration) {
        acceleratedScenario = acceleration;
    }
    /**
     * @brief Requests whether the scenario is accelerated.
     * 
     * @return True if accelerated launch is performed, and False otherwise.
     */
    bool isAccelerated() const {
        return acceleratedScenario;
    }

private:
    /**
     * @brief ContextStats is an inner structure to store summary
     * about the bins and items available in the instance.
     */
    struct ContextStats {
    public:
        /// @brief The maximal allowed weight of items inside bin among all bin types.
        double maxAllowedBinWeight;
        /// @brief The minimal allowed bin volume among all bin types.
        unsigned long long minBinVolume;
    };

private:
    /// @brief Instance of ContextStats to store summary about bins and items in the instance.
    ContextStats stats;

    /// @brief The types of bins/trucks available in the instance.
    std::vector<BinInfo> instanceBins;
    /// @brief The types of items/packages available in the instance.
    std::vector<ItemInfoWrapper> instanceItems;

    /// @brief Dictionary from item/package description to item/package type.
    std::unordered_map<ItemInfo, size_t, ItemInfoHash> itemInfoToType;
    /// @brief Dictionary from product name to the number of packages with this product.
    std::unordered_map<std::string, size_t> productCount;

    /// @brief The structure with instance-specific parameters and requirements.
    Constraints constraints;
    /// @brief Whether launch is according to accelerated scenario.
    bool acceleratedScenario;
};

/// @brief Pointer to a Context (Instance Description) object.
typedef std::shared_ptr<Context> ContextPtr;
/// @brief Constant pointer to a Context (Instance Description) object.
typedef std::shared_ptr<const Context> ConstContextPtr;


/**
 * @brief ItemIdManager is an entity to retrieve item ids one-by-one 
 * for a particular item types and update corresponding data structures.
 */
class ItemIdManager {
public:
    /**
     * @brief Default constructor for ItemIdManager.
     */
    ItemIdManager() {}
    /**
     * @brief Constructst ItemIdManager from instance description.
     * 
     * @param context Instance description.
     */
    ItemIdManager(ConstContextPtr context);
    /**
     * @brief Sets the instance-related items (in group format).
     * 
     * @param context Instance description.
     */
    void setInstanceItems(ConstContextPtr context);
    /**
     * @brief Returns an available Id for a particular item type.
     * Provides assertion of there are no more items of the 
     * requested item type.
     * 
     * @param itemType The type of the item.
     * 
     * @return The index of the available id.
     */
    size_t getAvailableId(size_t itemType);

private:
    /// @brief The vector with item groups, each describing item 
    /// parameters and ids of the coresponding items.
    std::vector<ItemInfoWrapper> instanceItems;
};


} // namespace esicup
