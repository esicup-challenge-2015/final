/**
 * @file common.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Introduces basic structures and corresponding API to store packing.
 * Layer, Stack, Bin and positioned versions LiftedLayer and LoadedStack.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include <vector>
#include <string>
#include <stdexcept>
#include <memory>
#include <unordered_map>
#include <tuple>

#include "context.h"

namespace esicup {

/**
 * @brief Interface specifying API for a box - something 
 * having width, height, length, volume, weight, area 
 * and item count.
 */
class IBox {
public:
    /**
     * @brief Constructor for a Box. 
     * 
     * @param contextData  - Pointer to the instance description.
     */
    IBox(ConstContextPtr contextData) : context(contextData) {};
    /**
     * @brief Method to retrieve the width of the box.
     * 
     * @return The width of the box.
     */
    virtual size_t getWidth() const = 0;
    /**
     * @brief Method to retrieve the length of the box.
     * 
     * @return The length of the box.
     */
    virtual size_t getLength() const = 0;
    /**
     * @brief Method to retrieve the height of the box.
     * 
     * @return The height of the box.
     */
    virtual size_t getHeight() const = 0;
    /**
     * @brief Method to compute the volume of the box.
     * 
     * @return The volume of the box.
     */
    unsigned long long getVolume() const {
        return static_cast<unsigned long long>(getWidth()) *
            static_cast<unsigned long long>(getLength()) *
            static_cast<unsigned long long>(getHeight());
    }
    /**
     * @brief Method to retrieve the weight of the items inside the box.
     * 
     * @return The weight of the all items inside the box.
     */
    virtual double getWeight() const = 0;
    /**
     * @brief Method to retrieve the number of items inside the box.
     * 
     * @return The number of items inside the box.
     */
    virtual size_t getItemCount() const = 0;
    /**
     * @brief Method to compute the area of the box.
     * 
     * @return The area of the box.
     */
    unsigned long long getBoxArea() const {
        return static_cast<unsigned long long>(getLength())
            * static_cast<unsigned long long>(getWidth());
    };

protected:
    /**
     * @brief Pointer to the structure describing the instance.
     */
    ConstContextPtr context;
};

/**
 * @brief Layer represents a rectangle of items packed at the same level.
 * The rectangle has shape widthInItems x lengthInItems.
 */
class Layer final : public IBox {
public:
    /**
     * @brief Layer constructor.
     * 
     * @param widthInItems The width of the layer (in items).
     * @param lengthInItems The length of the layer (in items).
     * @param itemType The type index of each item.
     * @param itemPosition Position of each item (lengthwise or widthwise).
     * @param context The context describing instance-specific input.
     * @param allowFlip Whether flipping items in the layer is allowed.
     */
    Layer(size_t widthInItems,
          size_t lengthInItems,
          size_t itemType,
          Position itemPosition,
          const ConstContextPtr& context,
          bool allowFlip = true);

    /**
     * @brief Verifies whether the layer of the shape widthInItems x lengthInItems
     * with items of itemType specifically positioned can be created according to
     * problem requirements.
     * 
     * @param widthInItems  The potential layer width in items.
     * @param lengthInItems The potential layer length in items.
     * @param itemType      The potential type of the item in the layer.
     * @param position      The potential positioning for each item in the layer.
     * @param context       The context describing instance-specific input.
     * 
     * @return True when the constraints are satisfied, and False otherwise. 
     */
    static bool ifCanCreate(size_t widthInItems,
            size_t lengthInItems,
            size_t itemType,
            Position position,
            const ConstContextPtr& context);

    /**
     * @brief Verifies if the layer can be flipped.  Flip means 
     * swap(widthInItems, lengthInItems) and rotation in position.
     * 
     * @return True if the flipped layer satisfies requirements, and False otherwise.
     */
    bool ifCanFlip() const;
    /**
     * @brief Turns the layer orthogonally. More precisely, swaps
     * widthInItems and lengthInItems. Afterwards, rotates the position.
     */
    void flip();

    /**
     * @brief Builds layers similar to the layer provided as an argument. 
     * Returns vector with two or one item. One of them has the same structre
     * as the argument layer, the second is the flipped argument in case
     * flipping is feasible.
     * 
     * @param layer The layer to start with.
     * @return Vector of layers similar to current.
     */
    static std::vector<Layer> buildSimilar(Layer layer);

    /**
     * @brief Retrieves the layer width in items.
     * 
     * @return The layer width in items.
     */
    size_t getWidthInItems() const;
    /**
     * @brief Retrieves the layer length in items.
     * 
     * @return The layer length in items.
     */
    size_t getLengthInItems() const;
    /**
     * @brief Checks whether the layer items are metal.
     * 
     * @return True if the layer items are from 
     * metal, and False otherwise.
     */
    bool isMetal() const;

    /**
     * @brief Retrieves the number of items in the layer.
     * 
     * @return The number of items in the layer.
     */
    virtual size_t getItemCount() const override;
    /**
     * @brief Computes layer width.
     * 
     * @return The width of the layer.
     */
    virtual size_t getWidth() const override;
    /**
     * @brief Computes layer length.
     * 
     * @return The length of the layer.
     */
    virtual size_t getLength() const override;
    /**
     * @brief Computes layer height.
     * 
     * @return The height of the layer.
     */
    virtual size_t getHeight() const override;
    /**
     * @brief Computes layer weigth.
     * 
     * @return The weight of the layer.
     */
    virtual double getWeight() const override;

    /**
     * @brief Retrieves the item type.
     * 
     * @return The type index of the item.
     */
    size_t getItemType() const;
    /**
     * @brief Retrieves the position of the items inside.
     * 
     * @return The position of the items inside.
     */
    Position getItemPosition() const;

private:
    /// @brief The width of the layer in items (number of items in a row).
    size_t widthInItems;
    /// @brief The length of the layer in items (number of items in a column).
    size_t lengthInItems;
    /// @brief The type index of the item.
    size_t itemType;
    /// @brief Information about the item parameters.
    ItemInfo itemInfo;
    /// @brief Position is the way the item is placed. Should be 
    /// either Position::LENGTHWISE or Position::WIDTHWISE.
    Position itemPosition;
    /// @brief Whether flipping the item is allowed. Flipping stands for 
    /// swap(widthInItems, lengthInItems) and corresponding update in position.
    bool allowFlip;
};


/**
 * @brief Stores layer together with the height of the layer origin 
 * in the corresponding stack.
 */
struct LiftedLayer {
public:
    /// @brief The structure of the layer.
    Layer layer;
    /// @brief The height in the corresponding stack.
    size_t heightShift;
};

/**
 * @brief Stack is a sequence of layers placed vertically.
 */
class Stack final: public IBox {
public:
    /**
     * @brief Stack constructor.
     * 
     * @param context Context describing instance input information.
     * @param allowFlip Whether flipping stack is allowed.
     */
    Stack(ConstContextPtr context, bool allowFlip = true);

    /**
     * @brief Builds stacks, similar to the provided at an argument.
     * There are at most two similar options: stack equal to the 
     * referenced, and a flipped stack if available and satisfies requirements.
     * 
     * @param stack The stack to use at a reference.
     * 
     * @return Vector with similar to the referenced stacks.
     */
    static std::vector<Stack> buildSimilar(Stack stack);

    /**
     * @brief Verifies if the layer can be added to the stack.
     * 
     * @param layer The layer candidate to get loaded.
     * @param stackFinalized Whether the stack should be finalized.
     * 
     * @return True if there is a feasible way to add 
     *      the layer to the stack, and False otherwise.
     */
    bool ifCanPut(const Layer& layer, bool* stackFinalized) const;
    /**
     * @brief Verifies if the stack consists of metal items.
     * 
     * @return True if stack consists of metal items, and False otherwise.
     */
    bool isMetal() const;

    /**
     * @brief Counts the number of items in the stack.
     * 
     * @return The number of items in the stack.
     */
    virtual size_t getItemCount() const override;
    /**
     * @brief Retrieves the width of the stack.
     * 
     * @return The width of the stack.
     */
    virtual size_t getWidth() const override;
    /**
     * @brief Retrieves the length of the stack.
     * 
     * @return The length of the stack.
     */
    virtual size_t getLength() const override;
    /**
     * @brief Retrieves the height of the stack.
     * 
     * @return The height of the stack.
     */
    virtual size_t getHeight() const override;
    /**
     * @brief Retrieves the weight of the stack.
     * 
     * @return The weight of the stack.
     */
    virtual double getWeight() const override;

    /**
     * @brief Loads the layer to the stack.
     * 
     * @param layer The layer to load in the stack.
     */
    void carry(const Layer& layer);
    /**
     * @brief Reorders layers by their weight, to the form they
     * should be provided in the output.
     */
    void reorderLayers();

    /**
     * @brief Retrieves the sequence of currently carried layers 
     * in the order they are currently packed in stack.
     * 
     * @return The vector of with layers in stack.
     */
    std::vector<Layer> getCurrentOrder() const;
    /**
     * @brief Retrieves the sequence of layer placements for 
     * layers that are currently carried in stack.
     * 
     * @return The vector with layer placements inside stack.
     */
    std::vector<LiftedLayer> getLayerPlacements() const;

    /**
     * @brief Removes layer by index.
     * 
     * @param layerIndex The index of layer to remove.
     */
    void removeLayer(size_t layerIndex);
    /**
     * @brief Retrieves layer by identifier.
     * 
     * @param layerIndex The index of the layer.
     * 
     * @return Constant reference to the indexed layer.
     */
    const Layer& getLayerById(size_t layerIndex) const;

    /**
     * @brief Computes the ground area of the stack.
     * 
     * @return The ground area that the stack covers.
     */
    size_t getGroundArea() const;
    /**
     * @brief Counts the number of layers in the stack.
     * 
     * @return The number of layers in the stack.
     */
    size_t getLayerCount() const {
        return liftedLayers.size();
    }

    /**
     * @brief Computes the volume used by stack items inside.
     * 
     * @return The volume used by stack items inside.
     */
    size_t getUsefulVolume() const;

    /**
     * @brief Prepares a dictionary mapping from product name/id 
     * to the amount of items containing the product.
     * 
     * @return The dictionary from product id to amount of items with product.
     */
    std::unordered_map<std::string, size_t> getProductCount() const;

private:
    /**
     * @brief Updates statistics when layer is removed by index.
     * 
     * @param layerId The indexo f the removed layer.
     */
    void layerRemoveUpdate(size_t layerId);
    /**
     * @brief Updates statistics when layer is added to stack.
     * 
     * @param layer The added layer.
     */
    void layerPutUpdate(const Layer& layer);

private:
    /**
     * @brief Running statistics in stack.
     */
    struct StackStats {
        /// @brief The sum volume of all items in stacks.
        size_t usefulVolume;
        /// @brief The stack height.
        size_t height;
        /// @brief The stack length.
        size_t length;
        /// @brief The stack width.
        size_t width;
        /// @brief The weight of all items in stack.
        double weight;
        /// @brief The number of items in stack.
        size_t itemCount;
    };

private:
    /// @brief Whether flipping all layers is an option.
    bool allowFlip;
    /// @brief Running stack-related statistics.
    StackStats stats;
    /// @brief Vector storing the layers inside the stack.
    std::vector<LiftedLayer> liftedLayers;
};

/**
 * @brief Stores stack together with positional 
 * information inside the bin where packed.
 */
struct LoadedStack {
public:
    /// @brief The stack entity (sequence of layers).
    Stack stack;
    /// @brief The coordinate of origin shift along x-axis.
    size_t widthShift;
    /// @brief The coordinate of origin shift along y-axis.
    size_t lengthShift;
};

/**
 * @brief Bin is something where containers are packed and 
 * corresponds to trucks in the original problem statements.
 * The packing is stored as a vector of carried stacks.
 */
class Bin final: public IBox {
public:
    /**
     * @brief Bin constructor.
     * 
     * @param binType  The type of the truck/bin to pack items into.
     * @param context  The description of the instance input.
     */
    Bin (size_t binType, ConstContextPtr context);

    /**
     * @brief Counts the number of items/packages inside.
     * 
     * @return The number of items/packages inside the bin.
     */
    virtual size_t getItemCount() const override;
    /**
     * @brief Retrieves the width of the bin (truck/container).
     * 
     * @return The width of the bin (truck/container).
     */
    virtual size_t getWidth() const override final;
    /**
     * @brief Retrieves the length of the bin (truck/container).
     * 
     * @return The length of the bin (truck/container). 
     */
    virtual size_t getLength() const override final;
    /**
     * @brief Retrieves the height of the bin (truck/container).
     * 
     * @return The height of the bin (truck/container).
     */
    virtual size_t getHeight() const override final;
    /**
     * @brief Retrieves the weight of all items inside the bin (truck/container).
     * 
     * @return The weight of all items inside the bin (truck/container).
     */
    virtual double getWeight() const override final; 
    /**
     * @brief Retrieves the maximal allowed weight inside the bin (truck/container).
     * 
     * @return The maximal allowed weight inside the bin (truck/container).
     */
    double getMaximalAllowedWeight() const;

    /**
     * @brief Retrieves the type of the bin (truck/container).
     * 
     * @return The type index of the bin (truck/container).
     */
    size_t getBinType() const;

    /**
     * @brief Verifies if a new stack can be brought to the specified position inside the bin.
     * 
     * @param stack The stack candidate for adding to the bin (container).
     * @param widthShift The shift along x-axis for the potential stack origin.
     * @param lengthShift  The shift along y-axis for the potential stack origin.
     * @param overlapCheck Whether to check if the stack candidate would overlap 
     *      some existing stacks inside the bin (truck/container).
     * @return True if no problems are found, and False otherwise.
     */
    bool ifCanPut(const Stack& stack, size_t widthShift, 
            size_t lengthShift, bool overlapCheck = true) const;
    /**
     * @brief Loads the stack to the specified position inside the bin/container.
     * 
     * @param stack The stack to carry on the container/truck/bin.
     * @param widthShift The shift along x-axis to the origin of the stack position.
     * @param lengthShift The shift along y-axis to the origin of the stack position.
     */
    void carry(const Stack& stack, size_t widthShift, size_t lengthShift);
    /**
     * @brief Counts the number of stacks inside the container/bin/truck.
     * 
     * @return The number of stacks packed inside the container/bin/truck.
     */
    size_t getStackCount() const;
    /**
     * @brief Retrieves the vector with stacks alongside with their placements.
     * 
     * @return The vector with a structure including stacks and their placements.
     */
    const std::vector<LoadedStack>& getStackPlacements() const;

    /**
     * @brief Retrieves stack from inner data structure by index.
     * 
     * @param stackId The index of the stack.
     * @return Constant reference to the requested stack.
     */
    const Stack& getStackById(size_t stackId) const;
    /**
     * @brief Retrieves stack coordinates from inner data structure by index.
     * 
     * @param stackId The index of the stack.
     * @return Tuple with two components: (x-coordnate, y-coordinate).
     */
    std::tuple<size_t, size_t> getCoordinatesById(size_t stackId) const;
    /**
     * @brief Removes stack from the bin.
     * 
     * @param The index of stack to remove.
     */
    void removeStack(size_t stackId);

    /**
     * @brief Checks if after bringing additional layer to a particular 
     * state the packing would remain feasible.
     * 
     * @param stackId The index of stack to improve.
     * @param layer The layer to bring on top of the stack.
     * @param stackFinalized Flag to bring through for tracking if stack is finalized.
     * @return True if the result would remain feasible, and False otherwise.
     */
    bool ifCanPutLayerToStack(
            size_t stackId,
            const Layer& layer,
            bool* stackFinalized) const;

    /**
     * @brief Loads layer on top of a positioned stack inside the bin.
     * 
     * @param stackId The index of stack to update.
     * @param layer The layer to bring on top of the stack.
     */
    void loadLayerToStack(size_t stackId, const Layer& layer);
    /**
     * @brief Retrieve the layer from stack inside the bin by stack-layer indices.
     * 
     * @param stackId The index of stack.
     * @param layerId The index of layer.
     * 
     * @return The constant reference to the corresponding layer.
     */
    const Layer& getLayerFromStack(size_t stackId, size_t layerId) const;
    /**
     * @brief Removes layer from stack inside the bin by stack-layer indices.
     * 
     * @param stackId The index of stack.
     * @param layerId The index of layer.
     */
    void removeLayerFromStack(size_t stackId, size_t layerId);
    /**
     * @brief Reorders layers in each particular stack.
     * 
     */
    void reorderAllLayers();

    /**
     * @brief Counts the number of packages with particular product for 
     * each product type, and returns the result in the form of the 
     * corresponding dictionary.
     * 
     * @return Dictionary from product type identifier to the number 
     * of products of each particular type.
     */
    std::unordered_map<std::string, size_t> getProductCount() const;
    /**
     * @brief Checks if this bin fits to the role of zero bin according 
     * to the product-demand tolerance.
     * 
     * @return True if requirements are satisfied, and False otherwise.
     */
    bool ifCanBeZero() const;

    /**
     * @brief Retrieves the used area by stacks inside the bin.
     * 
     * @return The value of the used area.
     */
    size_t getUsedArea() const;
    /**
     * @brief Retrieves the used volume by stacks inside the bin.
     * 
     * @return The value of the stack volume inside the bin.
     */
    size_t getStackVolume() const;
    /**
     * @brief Retrieves the effective volume used by packages inside the bin.
     * 
     * @return The value of the volume sum of all items inside the bin.
     */
    size_t getUsedVolume() const;

private:
    /**
     * @brief Updates statistics according to removing a particular stack.
     * 
     * @param stack The recently removed stack.
     */
    void removeStackUpdate(const Stack& stack);
    /**
     * @brief Updates statistics according to a newly carried stack in bin.
     * 
     * @param stack The recently added stack.
     */
    void addStackUpdate(const Stack& stack);

private:
    /**
     * @brief Structure to track the running statistics around 
     * the current packages inside the bin.
     */
    struct BinStats {
    public:
        /// @brief The total weight of all items inside the bin.
        double weightInside;
        /// @brief The total covered area by stacks inside the bin.
        size_t coveredArea;
        /// @brief The total volume of items inside the bin.
        size_t itemVolume;
        /// @brief The total volume of stacks inside the bin.
        size_t stackVolume;
        /// @brief The number of the items inside the bin.
        size_t itemCount;
    };

private:
    /// @brief The running statistics around the current packages inside the bin.
    BinStats stats;
    /// @brief The vector with placed stacks.
    std::vector<LoadedStack> loadedStacks;
    /// @brief Index of the type of the bin/container/truck.
    size_t binType;
    /// @brief The description of the bin/container/truck type.
    BinInfo binInfo;
};

/**
 * @brief Verifies if candidate stack placement overlaps with a 
 * placement of a particular loaded at fixed position stack.
 * 
 * @param candidate The stack candidate for placement.
 * @param widthShift The shift along x-axis to the stack candidate origin.
 * @param lengthShift The shift along y-axis to the stack candidate origin.
 * @param fixed The already loaded stack with fixed position.
 * 
 * @return True if stacks would overlap for such placement, and False otherwise.
 */
bool ifStackPlacementsOverlap(
    const Stack &candidate, size_t widthShift, size_t lengthShift, const LoadedStack &fixed);

} // namespace esicup

