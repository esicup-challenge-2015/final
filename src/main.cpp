/// Main entry point.
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "pipe.h"
#include "schedule.h"

#include <iostream>


/**
 * @brief Main entry point. Parses arguments
 * and starts parallel launch.
 * 
 * @param argc Number of CLI arguments.
 * @param argv Array with CLI arguments.
 * 
 * @return Success code. 
 */
int main(int argc, char** argv) {
    esicup::Parser arguments;
    esicup::InstancesData data = arguments.parse(argc, argv);
    if (!data.initOk) {
        esicup::printUsage(argc, argv);
        return 1;
    }
    parallelLaunch(data);
    return 0;
}

