/// Implementation for stack-related API described in common.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "common.h"

#include <cassert>
#include <iostream>
#include <algorithm>
#include <tuple>
#include <vector>
#include <limits>

namespace esicup {

Stack::Stack(ConstContextPtr context, bool allowFlip)
    : IBox(context)
    , allowFlip(allowFlip) {
    stats = {0, 0, 0, 0, 0, 0};
}

bool Stack::ifCanPut(const Layer& layer, bool* stackFinalized) const {
    if (!layer.getLengthInItems() || !layer.getWidthInItems()) {
        return false;
    }

    if (liftedLayers.empty()) {
        *stackFinalized = false;
        return true;
    }

    // C28
    if (isMetal() && (layer.getLengthInItems() > 1 || layer.getWidthInItems() > 1)) {
        *stackFinalized = false;
        return false;
    }

    // C12
    if ((!isMetal() && layer.isMetal()) || (isMetal() && !layer.isMetal())) {
        *stackFinalized = false;
        return false;
    }


    // Assumption: insertion doesn't extend stack
    if (layer.getWidth() > getWidth() || layer.getLength() > getLength()) {
        *stackFinalized = false;
        return false;
    }

    const Constraints& constraints = context->getConstraints();

    // Check that new layer is the top one if added.
    bool newIsTop = true;
    for (const auto& placement : liftedLayers) {
        if (placement.layer.getWeight() < layer.getWeight()) {
            newIsTop = false;
        }
    }

    double totalWeight = getWeight() + layer.getWeight();

    if (!isMetal()) {
        // Preparation for checking C13 and C29.
        const double EPS = 1e-7;
        size_t groundArea = 0;
        size_t downLayerItemCount = 0;
        double bottomlessStackWeight = totalWeight;
        if (layer.getWeight() > liftedLayers.front().layer.getWeight()) {
            groundArea = layer.getBoxArea();
            bottomlessStackWeight -= layer.getWeight();
            downLayerItemCount = layer.getItemCount();
        } else {
            groundArea = getGroundArea();
            bottomlessStackWeight -= liftedLayers.front().layer.getWeight();
            downLayerItemCount = liftedLayers.front().layer.getItemCount();
        }

        // C13
        if (totalWeight / groundArea + EPS > constraints.maximum_density_allowed_kg) {
            *stackFinalized = false;
            return false;
        }

        // C29
        if (1.0 / downLayerItemCount * bottomlessStackWeight + EPS >
                constraints.maximum_weigth_above_item_kg) {
            *stackFinalized = false;
            return false;
        }
    }

    // C10
    const size_t SIZE_INIT = std::numeric_limits<size_t>::max();
    size_t noTopMinWidth = SIZE_INIT, noTopMinLength = SIZE_INIT, 
            noTopMaxWidth = 0, noTopMaxLength = 0;
    for (size_t idx = 0; idx < liftedLayers.size(); ++idx) {
        bool conditionForNew = (idx + 1 == liftedLayers.size()) && !newIsTop;
        auto& nextLayer = conditionForNew ? layer : liftedLayers[idx].layer;
        noTopMinWidth = std::min(noTopMinWidth, nextLayer.getWidth());
        noTopMinLength = std::min(noTopMinLength, nextLayer.getLength());
        noTopMaxWidth = std::max(noTopMaxWidth, nextLayer.getWidth());
        noTopMaxLength = std::max(noTopMaxLength, nextLayer.getLength());
    }
    const double EPS = 1e-7;
    if (EPS + static_cast<double>(noTopMaxWidth) 
            / static_cast<double>(noTopMinWidth) 
            > 1. + constraints.layer_size_deviation_tolerance
            || EPS + static_cast<double>(noTopMaxLength) 
            / static_cast<double>(noTopMinLength) 
            > 1. + constraints.layer_size_deviation_tolerance) {
        return false;
    }
    auto& top = newIsTop ? layer : liftedLayers.back().layer;
    if (top.getWidth() > noTopMinWidth) {
        const double EPS = 1e-7;
        if (static_cast<double>(top.getWidth()) 
                / static_cast<double>(noTopMinWidth) + EPS 
                > 1. + context->getConstraints().layer_size_deviation_tolerance) {
            return false;
        }
    }

    if (top.getLength() > noTopMinLength) {
        const double EPS = 1e-7;
        if (static_cast<double>(top.getLength()) 
                / static_cast<double>(noTopMinLength) + EPS 
                > 1. + context->getConstraints().layer_size_deviation_tolerance) {
            return false;
        }
    }

    return true;
}

bool Stack::isMetal() const {
    if (liftedLayers.empty()) {
        return false;
    }
    return getLayerById(0).isMetal();
}

size_t Stack::getWidth() const  {
    return stats.width;
}

size_t Stack::getLength() const {
    return stats.length;
}

size_t Stack::getHeight() const {
    return stats.height;
}

double Stack::getWeight() const {
    return stats.weight;
}

void Stack::carry(const Layer& layer) {
    layerPutUpdate(layer);
    liftedLayers.push_back(LiftedLayer({layer, 0}));
    reorderLayers();
}

void Stack::reorderLayers() {
    std::sort(std::begin(liftedLayers), std::end(liftedLayers),
        [](const  LiftedLayer& lhs, const LiftedLayer& rhs)  -> bool {
            return lhs.layer.getWeight() > rhs.layer.getWeight();
    });
    size_t height = 0;
    for (auto& placement : liftedLayers) {
        placement.heightShift = height;
        height += placement.layer.getHeight();
    }
}

std::vector<Layer> Stack::getCurrentOrder() const {
    std::vector<Layer>result;
    for (const auto& entry : liftedLayers) {
        result.push_back(entry.layer);
    }
    return result;
}

std::vector<LiftedLayer> Stack::getLayerPlacements() const {
    return liftedLayers;
}

void Stack::removeLayer(size_t layerIndex) {
    assert(layerIndex < liftedLayers.size());
    layerRemoveUpdate(layerIndex);
    liftedLayers.erase(liftedLayers.begin() + layerIndex);
    reorderLayers();
}

const Layer& Stack::getLayerById(size_t layerIndex) const {
    assert(layerIndex < liftedLayers.size());
    return liftedLayers[layerIndex].layer;
}

size_t Stack::getItemCount() const {
    return stats.itemCount;
}

std::unordered_map<std::string, size_t> Stack::getProductCount() const {
    std::unordered_map<std::string, size_t> productCount;
    for (const auto& placement : liftedLayers) {
        size_t count = placement.layer.getItemCount();
        std::string name = context->getItemInfo(
            placement.layer.getItemType()).productName;
        productCount[name] += count;
    }
    return productCount;
}

size_t Stack::getGroundArea() const {
    if (!liftedLayers.empty()) {
        return liftedLayers.front().layer.getBoxArea();
    }
    return 0;
}

void Stack::layerRemoveUpdate(size_t layerId) {
    stats.usefulVolume -= liftedLayers[layerId].layer.getVolume();
    stats.height -= liftedLayers[layerId].layer.getHeight();
    stats.itemCount -= liftedLayers[layerId].layer.getItemCount();
    stats.weight -= liftedLayers[layerId].layer.getWeight();
    stats.length = stats.width = 0;
    for (size_t index = 0; index < liftedLayers.size(); ++index) {
        if (index != layerId)  {
           const auto& currentLayer = liftedLayers[index].layer;
           stats.length = std::max(stats.length, currentLayer.getLength());
           stats.width = std::max(stats.width, currentLayer.getWidth());
        }
    }
}

void Stack::layerPutUpdate(const Layer& layer) {
    stats.usefulVolume += layer.getVolume();
    stats.height += layer.getHeight();
    stats.weight += layer.getWeight();
    stats.length = std::max(layer.getLength(), stats.length);
    stats.width = std::max(layer.getWidth(), stats.width);
    stats.itemCount += layer.getItemCount();
}


std::vector<Stack> Stack::buildSimilar(Stack stack) {
    std::vector<Stack> result;
    result.push_back(stack);
    bool allFlipped = stack.allowFlip;
    for (auto& placement : stack.liftedLayers) {
        auto& layer = placement.layer;
        if (layer.ifCanFlip()) {
            layer.flip();
        } else {
            allFlipped = false;
            break;
        }
    }
    if (allFlipped) {
        std::swap(stack.stats.length, stack.stats.width);
        result.push_back(stack);
    }
    if (result.front().getLength() > result.back().getLength()) {
        std::swap(result.front(), result.back());
    }
    return result;
}

size_t Stack::getUsefulVolume() const {
    return stats.usefulVolume;
}

} // namespace esicup
