/// Implementation for layer-related API described in common.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "common.h"

#include <iostream>
#include <cassert>
#include <vector>
#include <algorithm>


namespace esicup {

Layer::Layer(size_t widthInItems,
             size_t lengthInItems,
             size_t itemType,
             Position itemPosition,
             const ConstContextPtr& context,
             bool allowFlip)
    : IBox(context)
    , widthInItems(widthInItems)
    , lengthInItems(lengthInItems)
    , itemType(itemType)
    , itemPosition(itemPosition)
    , allowFlip(allowFlip) {
        assert(itemPosition != Position::BOTH);
        itemInfo = context->getItemInfo(itemType);
}

bool Layer::ifCanFlip() const {
    assert(itemPosition != Position::BOTH);
    Position nextPosition = itemPosition == Position::LENGTHWISE ?
        Position::WIDTHWISE : Position::LENGTHWISE;
    return allowFlip && ifCanCreate(lengthInItems, widthInItems, itemType, nextPosition, context);
}

void Layer::flip() {
    assert(itemPosition != Position::BOTH);
    assert(ifCanFlip());
    if (itemPosition == Position::LENGTHWISE) {
        itemPosition = Position::WIDTHWISE;
    } else {
        itemPosition = Position::LENGTHWISE;
    }
    std::swap(widthInItems, lengthInItems);
}

bool Layer::ifCanCreate(
        size_t widthInItems,
        size_t lengthInItems,
        size_t itemType,
        Position position,
        const ConstContextPtr& context) {
    // Position must be fixed.
    if (position == Position::BOTH) {
        return false;
    }
    // Parameters widthInItems and lengthInItems must be positive.
    if (!widthInItems  || !lengthInItems) {
        return false;
    }
    const auto& item = context->getItemInfo(itemType);
    // Position must fit initial requirement.
    if (item.position != Position::BOTH  &&
            item.position != position) {
        return false;
    }

    // If layer is metal, it can contain only one item inside.
    const size_t ONE = 1;
    if (item.material == Material::METAL && (widthInItems > ONE || lengthInItems > ONE)) {
        return false;
    }

    // There should be a way to decide how to build rows.
    const Constraints& constraints = context->getConstraints();
    bool foundOpportunity = false;
    if (widthInItems <= constraints.max_number_of_items_in_a_row &&
        lengthInItems <= constraints.max_number_of_rows_in_a_layer) {
        foundOpportunity = true;
    }
    if (widthInItems <= constraints.max_number_of_rows_in_a_layer &&
        lengthInItems <= constraints.max_number_of_rows_in_a_layer) {
        foundOpportunity = true;
    }
    return foundOpportunity;
}

size_t Layer::getWidthInItems() const {
    return widthInItems;
}

size_t Layer::getLengthInItems() const {
    return lengthInItems;
}

size_t Layer::getWidth() const {
    assert(itemPosition != Position::BOTH);
    if (itemPosition == Position::LENGTHWISE) {
        return widthInItems * itemInfo.width;
    } else {
        return widthInItems * itemInfo.length;
    }
}

size_t Layer::getLength() const {
    assert(itemPosition != Position::BOTH);
    if (itemPosition == Position::LENGTHWISE) {
        return lengthInItems * itemInfo.length;
    } else {
        return lengthInItems * itemInfo.width;
    }
}

size_t Layer::getHeight() const {
    return itemInfo.height;
}

double Layer::getWeight() const {
    return static_cast<double>(itemInfo.weight)
        * static_cast<double>(lengthInItems)
        * static_cast<double>(widthInItems);
}

size_t Layer::getItemType() const {
    return itemType;
}

Position Layer::getItemPosition() const {
    return itemPosition;
}

bool Layer::isMetal() const {
    return (itemInfo.material == Material::METAL);
}

size_t Layer::getItemCount() const {
    return getWidthInItems() * getLengthInItems();
}

std::vector<Layer> Layer::buildSimilar(Layer layer) {
    std::vector<Layer> result = {layer};
    if (layer.ifCanFlip()) {
        layer.flip();
        result.push_back(layer);
    }
    return result;
}

} // namespace esicup
