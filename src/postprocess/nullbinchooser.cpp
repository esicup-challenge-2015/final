/// Implements NullBinChooser defined in postprocess.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "postprocess.h"
#include "generator.h"

#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_map>
#include <cassert>
#include <vector>

namespace esicup {


SolutionInstance NullBinChooser::update(
        const SolutionOutputData& data, ConstContextPtr context) {
    TrivialPostprocessor trivial;
    SolutionInstance bestTrivial = trivial.update(data, context);
    size_t binsSize = bestTrivial.bins.size();
    auto bestSolution = bestTrivial;

    const size_t MAX_N_REPACK = 4;
    const size_t CUT_COUNT = 2;
    const double EPS = 1e-7;
    const size_t MAX_CHUNK_SIZE = 5;

    auto sortedData = data;
    std::sort(sortedData.solutions.begin(), sortedData.solutions.end(),
        [] (const SolutionInstance& lhs, const SolutionInstance& rhs) {
            return calculateObjective(lhs, false) < calculateObjective(rhs, false);
    });

    const size_t countInstances = std::min(MAX_CHUNK_SIZE, sortedData.solutions.size());
    std::vector<SolutionInstance> chunkInstances;
    chunkInstances.insert(chunkInstances.end(), 
        sortedData.solutions.begin(), sortedData.solutions.begin() + countInstances);

    size_t countContainersToRepack = std::min(MAX_N_REPACK, (binsSize + 1) / 2);
    for (const auto& solution : chunkInstances) {
        for (size_t idx = CUT_COUNT; idx <= countContainersToRepack; ++idx) {
            if (binsSize >= idx) {
                SolutionInstance local = repackToSelectNullBin(solution, context, idx);
                if (calculateObjective(local) + EPS < calculateObjective(bestSolution)) {
                    bestSolution = local;
                }
            }
        }
    }
    return bestSolution;
}

SolutionInstance NullBinChooser::repackToSelectNullBin(
        const SolutionInstance& instance,
        ConstContextPtr context, 
        const size_t countBins) {

    auto sortInstance = instance;
    std::sort(sortInstance.bins.begin(), sortInstance.bins.end(),
        [] (const BinWithHistory& lhs, const BinWithHistory& rhs) -> bool {
        return lhs.bin.getStackVolume() < rhs.bin.getStackVolume();
    });

    SolutionInstance chunk;
    chunk.bins.insert(chunk.bins.end(), 
        sortInstance.bins.begin(), sortInstance.bins.begin() + countBins);

    GroupOrder extracted = extractItems(chunk);
    auto chunkOrders = reorderGroups(extracted, context);

    SolutionInstance selectedInstance;
    bool assigned = false;

    for (const auto& order : chunkOrders) {
        SolutionInputData inputData;
        inputData.context = context;
        inputData.orders.push_back(order);

        CompositeProcessor processor(context, false);
        SolutionOutputData chunkSolution = processor.solve(inputData);

        SolutionOutputData fullSolution;
        for (const auto& solution : chunkSolution.solutions) {
            SolutionInstance configured;
            configured.bins.insert(configured.bins.end(), solution.bins.begin(), solution.bins.end());
            configured.bins.insert(configured.bins.end(), 
                sortInstance.bins.begin() + countBins, sortInstance.bins.end());
            fullSolution.solutions.push_back(configured);
        }

        TrivialPostprocessor trivial;
        SolutionInstance candidateInstance = trivial.update(fullSolution, context);

        const double EPS = 1e-7;
        if (!assigned || calculateObjective(candidateInstance) + EPS 
                            < calculateObjective(selectedInstance)) {
            selectedInstance = candidateInstance;
        }
    }

    return selectedInstance;
}


std::vector<GroupOrder> NullBinChooser::reorderGroups(
        const GroupOrder& order, ConstContextPtr context) {
    std::vector<GroupOrder> resultStorage;
    std::vector<Group> primary;
    std::vector<Group> secondary;

    for (const auto& group : order.groups) {
        std::pair<Group, Group> cutter = splitGroup(group, context);
        if (cutter.first.count != 0) {
            primary.push_back(cutter.first);
        }
        if (cutter.second.count != 0) {
            secondary.push_back(cutter.second);
        }
    }

    GroupOrder primaryOrder(primary, "");
    GroupOrder secondaryOrder(secondary, "");

    GroupOrderGenerator generator(context);
    generator.generate(primaryOrder, secondaryOrder, resultStorage);

    return resultStorage;
}

std::pair<Group, Group> NullBinChooser::splitGroup(const Group& group, ConstContextPtr context) {
    const double EPS = 1e-7;
    int maxAllowed = EPS + context->getConstraints().tolerance_item_product_demands
        * context->getItemCount(group.itemType);
    int currentCount = group.count;

    Group first(group.itemType, std::max(0, currentCount - maxAllowed));
    Group second(group.itemType, group.count - first.count);

    return std::make_pair(first, second);
}

GroupOrder NullBinChooser::extractItems(const SolutionInstance& instance) {
    GroupOrder resultSequence;
    std::unordered_map<size_t, size_t> mapperTypeToCount;

    for (const auto& binWithStory : instance.bins) {
        const auto& stacks = binWithStory.bin.getStackPlacements();
        for (const auto& loadedStack : stacks) {
            const auto& layers = loadedStack.stack.getLayerPlacements();
            for (const auto& liftedLayer : layers) {
                mapperTypeToCount[liftedLayer.layer.getItemType()] 
                    += liftedLayer.layer.getItemCount();
            }
        }
    }

    for (const auto& pairTypeCount : mapperTypeToCount) {
        Group group(pairTypeCount.first, pairTypeCount.second);
        resultSequence.groups.push_back(group);
    }

    return resultSequence;
}

} // namespace esicup
