/// Implements functions defined in postprocess.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "postprocess.h"

#include <cassert>
#include <vector>
#include <functional>


namespace esicup {

size_t calculateObjective(const SolutionInstance& instance, bool accountNullBin) {
    assert(instance.bins.size() > 0);
    auto minVolumeBinWithStory = instance.bins[0];
    size_t result = 0.0;
    for (const auto& binWithStory : instance.bins) {
        result += binWithStory.bin.getVolume();
        if (binWithStory.bin.getStackVolume() < minVolumeBinWithStory.bin.getStackVolume()) {
            minVolumeBinWithStory = binWithStory;
        }
    }
    if (accountNullBin && minVolumeBinWithStory.bin.ifCanBeZero()) {
        result -= minVolumeBinWithStory.bin.getVolume();
    }
    return result;
}

std::vector<StackOrder> getOrders(std::vector<Stack> currentOrder) {
    std::vector<StackOrder> finalOrder = {currentOrder};
    std::vector<std::function<bool (const Stack&, const Stack&)>> comparators {
        [](const Stack& lhs, const Stack& rhs) -> bool {
            return lhs.getBoxArea() > rhs.getBoxArea();
        },
        [](const Stack& lhs, const Stack& rhs) -> bool {
            return lhs.getVolume() > rhs.getVolume();
        },
        [](const Stack& lhs, const Stack& rhs) -> bool {
            return lhs.getWeight() > rhs.getWeight();
        },
        [](const Stack& lhs, const Stack& rhs) -> bool {
            return lhs.getHeight() > rhs.getHeight();
        }
    };
    for (auto& sorter : comparators) {
        sort(currentOrder.begin(), currentOrder.end(), sorter);
        finalOrder.push_back(currentOrder);
    }
    return finalOrder;
}

void packToFiller(IFiller2DPtr filler, StackOrder* currentOrder) {
    StackOrder missedStacks;
    for (auto& stack : *currentOrder) {
        if (filler->ifCanPutStack(stack)) {
            filler->putStack(stack);
        } else {
            missedStacks.push_back(stack);
            break;
        }
    }
    currentOrder->swap(missedStacks);
}

} // namespace esicup
