/// Implements SwapStackPostprocessor defined in postprocess.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "postprocess.h"
#include "../process/filler2d/filler2d.h"

#include <algorithm>
#include <iostream>
#include <vector>
#include <deque>
#include <queue>
#include <tuple>
#include <cassert>


namespace esicup {

SolutionInstance SwapStackPostprocessor::update(
        const SolutionOutputData& data,
        ConstContextPtr context) {
    SolutionOutputData filteredData;
    TrivialPostprocessor trivial;
    // At least one solution we have.
    filteredData.solutions.push_back(trivial.update(data, context));
    for (auto solution : data.solutions) {
        solution.sortBinsByStackVolume();
        // If true this case have already been processed in trivial postprocessor.
        if (solution.bins.front().bin.ifCanBeZero()) {
            continue;
        }
        bool resultIsFeasible = true;
        auto zeroBin = solution.bins.front().bin;
        size_t stackIdxToReplace = findProblematicStack(zeroBin, context);
        // Do something only if information is consistent.
        if (stackIdxToReplace < zeroBin.getStackCount()) {
            Stack zeroBinStack = zeroBin.getStackById(stackIdxToReplace);
            zeroBin.removeStack(stackIdxToReplace);
            size_t binId = 0, stackId = 0;
            std::tie(binId, stackId) = findReplaceStack(solution, zeroBinStack);
            // Perform modification only if information is consistent.
            if (binId != solution.bins.size()) {
                size_t widthShift = 0, lengthShift = 0;
                auto& otherBin = solution.bins[binId].bin;
                std::tie(widthShift, lengthShift) = otherBin.getCoordinatesById(stackId);
                Stack otherBinStack = otherBin.getStackById(stackId);
                otherBin.removeStack(stackId);
                assert(otherBin.ifCanPut(zeroBinStack, widthShift, lengthShift));
                otherBin.carry(zeroBinStack, widthShift, lengthShift);
                std::vector<LoadedStack> zeroBinStacks = zeroBin.getStackPlacements();
                zeroBinStacks.push_back({otherBinStack, 0, 0});
                bool successRepack = false;
                Bin repackResult(0, context);
                std::tie(successRepack, repackResult) = repackBin(
                        zeroBinStacks,
                        solution.bins.front().history,
                        context);
                if (successRepack) {
                    solution.bins.front().bin = repackResult;
                    resultIsFeasible = true;
                } else {
                    resultIsFeasible = false;
                }
            }
        }

        solution.sortBinsByStackVolume();
        if (resultIsFeasible && solution.bins.front().bin.ifCanBeZero()) {
            filteredData.solutions.push_back(solution);
        }
    }
    SolutionSelector selector;
    return selector.update(filteredData, context);
}

size_t SwapStackPostprocessor::findProblematicStack(
        const Bin& bin, const ConstContextPtr& context) const {
    auto binProductCountMap = bin.getProductCount();
    size_t selectedArea = 0, selectedId = bin.getStackCount();
    for (size_t stackId = 0; stackId < bin.getStackCount(); ++stackId) {
        auto stackProductCountMap = bin.getStackById(stackId).getProductCount();
        bool allIsOk = true;
        for (auto& valuePair : binProductCountMap) {
            size_t fullCount = context->getProductCount(valuePair.first);
            const double EPS = 1e-7;
            size_t maxAllowed = EPS +
                context->getConstraints().tolerance_item_product_demands * fullCount;
            size_t currentCount = valuePair.second - stackProductCountMap[valuePair.first];
            if (currentCount > maxAllowed) {
                allIsOk = false;
                break;
            }
        }
        if (allIsOk) {
            if (selectedId == bin.getStackCount() || selectedArea > bin.getStackById(stackId).getBoxArea()) {
                selectedId = stackId;
                selectedArea = bin.getStackById(stackId).getBoxArea();
            }
        }
    }
    return selectedId;
}

std::tuple<size_t, size_t> SwapStackPostprocessor::findReplaceStack(
        const SolutionInstance& solution,
        const Stack& stack) const {
    size_t selectedBinId = solution.bins.size();
    size_t selectedStackId = 0;
    size_t selectedStackArea = 0;
    for (size_t solutionId = 1; solutionId < solution.bins.size(); ++solutionId) {
        auto& bin = solution.bins[solutionId].bin;
        for (size_t stackId = 0; stackId < bin.getStackCount(); ++stackId) {
            size_t widthShift = 0, lengthShift = 0;
            auto previousStack = bin.getStackById(stackId);
            if (previousStack.getLength() < stack.getLength() 
                    || previousStack.getWidth() < stack.getWidth()) {
                continue;
            }
            auto experimentBin = bin;
            std::tie(widthShift, lengthShift) = experimentBin.getCoordinatesById(stackId);
            experimentBin.removeStack(stackId);
            if (experimentBin.ifCanPut(stack, widthShift, lengthShift) &&
                    (selectedBinId == solution.bins.size() 
                    || selectedStackArea > previousStack.getBoxArea())) {
                selectedStackId = stackId;
                selectedBinId = solutionId;
                selectedStackArea = previousStack.getBoxArea();
            }
        }
    }
    return std::tie(selectedBinId, selectedStackId);
}

std::tuple<bool, Bin> SwapStackPostprocessor::repackBin(
        const std::vector<LoadedStack>& stacks,
        std::pair<FillerType, LayerStrategyType> fillerConfiguration,
        const ConstContextPtr& context) const {
    StackOrder order;
    for (auto& loadedStack : stacks) {
        order.push_back(loadedStack.stack);
    }
    FillerFactory factory(context);
    std::vector<StackOrder> allOrders = getOrders(order);
    bool repacked = false;
    Bin resultBin = Bin(0, context);
    for (auto& currentOrder : allOrders) {
        IFiller2DPtr filler = factory.createFiller2D(
            fillerConfiguration.first, fillerConfiguration.second, 0);
        packToFiller(filler, &currentOrder);
        if (!currentOrder.size()) {
            repacked = true;
            resultBin = filler->getBin();
            break;
        }
    }
    return std::tie(repacked, resultBin);
}

} // namespace esicup
