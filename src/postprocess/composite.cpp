/// Implements CompositePostprocessor defined in postprocess.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "postprocess.h"

#include <tuple>
#include <vector>
#include <algorithm>

namespace esicup {

SolutionInstance CompositePostprocessor::update(
        const SolutionOutputData& dataToUpdate, ConstContextPtr context) {
    SolutionOutputData outputData;
    NullBinChooser nullBinChooser;
    SwapStackPostprocessor swapStackPostProcessor;
    TrivialPostprocessor trivialPostProcessor;
    outputData.solutions = {
        nullBinChooser.update(dataToUpdate, context),
        swapStackPostProcessor.update(dataToUpdate, context),
        trivialPostProcessor.update(dataToUpdate, context)
    };
    SolutionSelector selector;
    return selector.update(outputData, context);
}

} // namespace esicup
