/**
 * @file postprocess.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines postprocessor entities - entities to slightly update 
 * solution received in processing stage and to convert to the export format.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "../interfaces.h"
#include "../process/process.h"

#include <tuple>
#include <vector>
#include <algorithm>

namespace esicup {

/// @brief Simplified definition for stack vector.
typedef std::vector<Stack> StackOrder;

/**
 * @brief Calculates the main objective mentioned in competition.
 * 
 * @param instance The solution to compute the objective from.
 * @param accounNullBin Whether to account for null bin when computing objective.
 * 
 * @return The value of the first objective (the total bin volume).
 */
size_t calculateObjective(const SolutionInstance& instance, bool accounNullBin = true);
/**
 * @brief Sorts vector of stacks according to 
 * multiple comparators and returns the obtained sortings.
 * 
 * @param initialOrder The original stack sequence.
 * 
 * @return Vector with different stack orders.
 */
std::vector<StackOrder> getOrders(std::vector<Stack> initialOrder);
/**
 * @brief Tries to pack stacks from stack vector into a filler.
 * Those not packed are returned to stack vector (that gets updated),
 * while those packed are removed from stack vector.
 * 
 * @param filler The filler to pack stacks inside.
 * @param currentOrder The stack sequence for packing. Gets updated in the process.
 */
void packToFiller(IFiller2DPtr filler, StackOrder* currentOrder);

/**
 * @brief Postprocessing interface. Final modifications to the solution.
 */
class IPostprocessor {
public:
    /**
     * @brief Applies final modifications to the solution, and returns the 
     * version that is ready to get exported for checking.
     * 
     * @param data The result of processing stage.
     * @param context Context with information about input instance.
     * 
     * @return  Solution in the format for exporting and checking.
     */
    virtual SolutionInstance update(
        const SolutionOutputData& data, 
        ConstContextPtr context) = 0;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~IPostprocessor() {};
};


/**
 * @brief Selects the most effective solution among candidates stored in 
 * process stage output. Each SolutionInstance among candidates must be 
 * sorted and have appropriate zero bin.
 */
class SolutionSelector final : public IPostprocessor {
public:
    /**
     * @brief Selects the most effective solution among candidates
     * stored in solution output.
     * 
     * @param data Solution output data in the format received at the processing stage.
     * @param context Context with information about input instance.
     * 
     * @return The most effective SolutionInstance among those in 
     *      solution output data according to metric O1.
     */
    SolutionInstance update(
        const SolutionOutputData& data, 
        ConstContextPtr context);
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~SolutionSelector() {}
};

/**
 * @brief Trivial postprocessor simply ensures that the data is in 
 * appropriate format according to zero bin condition, and fixes if not.
 * Afterwards delegates the rest of the job to SolutionSelector.
 */
class TrivialPostprocessor final : public IPostprocessor {
public:
    /**
     * @brief Goes through each SolutionInstance in data. Sorts it by stack volume sum. 
     * If zero bin cannot be zero, then inserts empty bin to the front. Sorted order is 
     * preserved. Then decides with SolutionSelector.
     * 
     * @param data Solution output data in the format received at the processing stage.
     * @param context Context with information about input instance.
     * 
     * @return The most effective SolutionInstance among considered according to metric O1.
     */
    SolutionInstance update(
        const SolutionOutputData& data, 
        ConstContextPtr context) override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~TrivialPostprocessor() {}
};

/**
 * @brief Repacks prefix part of the solution differently 
 * and checks whether that brought some improvement.
 */
class NullBinChooser final : public IPostprocessor {
public:
    /**
     * @brief Updates the solutions from processing stage and 
     * returns the most effective according to metric O1.
     * Inside, tries to repack part of the solutions
     * and checks whether that brought improvement in metric O1.
     * 
     * @param data Solution output data in the format received after the processing stage.
     * @param context Context with information about input instance.
     * 
     * @return The selected most effective solution among the available options. 
     */
    SolutionInstance update(
        const SolutionOutputData& data, 
        ConstContextPtr context) override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~NullBinChooser() {}
private:
    /**
     * @brief Repacks part of the solution and tries to 
     * assign the other bin to the null bin role.
     * 
     * @param instance The solution instance to start from.
     * @param context Context with information about input instance.
     * @param countBins The number of bins to unpack for further repacking.
     * 
     * @return The most effective according to O1 repacked solution. 
     */
    SolutionInstance repackToSelectNullBin(
        const SolutionInstance& instance,
        ConstContextPtr context, 
        const size_t countBins);
    /**
     * @brief Generates group orders by applying group order generator.
     * Essentially, the original order is divided into two parts.
     * The parts are sorted according to specific comparators, and 
     * then merged.
     * 
     * @param order The group order to start from.
     * @param context Context with information about input instance.
     * 
     * @return The vector with generated group orders.
     */
    std::vector<GroupOrder> reorderGroups(
        const GroupOrder& order, 
        ConstContextPtr context);
    /**
     * @brief Extracts packages from packing and forms corresponding groups.
     * 
     * @param instance The packed instance.
     * 
     * @return The extracted group order.
     */
    GroupOrder extractItems(
        const SolutionInstance& instance);
    /**
     * @brief Splits item group into two parts 
     * according to product demand requirements.
     * 
     * @param group The group to split.
     * @param context Context with information about input instance.
     * 
     * @return Pair with two groups, obtained in the result of splitting.
     */
    std::pair<Group, Group> splitGroup(
        const Group& group, 
        ConstContextPtr context);
};

/**
 * @brief Postprocessor to explore opportunities 
 * to swap some stacks.
 */
class SwapStackPostprocessor final : public IPostprocessor {
public:
    /**
     * @brief Explores opportunities by swapping some stacks.
     * Applies SolutionSelector on the obtained options.
     * 
     * @param data The data storing multiple opportunities 
     *      in the format after processing stage.
     * @param context Context with input information about the instance.
     * 
     * @return The most effective according to O1 solution among the generated variants.
     */
    SolutionInstance update(
        const SolutionOutputData& data, 
        ConstContextPtr context) override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~SwapStackPostprocessor() {};

private:
    /**
     * @brief Finds the id of the stack inside zero bin that may benefit from replacement. 
     * If there are multiple opportunities, returns the one corresponding 
     * to stack with lowest area. If there is no option, returns bin.getStackCount().
     * 
     * @param bin The zero bin to look inside for the stack.
     * @param context Context with input information about the instance.
     * 
     * @return The index of the stack inside the bin that may benefit from replacement. 
     */
    size_t findProblematicStack(
        const Bin& bin, 
        const ConstContextPtr& context) const;

    /**
     * @brief Finds the stack opportunity to replace with the requested option.
     * Returns tuple containing std::tie(binId, stackId) of found bin and stack to replace.
     * It is guaranteed that binId does not point to zero bin.
     * In case there is no such bin found, binId is equal to solution.bins.size().
     * 
     * @param solution The solution to look for replacement.
     * @param stack The stack reference for replacement (
     *      i.e. this stack is to get replaced with found).
     * 
     * @return The tuple containing std::tie(binId, stackId) of found bin and stack to replace.
     */
    std::tuple<size_t, size_t> findReplaceStack(
            const SolutionInstance& solution,
            const Stack& stack) const;

    /**
     * @brief Repacks stacks into a bin described by filler configuration. 
     * The new bin is supposed to be zero. Thus we will use the biggest possible 
     * bin for it -- this volume is anyway not counted in the major metric. 
     * By sort in context, the biggest bin type have type index 0.
     * 
     * @param stacks The vector with stacks to get packed.
     * @param fillerConfiguration The configuration for the filler used to fill the bin.
     * @param context Context with input information about the instance.
     * 
     * @return Tuple with two entries. The first is true only if the repacking was 
     * successful, i.e. whether all stacks are packed (and false otherwise). The second 
     * provides the new bin with those stacks that were packed. If not all were packed,
     * returns trivial empty bin.
     */
    std::tuple<bool, Bin> repackBin(
            const std::vector<LoadedStack>& stacks,
            std::pair<FillerType, LayerStrategyType> fillerConfiguration,
            const ConstContextPtr& context) const;
};

/**
 * @brief Applies multiple postprocessors and then selects the 
 * most effective solution instance.
 */
class CompositePostprocessor final : public IPostprocessor {
public:
    /**
     * @brief Applies multiple postprocessors in the following order:
     * NullBinChooser, SwapStackPostprocessor, TrivialPostprocessor.
     * Afterwards, aggregates with SolutionSelector.
     * 
     * @param data The data in the format provided after processing stage.
     * @param context Context with input information about the instance.
     * 
     * @return The most effective solution instance among those considered.
     */
    SolutionInstance update(
        const SolutionOutputData& data, 
        ConstContextPtr context) override;
    /**
     * @brief Virtual destructor to ensure correct C++ polymorphism.
     */
    virtual ~CompositePostprocessor() {};
};

} // namespace esicup

