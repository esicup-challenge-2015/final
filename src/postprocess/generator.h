/**
 * @file generator.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines and implements group order generator. 
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once
#include "postprocess.h"

#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_map>
#include <cassert>
#include <vector>

namespace esicup {

/**
 * @brief Group order generator is an entity to merge two parts of the entire
 * sequence, where each part is sorted according to various comparators before
 * merging.
 */
class GroupOrderGenerator {
public:
    /**
     * @brief Constructor for group order generator.
     * 
     * @param context The context with instance input information.
     */
    GroupOrderGenerator(ConstContextPtr context) {
        //by count
        comparators.push_back([context] (const Group& lhs, const Group& rhs) -> bool {
            return lhs.count > rhs.count;
        });
        //by height
        comparators.push_back([context] (const Group& lhs, const Group& rhs) -> bool {
            return context->getItemInfo(lhs.itemType).height >
                context->getItemInfo(rhs.itemType).height;
        });
        //by area
        comparators.push_back([context] (const Group& lhs, const Group& rhs) -> bool {
                return context->getItemInfo(lhs.itemType).width 
                        * context->getItemInfo(lhs.itemType).length 
                        > context->getItemInfo(rhs.itemType).width 
                        * context->getItemInfo(rhs.itemType).length;
        });
        //by weight
        comparators.push_back([context] (const Group& lhs, const Group& rhs) -> bool {
            return context->getItemInfo(lhs.itemType).weight >
                context->getItemInfo(rhs.itemType).weight;
        });
    }
    /**
     * @brief Generates alternative groups to orderStorage. 
     * 
     * @param firstPart The first part of the group sequence.
     * @param secondPart The second part of the group sequence.
     * @param orderStorage The storage to write the generated groups inside.
     */
    void generate(
            const GroupOrder& firstPart, 
            const GroupOrder& secondPart,
            std::vector<GroupOrder>& orderStorage) {
        for (const auto& sorter : comparators) {
            auto frontPart(firstPart);
            auto backPart(secondPart);

            std::sort(frontPart.groups.begin(), frontPart.groups.end(), sorter);
            std::sort(backPart.groups.begin(), backPart.groups.end(), sorter);

            auto merged = frontPart;
            merged.groups.insert(merged.groups.end(), backPart.groups.begin(), backPart.groups.end());
            orderStorage.push_back(merged);
        }
    }

private:
    /// @brief Comparators to apply when sorting before merging the parts.
    std::vector<std::function<bool (const Group&, const Group&)>> comparators;
};

} // namespace esicup