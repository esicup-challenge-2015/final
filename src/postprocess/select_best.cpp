/// Implements BestSolutionSelector defined in postprocess.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "postprocess.h"

#include <algorithm>
#include <cassert>


namespace esicup {

SolutionInstance SolutionSelector::update(
            const SolutionOutputData& aData,
            ConstContextPtr context) {
    assert(context->getItemTypesCount());
    auto outputData = aData;
    auto selectedResult = outputData.solutions.front();
    size_t minBinVolume = 0;
    bool assigned = false;
    for (auto& solution : outputData.solutions) {
        size_t fullVolume = solution.getSumBinVolume();
        assert(solution.isSorted() && solution.bins.front().bin.ifCanBeZero());
        fullVolume -= solution.bins.front().bin.getVolume();
        if (!assigned || minBinVolume > fullVolume) {
            assigned = true;
            minBinVolume = fullVolume;
            selectedResult = solution;
        }
    }
    return selectedResult;
}

} // namespace esicup
