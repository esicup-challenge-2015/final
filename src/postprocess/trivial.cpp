/// Implements TrivialPostprocessor defined in postprocess.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "postprocess.h"

#include <algorithm>
#include <iostream>
#include <cassert>


namespace esicup {

SolutionInstance TrivialPostprocessor::update(
        const SolutionOutputData& aData,
        ConstContextPtr context) {
    auto modifiedData = aData;
    for (auto& solution : modifiedData.solutions) {
        solution.sortBinsByStackVolume();
        if (!solution.bins.front().bin.ifCanBeZero()) {
            solution.bins.push_back({Bin(0, context), 
                {FillerType::ROUND_FILLER_CENTER, LayerStrategyType::MINIMAL_LOST_AREA}});
            for (size_t index = 0; index < solution.bins.size(); ++index) {
                std::swap(solution.bins[index], solution.bins.back());
            }
        }
        assert(solution.isSorted() && solution.bins.front().bin.ifCanBeZero());
    }
    SolutionSelector selector;
    return selector.update(modifiedData, context);
}

} // namespace esicup
