/**
 * @file pipe.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Commonly included headers.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "context.h"
#include "common.h"
#include "input.h"
#include "preprocess/preprocess.h"
#include "process/process.h"
#include "postprocess/postprocess.h"
#include "output.h"
#include "parser.h"
