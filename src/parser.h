/**
 * @file parser.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief API and structures related to parsing CLI arguments.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 * 
 */
#pragma once

#include "interfaces.h"

namespace esicup {

/**
 * @brief Shows message describing correct executable call.
 * 
 * @param argc Number of arguments.
 * @param argv Array with arguments.
 */
void printUsage(int argc, char** argv);

/**
 * @brief Interface for the parser - entity that 
 * reads CLI arguments.
 */
class IParser {
public:
    /**
     * @brief Parses CLI arguments.
     * 
     * @param argc Number of CLI arguments.
     * @param argv Array with CLI arguments.
     * @return InstancesData containing CLI arguments.
     */
    virtual InstancesData parse(int argc, char** argv) = 0;
    /**
     * @brief Virtual destructor for correct C++ polymorphism.
     */
    virtual ~IParser() {};
};

/**
 * @brief Parses CLI arguments.
 */
class Parser : public IParser {
public:
    /**
     * @brief Parses CLI arguments.
     * 
     * @param argc Number of CLI arguments.
     * @param argv Array with CLI arguments.
     * @return InstancesData containing CLI arguments.
     */
    virtual InstancesData parse(int argc, char**argv) override;
    /**
     * @brief Virtual destructor for correct C++ polymorphism.
     */
    virtual ~Parser() {};

public:
    /// @brief Suffix for the file listing instances.
    static const std::string instancesFileSuffix;

private:
    /**
     * @brief Extracts names of the instance test directories.
     * 
     * @param directories The file listing the directories.
     * @return Vector with corresponding directory names.
     */
    std::vector<std::string> extractDirectoriesNames(
        const std::string& directories);
};


} // namespace esicup
