#!/usr/bin/env bash

# Checks the solution after the packing is ready.
#
# Author: Aliaksandr Nekrashevich
# Email: aliaksandr.nekrashevich@queensu.ca
# (c) Smith School of Business, 2024 

export CLASSPATH=./javaCheckerESICUP.jar
java -Xmx1024m javaCheckerESICUPPck.javaCheckerESICUPMain ./data/instancesA 2>&1
java -Xmx1024m javaCheckerESICUPPck.javaCheckerESICUPMain ./data/instancesB 2>&1
java -Xmx1024m javaCheckerESICUPPck.javaCheckerESICUPMain ./data/instancesX 2>&1

cat ./data/instancesA/checker_logs.txt
cat ./data/instancesB/checker_logs.txt
cat ./data/instancesX/checker_logs.txt

