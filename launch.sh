#!/usr/bin/env bash

# Launches the solution on the test instances.
#
# Author: Aliaksandr Nekrashevich
# Email: aliaksandr.nekrashevich@queensu.ca
# (c) Smith School of Business, 2024 

cd data
make clean
cd ..

cd src
make opt
mv main ../
cd ..
./main -p ./data/instancesA -a YES
./main -p ./data/instancesB -a YES
./main -p ./data/instancesX -a YES
rm -rf ./main

