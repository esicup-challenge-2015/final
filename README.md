# ESICUP Challenge 2015 - Final Submission

## Instructions

The environment to launch is Ubuntu Linux 22.04 LTS.

1. `launch.sh` - compiles executable from sources and launches on instances
2. `check.sh`  - executes checker, assumes that the instances are already solved and contain executable outputs
3. `document.sh` - documents the implementation with doxygen

## Disclaimer
Disclaimer: The original version of the code was implemented by multiple team members from Belarusian SU. The authorship in the source code file only shows that the corresponding team member decided to mention the authorship - however, there could have been other source code authors. Please see the abstract in the ESICUP meeting for the complete set of team members.