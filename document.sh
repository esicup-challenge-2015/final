#!/usr/bin/env bash

# Documents the source code.
#
# Author: Aliaksandr Nekrashevich
# Email: aliaksandr.nekrashevich@queensu.ca
# (c) Smith School of Business, 2024 

set -uexo pipefail

rm -rf docs
rm -rf documentation
doxygen
cd docs/latex
make
cd ../..
mkdir -p documentation
mv docs/latex/refman.pdf documentation/api.pdf

set +uexo
